using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Icebreakers
{
    public struct PanelIntersection
    {
        public bool front;
        public float t;
    }

    public class Panel : MonoBehaviour
    {
        [SerializeField]
        Transform sceneRoot;
        public Transform SceneRoot => sceneRoot;

        [SerializeField]
        PanelView panelView;
        public PanelView PanelView => panelView;

        [SerializeField]
        int panelNumber = 1;
        public int PanelNumber => panelNumber;

        [SerializeField]
        bool isInside;
        public bool IsInside
        {
            get => isInside;
            set => isInside = value;
        }

        [SerializeField]
        Light sceneLight;
        public Light SceneLight => sceneLight;

        [SerializeField]
        List<SpeechBubble> speechBubbles = new List<SpeechBubble>();
        public IReadOnlyList<SpeechBubble> SpeechBubbles => speechBubbles;

        public float showAfter = 0.0f;

        public void RemoveSpeechBubble(SpeechBubble speechBubble)
        {
            speechBubbles.Remove(speechBubble);
        }

        public void AddSpeechBubble(SpeechBubble speechBubble)
        {
            if (!speechBubbles.Contains(speechBubble))
                speechBubbles.Add(speechBubble);
        }

        public bool IntersectWithPanel(Ray ray, float maxDistance, out PanelIntersection intersection)
        {
            intersection = default;

            if (panelView == null)
                return false;

            Vector3 planeNormal = panelView.transform.forward;
            Vector3 planeOrigin = panelView.transform.position;
            Vector3 panelRight = panelView.transform.right / panelView.Size.x;
            Vector3 panelUp = panelView.transform.up / panelView.Size.y;

            float distance = -Vector3.Dot(planeNormal, planeOrigin);

            float vdot = Vector3.Dot(ray.direction, planeNormal);
            float ndot = -Vector3.Dot(ray.origin, planeNormal) - distance;

            intersection.front = vdot < 0;

            if (Mathf.Abs(vdot) < 0.0001f)
            {
                return false;
            }

            float t = ndot / vdot;
            intersection.t = t;

            if (t < 0 || t > maxDistance)
            {
                return false;
            }

            Vector3 pointOnPlane = (ray.origin + ray.direction * t) - planeOrigin;
            float x = Vector3.Dot(pointOnPlane, panelRight);
            float y = Vector3.Dot(pointOnPlane, panelUp);

            Vector2 uv = new Vector2(x, y) + new Vector2(0.5f, 0.5f);

            if (uv.x < 0 || uv.x > 1 || uv.y < 0 || uv.y > 1)
            {
                return false;
            }

            Texture2D mask = panelView.Mask;
            if (mask != null)
            {
                if (!mask.isReadable)
                {
                    Debug.LogError("Cannot read panel mask", mask);
                    return true;
                }
                else
                {
                    float maskValue = mask.GetPixelBilinear(uv.x, uv.y).a;
                    return maskValue > 0.5;
                }

            }

            return true;
        }

        [HideInInspector]
        public bool isInEditMode = false;
        [HideInInspector]
        public GameObject panelPreviewCamera;
        [HideInInspector]
        public GameObject localViewOrigion;
        [HideInInspector]
        public GameObject panelCutoutMesh;

#if UNITY_EDITOR

        public IList SpeechBubbleList => speechBubbles;

        public void Initialize(PanelView panelView, Transform sceneRoot)
        {
            this.panelView = panelView;
            this.sceneRoot = sceneRoot;
        }

        public void UpdateRenderingLayers(int panelNumber)
        {
            this.panelNumber = panelNumber;

            if (sceneRoot == null)
                return;

            uint layer = 1u << panelNumber;
            foreach (var renderer in sceneRoot.GetComponentsInChildren<Renderer>())
            {
                renderer.renderingLayerMask = layer;
                if (PrefabUtility.IsPartOfAnyPrefab(renderer))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(renderer);
                }
            }
        }
#endif
    }
}