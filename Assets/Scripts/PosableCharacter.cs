using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    [Serializable]
    public class PoseBone
    {
        public string name;
        public Transform transform;

        public Vector3 restPosition;
        public Quaternion restRotation;
        public Vector3 restScale;

        public float length = 0.1f;
        public Color color = Color.white;
        public bool enablePosition = true;
        public bool enableRotation = true;
        public bool enableScale = true;

        public bool enableIK = false;
        public int ikChainLength = 2;
        public string ikTargetName;
        public string ikPoleTargetName;

        [NonSerialized]
        public IKSolver ikSolver;

        [SerializeReference]
        public PoseBone parent;

        public Quaternion LocalSpaceToRigSpace(Quaternion rotation)
        {
            PoseBone currentParent = parent;
            while (currentParent != null)
            {
                rotation = currentParent.transform.localRotation * rotation;
                currentParent = currentParent.parent;
            }

            return rotation;
        }

        public Quaternion RigSpaceToLocalSpace(Quaternion rotation)
        {
            if (parent == null)
                return rotation;

            Quaternion parentSpace = Quaternion.identity;
            PoseBone currentParent = parent;
            while (currentParent != null)
            {
                parentSpace = currentParent.transform.localRotation * parentSpace;
                currentParent = currentParent.parent;
            }

            return Quaternion.Inverse(parentSpace) * rotation;
        }

        public Quaternion LocalSpaceToRigSpaceRestPose(Quaternion rotation)
        {
            PoseBone currentParent = parent;
            while (currentParent != null)
            {
                rotation = currentParent.restRotation * rotation;
                currentParent = currentParent.parent;
            }

            return rotation;
        }

        public Quaternion RigSpaceToLocalSpaceRestPose(Quaternion rotation)
        {
            if (parent == null)
                return rotation;

            Quaternion parentSpace = Quaternion.identity;
            PoseBone currentParent = parent;
            while (currentParent != null)
            {
                parentSpace = currentParent.restRotation * parentSpace;
                currentParent = currentParent.parent;
            }

            return Quaternion.Inverse(parentSpace) * rotation;
        }

        public Vector3 LocalSpaceToRigSpace(Vector3 position)
        {
            if (parent == null)
                return position;

            PoseBone currentParent = parent;
            Matrix4x4 matrix = Matrix4x4.identity;
            while (currentParent != null)
            {
                matrix = Matrix4x4.TRS(
                    currentParent.transform.localPosition,
                    currentParent.transform.localRotation,
                    currentParent.transform.localScale) * matrix;
                currentParent = currentParent.parent;
            }

            return matrix.MultiplyPoint(position);
        }

        public Vector3 RigSpaceToLocalSpace(Vector3 position)
        {
            if (parent == null)
                return position;

            PoseBone currentParent = parent;
            Matrix4x4 matrix = Matrix4x4.identity;
            while (currentParent != null)
            {
                matrix = Matrix4x4.TRS(
                    currentParent.transform.localPosition,
                    currentParent.transform.localRotation,
                    currentParent.transform.localScale) * matrix;
                currentParent = currentParent.parent;
            }

            return matrix.inverse.MultiplyPoint(position);
        }

        public Vector3 LocalSpaceToRigSpaceRestPose(Vector3 position)
        {
            if (parent == null)
                return position;

            PoseBone currentParent = parent;
            Matrix4x4 matrix = Matrix4x4.identity;
            while (currentParent != null)
            {
                matrix = Matrix4x4.TRS(
                    currentParent.restPosition,
                    currentParent.restRotation,
                    currentParent.restScale) * matrix;
                currentParent = currentParent.parent;
            }

            return matrix.MultiplyPoint(position);
        }

        public Vector3 RigSpaceToLocalSpaceRestPose(Vector3 position)
        {
            if (parent == null)
                return position;

            PoseBone currentParent = parent;
            Matrix4x4 matrix = Matrix4x4.identity;
            while (currentParent != null)
            {
                matrix = Matrix4x4.TRS(
                    currentParent.restPosition,
                    currentParent.restRotation,
                    currentParent.restScale) * matrix;
                currentParent = currentParent.parent;
            }

            return matrix.inverse.MultiplyPoint(position);
        }
    }

    public enum BoneAxis
    {
        PositiveX = 0,
        PositiveY = 1,
        PositiveZ = 2,
        NegativeX = 3,
        NegativeY = 4,
        NegativeZ = 5,
    }

    public class PosableCharacter : MonoBehaviour
    {
        [SerializeField]
        Transform rigRoot;

        public Transform RigRoot => rigRoot;

        [SerializeField]
        [SerializeReference]
        PoseBone[] poseBones = Array.Empty<PoseBone>();

        [SerializeField]
        BoneAxis primaryBoneAxis = BoneAxis.PositiveY;
        [SerializeField]
        BoneAxis secondaryBoneAxis = BoneAxis.PositiveX;

        public BoneAxis PrimaryBoneAxis => primaryBoneAxis;
        public BoneAxis SecondaryBoneAxis => secondaryBoneAxis;

        public IReadOnlyList<PoseBone> PoseBones => poseBones;
        public PoseBone FindBone(string name)
        {
            foreach (var poseBone in poseBones)
            {
                if (poseBone.name == name)
                {
                    return poseBone;
                }
            }

            return null;
        }

        public void FindChildrenOfBone(PoseBone parent, List<PoseBone> children)
        {
            children.Clear();

            foreach (var poseBone in poseBones)
            {
                if (poseBone.parent == parent)
                {
                    children.Add(poseBone);
                }
            }
        }

        public void CreatePoseBones()
        {
            if (rigRoot == null)
                throw new Exception("Cannot create pose bones, no rig root was assigned");

            List<PoseBone> oldPoseBones = new List<PoseBone>(this.poseBones);

            List<PoseBone> poseBones = new List<PoseBone>();
            void CreatePoseBoneRecursive(Transform transform, PoseBone parent)
            {
                PoseBone poseBone;
                string name = transform.name;
                int index = oldPoseBones.FindIndex(x => x.name == name);
                // Look for bones that were create previously
                // to retain settings for each bone
                if (index != -1)
                {
                    poseBone = oldPoseBones[index];
                    poseBone.parent = parent;
                    poseBone.transform = transform;
                }
                else
                {
                    poseBone = new PoseBone
                    {
                        name = name,
                        parent = parent,
                        transform = transform,
                    };
                }

                poseBones.Add(poseBone);

                foreach (Transform child in transform)
                {
                    CreatePoseBoneRecursive(child, poseBone);
                }
            }

            CreatePoseBoneRecursive(rigRoot, null);

            this.poseBones = poseBones.ToArray();

            AutoSetBoneLengths();

            InitializeRestPoses();
        }

        public void AutoSetBoneLengths()
        {
            List<PoseBone> children = new List<PoseBone>();
            Vector3 primaryAxis = BoneAxisToVector(primaryBoneAxis);
            foreach (var poseBone in poseBones)
            {
                FindChildrenOfBone(poseBone, children);
                if (children.Count == 1)
                {
                    PoseBone child = children[0];
                    poseBone.length = Vector3.Dot(child.transform.localPosition, primaryAxis);
                }
            }
        }

        public void InitializeRestPoses()
        {
            foreach (var poseBone in poseBones)
            {
                poseBone.restPosition = poseBone.transform.localPosition;
                poseBone.restRotation = poseBone.transform.localRotation;
                poseBone.restScale = poseBone.transform.localScale;
            }
        }

        public void UpdateConstraints()
        {
            foreach (var poseBone in poseBones)
            {
                if (poseBone.enableIK)
                {
                    if (poseBone.ikSolver == null)
                    {
                        PoseBone ikTargetBone = FindBone(poseBone.ikTargetName);
                        if (ikTargetBone == null)
                        {
                            continue;
                        }

                        PoseBone ikPoleTargetBone = FindBone(poseBone.ikPoleTargetName);
                        List<PoseBone> boneChain = new List<PoseBone>();
                        boneChain.Add(poseBone);
                        PoseBone chainBone = poseBone;
                        for (int i = 0; i < poseBone.ikChainLength; i++)
                        {
                            if (chainBone.parent == null)
                                break;

                            boneChain.Insert(0, chainBone.parent);
                            chainBone = chainBone.parent;
                        }

                        poseBone.ikSolver = new IKSolver(boneChain.ToArray(), ikTargetBone, ikPoleTargetBone);
                    }

                    poseBone.ikSolver.Solve(500, 0.001f);
                }
            }
        }

        public void ResetConstraints()
        {
            foreach (var poseBone in poseBones)
            {
                poseBone.ikSolver = null;
            }
        }

        public static Vector3 BoneAxisToVector(BoneAxis axis)
        {
            switch (axis)
            {
                case BoneAxis.PositiveX: return new Vector3(1, 0, 0);
                case BoneAxis.PositiveY: return new Vector3(0, 1, 0);
                case BoneAxis.PositiveZ: return new Vector3(0, 0, 1);
                case BoneAxis.NegativeX: return new Vector3(-1, 0, 0);
                case BoneAxis.NegativeY: return new Vector3(0, -1, 0);
                case BoneAxis.NegativeZ: return new Vector3(0, 0, -1);
                default: throw new Exception("Enum out of range");
            }
        }
    }
}