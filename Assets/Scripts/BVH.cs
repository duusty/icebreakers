using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Icebreakers
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BVHNode
    {
        public Bounds bbox;
        public int start;
        public int primitiveCount;
        public int rightOffset;
        public bool IsLeaf => rightOffset == 0;
    }

    public struct Intersection
    {
        public int primitiveIndex;
        public float t;
    }

    public struct NativeRay
    {
        public float3 o;
        public float3 d;
        public float3 invD;
        public float tMin;
        public float tMax;

        public static NativeRay FromRay(Ray ray, float tMin, float tMax)
        {
            return new NativeRay
            {
                o = ray.origin,
                d = ray.direction,
                invD = 1.0f / new float3(ray.direction),
                tMin = tMin,
                tMax = tMax,
            };
        }
    }

    public interface IIntersectionTest<Primitive>
    {
        /// <summary>
        /// Return t along the ray of the intersection with primitive.
        /// When no intersection can be found float.PositiveInfinity should be returned.
        /// </summary>
        /// <param name="primitive"></param>
        /// <param name="ray"></param>
        /// <returns></returns>
        float IntersectWith(in Primitive primitive, in NativeRay ray);

        void Precompute(in NativeRay ray);
    }

    public interface IOverlapTest<Primitive>
    {
        bool OverlapsWith(in Primitive primitive);
    }

    public interface IPrimitive
    {
        Bounds GetBounds();
        Vector3 GetCentroid();
    }

    [NativeContainer]
    [StructLayout(LayoutKind.Sequential)]
    public unsafe struct NativeBVH<Primitive> : INativeDisposable
        where Primitive : struct, IPrimitive
    {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
        internal AtomicSafetyHandle m_Safety;
        static readonly SharedStatic<int> s_staticSafetyId = SharedStatic<int>.GetOrCreate<NativeBVH<Primitive>>();

        [BurstDiscard]
        static void CreateStaticSafetyId()
        {
            s_staticSafetyId.Data = AtomicSafetyHandle.NewStaticSafetyId<NativeBVH<Primitive>>();
        }

        [NativeSetClassTypeToNullOnSchedule]
        DisposeSentinel m_DisposeSentinel;
#endif

        [NativeDisableUnsafePtrRestriction]
        internal UnsafeList* m_Nodes;

        [NativeDisableUnsafePtrRestriction]
        internal UnsafeList* m_Primitives;

        public NativeBVH(NativeArray<Primitive> primitives, Allocator allocator)
            : this(primitives, allocator, 2)
        {

        }

        NativeBVH(NativeArray<Primitive> primitives, Allocator allocator, int disposeSentinelStackDepth)
        {
            var totalPrimitiveSize = UnsafeUtility.SizeOf<Primitive>() * (long)primitives.Length;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
            CheckAllocator(allocator);
            CheckIsUnmanaged<Primitive>();

            DisposeSentinel.Create(out m_Safety, out m_DisposeSentinel, disposeSentinelStackDepth, allocator);
            if (s_staticSafetyId.Data == 0)
            {
                CreateStaticSafetyId();
            }
            AtomicSafetyHandle.SetStaticSafetyId(ref m_Safety, s_staticSafetyId.Data);
#endif

            m_Nodes = UnsafeList.Create(UnsafeUtility.SizeOf<BVHNode>(), UnsafeUtility.AlignOf<BVHNode>(), 0, allocator);
            m_Primitives = UnsafeList.Create(UnsafeUtility.SizeOf<Primitive>(), UnsafeUtility.AlignOf<Primitive>(), primitives.Length, allocator);

            m_Primitives->Length = primitives.Length;
            UnsafeUtility.MemCpy(m_Primitives->Ptr, primitives.GetUnsafeReadOnlyPtr(), totalPrimitiveSize);

#if ENABLE_UNITY_COLLECTIONS_CHECKS
            AtomicSafetyHandle.SetBumpSecondaryVersionOnScheduleWrite(m_Safety, true);
#endif
        }

        /// <summary>
        /// Reports whether memory for the container is allocated.
        /// </summary>
        /// <value>True if this container object's internal storage has been allocated.</value>
        /// <remarks>Note that the container storage is not created if you use the default constructor. You must specify
        /// at least an allocation type to construct a usable container.</remarks>
        public bool IsCreated => m_Nodes != null;

        /// <summary>
        /// Disposes of this container and deallocates its memory immediately.
        /// </summary>
        public void Dispose()
        {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif
            UnsafeList.Destroy(m_Nodes);
            UnsafeList.Destroy(m_Primitives);
            m_Nodes = null;
            m_Primitives = null;
        }

        /// <summary>
        /// Safely disposes of this container and deallocates its memory when the jobs that use it have completed.
        /// </summary>
        /// <remarks>You can call this function dispose of the container immediately after scheduling the job. Pass
        /// the [JobHandle](https://docs.unity3d.com/ScriptReference/Unity.Jobs.JobHandle.html) returned by
        /// the [Job.Schedule](https://docs.unity3d.com/ScriptReference/Unity.Jobs.IJobExtensions.Schedule.html)
        /// method using the `jobHandle` parameter so the job scheduler can dispose the container after all jobs
        /// using it have run.</remarks>
        /// <param name="inputDeps">The job handle or handles for any scheduled jobs that use this container.</param>
        /// <returns>A new job handle containing the prior handles as well as the handle for the job that deletes
        /// the container.</returns>
        public JobHandle Dispose(JobHandle inputDeps)
        {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
            // [DeallocateOnJobCompletion] is not supported, but we want the deallocation
            // to happen in a thread. DisposeSentinel needs to be cleared on main thread.
            // AtomicSafetyHandle can be destroyed after the job was scheduled (Job scheduling
            // will check that no jobs are writing to the container).
            DisposeSentinel.Clear(ref m_DisposeSentinel);

            var jobHandle = new NativeBVHDisposeJob { Data = new NativeBVHDispose { m_Nodes = m_Nodes, m_Primitives = m_Primitives, m_Safety = m_Safety } }.Schedule(inputDeps);

            AtomicSafetyHandle.Release(m_Safety);
#else
            var jobHandle = new NativeBVHDisposeJob { Data = new NativeBVHDispose { m_Nodes = m_Nodes, m_Primitives = m_Primitives } }.Schedule(inputDeps);
#endif
            m_Nodes = null;
            m_Primitives = null;

            return jobHandle;
        }

        public NativeArray<Primitive> Primitives
        {
            get
            {
    #if ENABLE_UNITY_COLLECTIONS_CHECKS
                AtomicSafetyHandle.CheckGetSecondaryDataPointerAndThrow(m_Safety);
                var arraySafety = m_Safety;
                AtomicSafetyHandle.UseSecondaryVersion(ref arraySafety);
    #endif
                var array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<Primitive>(m_Primitives->Ptr, m_Primitives->Length, Allocator.None);

    #if ENABLE_UNITY_COLLECTIONS_CHECKS
                NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref array, arraySafety);
    #endif
                return array;
            }
        }

        public NativeArray<BVHNode> Nodes
        {
            get
            {
#if ENABLE_UNITY_COLLECTIONS_CHECKS
                AtomicSafetyHandle.CheckGetSecondaryDataPointerAndThrow(m_Safety);
                var arraySafety = m_Safety;
                AtomicSafetyHandle.UseSecondaryVersion(ref arraySafety);
#endif
                var array = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<BVHNode>(m_Nodes->Ptr, m_Nodes->Length, Allocator.None);

#if ENABLE_UNITY_COLLECTIONS_CHECKS
                NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref array, arraySafety);
#endif
                return array;
            }
        }


        [StructLayout(LayoutKind.Sequential)]
        struct BuildEntry
        {
            public int parent;
            public int start;
            public int end;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct PrimitiveInfo
        {
            public Bounds bounds;
            public Vector3 centroid;
        }

        struct BuildStack : IDisposable
        {
            NativeArray<BuildEntry> entries;
            int stackPtr;
            public int Count => stackPtr;

            public BuildStack(int size)
            {
                entries = new NativeArray<BuildEntry>(size, Allocator.Temp);
                stackPtr = 0;
            }

            public void Dispose()
            {
                entries.Dispose();
            }

            public BuildEntry Pop() { return entries[--stackPtr]; }
            public void Push(in BuildEntry entry) { entries[stackPtr++] = entry; }
        }

        public JobHandle BuildJob(int leafSize, JobHandle dependsOn = default)
        {
            return new BuildBVHJob
            {
                bvh = this,
                leafSize = leafSize,
            }.Schedule(dependsOn);
        }

        [BurstCompile(CompileSynchronously = true)]
        struct BuildBVHJob : IJob
        {
            public NativeBVH<Primitive> bvh;
            public int leafSize;

            public void Execute()
            {
                bvh.Build(leafSize);
            }
        }

        public unsafe void Build(int leafSize)
        {
            if (m_Primitives->Length == 0)
            {
                m_Nodes->Resize<BVHNode>(0, NativeArrayOptions.UninitializedMemory);
                return;
            }

            BuildStack todo = new BuildStack(128);

            const int Untouched = -1;
            const int TouchedTwice = -3;
            int nodeCount = 0;

            NativeArray<PrimitiveInfo> primInfos = new NativeArray<PrimitiveInfo>(m_Primitives->Length, Allocator.Temp);
            for (int i = 0; i < m_Primitives->Length; i++)
            {
                CheckIndexInRange(i, m_Primitives->Length);
                primInfos[i] = new PrimitiveInfo
                {
                    bounds = UnsafeUtility.ArrayElementAsRef<Primitive>(m_Primitives->Ptr, i).GetBounds(),
                    centroid = UnsafeUtility.ArrayElementAsRef<Primitive>(m_Primitives->Ptr, i).GetCentroid(),
                };
            }

            BuildEntry root = new BuildEntry
            {
                parent = -4,
                start = 0,
                end = m_Primitives->Length,
            };
            todo.Push(root);

            BVHNode node;
            NativeList<BVHNode> buildNodes = new NativeList<BVHNode>(m_Primitives->Length * 2, Allocator.Temp);

            while (todo.Count > 0)
            {
                BuildEntry bnode = todo.Pop();

                int start = bnode.start;
                int end = bnode.end;
                int primitiveCount = end - start;

                nodeCount++;
                node.start = start;
                node.primitiveCount = primitiveCount;
                node.rightOffset = Untouched;

                Bounds bb = primInfos[start].bounds;
                Bounds bc = new Bounds(primInfos[start].centroid, new Vector3(0, 0, 0));
                for (int p = start + 1; p < end; p++)
                {
                    bb.Encapsulate(primInfos[p].bounds);
                    bc.Encapsulate(primInfos[p].centroid);
                }

                node.bbox = bb;

                // If the number of primitives at this point is less than the leaf
                // size, then this will become a leaf. (Signified by rightOffset == 0)
                if (primitiveCount <= leafSize) {
                    node.rightOffset = 0;
                }

                buildNodes.Add(node);

                // Child touches parent...
                // Special case: Don't do this for the root.
                if (bnode.parent != -4)
                {
                    ref BVHNode parent = ref buildNodes.ElementAt(bnode.parent);
                    parent.rightOffset--;

                    // When this is the second touch, this is the right child.
                    // The right child sets up the offset for the flat tree.
                    if (parent.rightOffset == TouchedTwice)
                    {
                        parent.rightOffset = nodeCount - 1 - bnode.parent;
                    }
                }

                // If this is a leaf, no need to subdivide.
                if (node.rightOffset == 0) continue;

                // Set the split dimensions
                Vector3 extent = bc.extents;
                int splitDim = 0;
                if (extent.y > extent.x)
                    splitDim = 1;
                if (extent.z > extent.x && extent.z > extent.y)
                    splitDim = 2;

                // Split on the center of the longest axis
                float splitCoord = 0.5f * (bc.min[splitDim] + bc.max[splitDim]);

                // Partition the list of objects on this split
                int mid = start;
                for (int i = start; i < end; ++i)
                {
                    CheckIndexInRange(i, m_Primitives->Length);
                    float3 center = UnsafeUtility.ReadArrayElement<Primitive>(m_Primitives->Ptr, i).GetCentroid();
                    if (center[splitDim] < splitCoord)
                    {
                        CheckIndexInRange(mid, m_Primitives->Length);
                        Swap(ref UnsafeUtility.ArrayElementAsRef<Primitive>(m_Primitives->Ptr, i), ref UnsafeUtility.ArrayElementAsRef<Primitive>(m_Primitives->Ptr, mid));

                        PrimitiveInfo tmp2 = primInfos[i];
                        primInfos[i] = primInfos[mid];
                        primInfos[mid] = tmp2;

                        mid++;
                    }
                }

                // If we get a bad split, just choose the center...
                if (mid == start || mid == end)
                {
                    mid = start + (end - start) / 2;
                }

                BuildEntry rightChild = new BuildEntry{ parent = nodeCount - 1, start = mid, end = end };
                BuildEntry leftChild = new BuildEntry{ parent = nodeCount - 1, start = start, end = mid };

                todo.Push(rightChild);
                todo.Push(leftChild);
            }

            m_Nodes->Resize<BVHNode>(nodeCount, NativeArrayOptions.UninitializedMemory);
            for (int i = 0; i < nodeCount; i++)
            {
                CheckIndexInRange(i, m_Nodes->Length);
                UnsafeUtility.WriteArrayElement(m_Nodes->Ptr, i, buildNodes[i]);
            }

            buildNodes.Dispose();
            todo.Dispose();
        }

        [NativeContainer]
        internal unsafe struct NativeBVHDispose
        {
            [NativeDisableUnsafePtrRestriction]
            public UnsafeList* m_Nodes;
            public UnsafeList* m_Primitives;

#if ENABLE_UNITY_COLLECTIONS_CHECKS
            internal AtomicSafetyHandle m_Safety;
#endif

            public void Dispose()
            {
                UnsafeList.Destroy(m_Nodes);
                UnsafeList.Destroy(m_Primitives);
            }
        }

        [BurstCompile]
        internal unsafe struct NativeBVHDisposeJob : IJob
        {
            internal NativeBVHDispose Data;

            public void Execute()
            {
                Data.Dispose();
            }
        }

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        static void CheckIndexInRange(int value, int length)
        {
            if (value < 0)
                throw new IndexOutOfRangeException($"Value {value} must be positive.");

            if ((uint)value >= (uint)length)
                throw new IndexOutOfRangeException($"Value {value} is out of range in NativeList of '{length}' Length.");
        }

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        static void CheckAllocator(Allocator allocator)
        {
            // Native allocation is only valid for Temp, Job and Persistent.
            if (allocator <= Allocator.None)
                throw new ArgumentException("Allocator must be Temp, TempJob or Persistent", nameof(allocator));
        }

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        [BurstDiscard] // Must use BurstDiscard because UnsafeUtility.IsUnmanaged is not burstable.
        internal static void CheckIsUnmanaged<S>()
        {
            if (!UnsafeUtility.IsValidNativeContainerElementType<S>())
            {
                throw new ArgumentException($"{typeof(S)} used in native collection is not blittable, not primitive, or contains a type tagged as NativeContainer");
            }
        }

        static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }



        public Intersection Intersect<Intersector>(Intersector intersector, in NativeRay ray) where Intersector : IIntersectionTest<Primitive>
        {
            Intersection intersection;
            intersection.primitiveIndex = -1;
            intersection.t = float.PositiveInfinity;

            if (m_Nodes->Length == 0)
                return intersection;

            int* todo = stackalloc int[64];
            float* todoT = stackalloc float[64];
            int stackptr = 0;

            // "Push" on the root node to the working set
            todo[stackptr] = 0;
            todoT[stackptr] = float.NegativeInfinity;

            intersector.Precompute(ray);

            while (stackptr >= 0)
            {
                // Pop off the next node to work on.
                int ni = todo[stackptr];
                float near = todoT[stackptr];
                stackptr--;

                CheckIndexInRange(ni, m_Nodes->Length);
                ref BVHNode node = ref UnsafeUtility.ArrayElementAsRef<BVHNode>(m_Nodes->Ptr, ni);

                // If this node is further than the closest found intersection, continue
                if (near > intersection.t) continue;

                // Is leaf -> Intersect
                if (node.IsLeaf)
                {
                    for (int i = 0; i < node.primitiveCount; i++)
                    {
                        ref Primitive obj = ref UnsafeUtility.ArrayElementAsRef<Primitive>(m_Primitives->Ptr, node.start + i);

                        float current = intersector.IntersectWith(obj, ray);
                        if (current < intersection.t)
                        {
                            intersection.t = current;
                            intersection.primitiveIndex = node.start + i;

                            // // If we're only looking for occlusion, then any hit is good enough to return true
                            // if (Flags & TraverserFlags::OnlyTestOcclusion)
                            // {
                            //     return current;
                            // }
                        }
                    }

                }
                else
                {  // Not a leaf

                    CheckIndexInRange(ni + 1, m_Nodes->Length);
                    bool hitc0 = IntersectionPrimitives.IntersectRayBBox(UnsafeUtility.ArrayElementAsRef<BVHNode>(m_Nodes->Ptr, ni + 1).bbox, ray, out float hit1, out float hit2);

                    CheckIndexInRange(ni + node.rightOffset, m_Nodes->Length);
                    bool hitc1 = IntersectionPrimitives.IntersectRayBBox(UnsafeUtility.ArrayElementAsRef<BVHNode>(m_Nodes->Ptr, ni + node.rightOffset).bbox, ray, out float hit3, out float hit4);

                    if (hitc0 && hitc1)
                    {
                        // We assume that the left child is a closer hit...
                        int closer = ni + 1;
                        int other = ni + node.rightOffset;

                        // ... If the right child was actually closer, swap the relavent values.
                        if (hit3 < hit1)
                        {
                            Swap(ref hit1, ref hit3);
                            Swap(ref hit2, ref hit4);
                            Swap(ref closer, ref other);
                        }

                        // It's possible that the nearest object is still in the other side, but
                        // we'll check the further-awar node later...

                        // Push the farther first
                        stackptr++;
                        todo[stackptr] = other;
                        todoT[stackptr] = hit3;

                        // And now the closer (with overlap test)
                        stackptr++;
                        todo[stackptr] = closer;
                        todoT[stackptr] = hit1;
                    }
                    else if (hitc0)
                    {
                        stackptr++;
                        todo[stackptr] = ni + 1;
                        todoT[stackptr] = hit1;
                    }
                    else if (hitc1)
                    {
                        stackptr++;
                        todo[stackptr] = ni + node.rightOffset;
                        todoT[stackptr] = hit3;
                    }
                }
            }

            return intersection;
        }

        public static void Overlap(in Primitive primitive, NativeList<int> overlappingPrimitives)
        {

        }
    }

    public static class IntersectionPrimitives
    {
        static void Swap<T>(ref T a, ref T b)
        {
            T tmp = a;
            a = b;
            b = tmp;
        }

        public static bool IntersectRayBBox(in Bounds bbox, in NativeRay ray, out float tnear, out float tfar)
        {
            Vector3 min = bbox.min;
            Vector3 max = bbox.max;

            float tmin = (min.x - ray.o.x) * ray.invD.x;
            float tmax = (max.x - ray.o.x) * ray.invD.x;

            if (tmin > tmax)
            {
                Swap(ref tmin, ref tmax);
            }

            float tymin = (min.y - ray.o.y) * ray.invD.y;
            float tymax = (max.y - ray.o.y) * ray.invD.y;

            if (tymin > tymax)
            {
                Swap(ref tymin, ref tymax);
            }

            if ((tmin > tymax) || (tymin > tmax))
            {
                tnear = default;
                tfar = default;
                return false;
            }

            tmin = math.max(tymin, tmin);
            tmax = math.min(tymax, tmax);

            float tzmin = (min.z - ray.o.z) * ray.invD.z;
            float tzmax = (max.z - ray.o.z) * ray.invD.z;

            if (tzmin > tzmax)
            {
                Swap(ref tzmin, ref tzmax);
            }

            if ((tmin > tzmax) || (tzmin > tmax))
            {
                tnear = default;
                tfar = default;
                return false;
            }

            tmin = math.max(tzmin, tmin);
            tmax = math.min(tzmax, tmax);

            tnear = tmin;
            tfar = tmax;

            return true;
        }

        static int MaxDimension(in float3 v)
        {
            return v.x > v.y ? (v.x > v.z ? 0 : 2) : (v.y > v.z ? 1 : 2);
        }

        static float3 Permute(in float3 v, int x, int y, int z)
        {
            return new float3(v[x], v[y], v[z]);
        }

        public static bool IntersectRayTriangle(in float3 p0, in float3 p1, in float3 p2, in NativeRay ray,
            out float t, out float b0, out float b1, out float b2)
        {
            t = default;
            b0 = default;
            b1 = default;
            b2 = default;

            float3 p0t = p0 - ray.o;
            float3 p1t = p1 - ray.o;
            float3 p2t = p2 - ray.o;

            int kz = MaxDimension(math.abs(ray.d));
            int kx = kz + 1; if (kx == 3) kx = 0;
            int ky = kx + 1; if (ky == 3) ky = 0;

            float3 d = Permute(ray.d, kx, ky, kz);
            p0t = Permute(p0t, kx, ky, kz);
            p1t = Permute(p1t, kx, ky, kz);
            p2t = Permute(p2t, kx, ky, kz);

            float sx = -d.x / d.z;
            float sy = -d.y / d.z;
            float sz = 1.0f / d.z;
            p0t.x += sx * p0t.z;
            p0t.y += sy * p0t.z;
            p1t.x += sx * p1t.z;
            p1t.y += sy * p1t.z;
            p2t.x += sx * p2t.z;
            p2t.y += sy * p2t.z;

            float e0 = p1t.x * p2t.y - p1t.y * p2t.x;
            float e1 = p2t.x * p0t.y - p2t.y * p0t.x;
            float e2 = p0t.x * p1t.y - p0t.y * p1t.x;

            if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e1 > 0))
                return false;

            float det = e0 + e1 + e2;
            if (det == 0)
                return false;

            p0t.z *= sz;
            p1t.z *= sz;
            p2t.z *= sz;
            float tScaled = e0 * p0t.z + e1 * p1t.z + e2 * p2t.z;
            if (det < 0 && (tScaled >= 0 || tScaled < ray.tMax * det))
                return false;
            if (det > 0 && (tScaled <= 0 || tScaled > ray.tMax * det))
                return false;

            float invDet = 1.0f / det;
            b0 = e0 * invDet;
            b1 = e1 * invDet;
            b2 = e2 * invDet;
            t = tScaled * invDet;

            return true;
        }

        public static bool IntersectRayTriangle2(in float3 p0, in float3 p1, in float3 p2, in NativeRay ray,
            out float t, out float b0, out float b1, out float b2)
        {
            t = default;
            b0 = default;
            b1 = default;
            b2 = default;

            float3 e1 = p1 - p0;
            float3 e2 = p2 - p0;
            float3 h = math.cross(ray.d, e2);
            float divisor = math.dot(h, e1);

            if (divisor > -0.0000001f && divisor < 0.0000001f)
            {
                return false;
            }

            float inv_divisor = 1.0f / divisor;

            float3 s = ray.o - p0;
            float u = math.dot(h, s) * inv_divisor;
            if (u < 0.0f || u > 1.0f)
            {
                return false;
            }

            float3 q = math.cross(s, e1);
            float v = math.dot(ray.d, q) * inv_divisor;
            if (v < 0.0f || u + v > 1.0f)
            {
                return false;
            }

            float tt = math.dot(e2, q) * inv_divisor;
            if (tt < ray.tMin || tt > ray.tMax)
            {
                return false;
            }

            b0 = u;
            b1 = v;
            b2 = 1 - u - v;
            t = tt;

            return true;
        }

        public struct RayTrianglePrecomputed
        {
            public int kx;
            public int ky;
            public int kz;

            public float sx;
            public float sy;
            public float sz;
        }

        public static RayTrianglePrecomputed PrecomputeRayTriangle(in NativeRay ray)
        {
            int kz = MaxDimension(math.abs(ray.d));
            int kx = kz + 1; if (kx == 3) kx = 0;
            int ky = kx + 1; if (ky == 3) ky = 0;

            float3 d = Permute(ray.d, kx, ky, kz);

            float sx = -d.x / d.z;
            float sy = -d.y / d.z;
            float sz = 1.0f / d.z;

            return new RayTrianglePrecomputed
            {
                kx = kx,
                ky = ky,
                kz = kz,
                sx = sx,
                sy = sy,
                sz = sz,
            };
        }

        public static bool IntersectRayTrianglePrecomputed(in float3 p0, in float3 p1, in float3 p2, in NativeRay ray, in RayTrianglePrecomputed pre,
            out float t, out float b0, out float b1, out float b2)
        {
            t = default;
            b0 = default;
            b1 = default;
            b2 = default;

            float3 p0t = p0 - ray.o;
            float3 p1t = p1 - ray.o;
            float3 p2t = p2 - ray.o;

            p0t = Permute(p0t, pre.kx, pre.ky, pre.kz);
            p1t = Permute(p1t, pre.kx, pre.ky, pre.kz);
            p2t = Permute(p2t, pre.kx, pre.ky, pre.kz);

            p0t.x += pre.sx * p0t.z;
            p0t.y += pre.sy * p0t.z;
            p1t.x += pre.sx * p1t.z;
            p1t.y += pre.sy * p1t.z;
            p2t.x += pre.sx * p2t.z;
            p2t.y += pre.sy * p2t.z;

            float e0 = p1t.x * p2t.y - p1t.y * p2t.x;
            float e1 = p2t.x * p0t.y - p2t.y * p0t.x;
            float e2 = p0t.x * p1t.y - p0t.y * p1t.x;

            if ((e0 < 0 || e1 < 0 || e2 < 0) && (e0 > 0 || e1 > 0 || e1 > 0))
                return false;

            float det = e0 + e1 + e2;
            if (det == 0)
                return false;

            p0t.z *= pre.sz;
            p1t.z *= pre.sz;
            p2t.z *= pre.sz;
            float tScaled = e0 * p0t.z + e1 * p1t.z + e2 * p2t.z;
            if (det < 0 && (tScaled >= 0 || tScaled < ray.tMax * det))
                return false;
            if (det > 0 && (tScaled <= 0 || tScaled > ray.tMax * det))
                return false;

            float invDet = 1.0f / det;
            b0 = e0 * invDet;
            b1 = e1 * invDet;
            b2 = e2 * invDet;
            t = tScaled * invDet;

            return true;
        }
    }
}