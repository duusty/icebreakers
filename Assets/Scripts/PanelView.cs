using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    public class PanelView : MonoBehaviour
    {
        [SerializeField]
        Vector2 size = new Vector2(1.0f, 0.7f);
        public Vector2 Size
        {
            get => size;
            set => size = value;
        }

        [SerializeField]
        Texture2D mask;
        public Texture2D Mask => mask;


#if UNITY_EDITOR
        [HideInInspector]
        public Vector3 originalPosition = Vector3.zero;

        [HideInInspector]
        public Quaternion originalRotation = Quaternion.identity;

        void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.red;
            Panel panel = GetComponentInParent<Panel>();
            if (panel != null && panel.isInEditMode)
            {
                Gizmos.color = Color.green;
            }
            Gizmos.DrawWireCube(new Vector3(0, 0, 0), new Vector3(size.x, size.y, 0));

        }
#endif
    }
}