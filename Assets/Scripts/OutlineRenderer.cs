using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Collections;
using UnityEngine;
using Unity.Profiling;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Rendering;

namespace Icebreakers
{
    [ExecuteAlways]
    public class OutlineRenderer : MonoBehaviour
    {
        MeshFilter sourceMeshFilter;
        MeshRenderer sourceMeshRenderer;
        SkinnedMeshRenderer sourceSkinnedMeshRenderer;

        [SerializeField]
        [HideInInspector]
        BakedLineData bakedLineData;

        public Renderer SourceRenderer => sourceMeshRenderer != null ? (Renderer)sourceMeshRenderer : (Renderer)sourceSkinnedMeshRenderer;

        [NonSerialized]
        OutlineMesh outlineMesh;

        [NonSerialized]
        OutlineMesh.AccelerationStructure accel;

        public bool IsSkinned => bakedLineData == null && sourceSkinnedMeshRenderer != null;
        Mesh SourceMesh => sourceMeshFilter != null ? sourceMeshFilter.sharedMesh : (sourceSkinnedMeshRenderer != null ? sourceSkinnedMeshRenderer.sharedMesh : null);
        public SkinnedMeshRenderer SkinnedMeshRenderer => sourceSkinnedMeshRenderer;
        public bool IsValid => CanSetUp;

        bool CanSetUp
        {
            get
            {
                if (bakedLineData != null)
                    return true;

                if (sourceMeshFilter == null && sourceSkinnedMeshRenderer == null)
                    return false;

                Mesh sourceMesh = sourceMeshFilter != null ? sourceMeshFilter.sharedMesh : sourceSkinnedMeshRenderer.sharedMesh;
                return sourceMesh != null && sourceMesh.isReadable;
            }
        }

        public static readonly HashSet<OutlineRenderer> outlineRenderers = new HashSet<OutlineRenderer>();

        void OnEnable()
        {
            if (!outlineRenderers.Add(this))
            {
                Debug.LogError("Failed to add outline renderer, state has been corrupted");
            }

            sourceMeshFilter = GetComponent<MeshFilter>();
            sourceMeshRenderer = GetComponent<MeshRenderer>();
            sourceSkinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();

            if (CanSetUp)
            {
                EnsureOutlineMesh();
            }
            else
            {
                if (sourceMeshFilter == null && sourceSkinnedMeshRenderer == null)
                    Debug.LogError("Missing mesh filter or skinned mesh renderer for outline renderer", this);

                Mesh mesh = SourceMesh;
                if (mesh != null)
                {
                    if (!mesh.isReadable)
                    {
                        Debug.LogError($"Cannot generate outlines for mesh {mesh.name}: it is not readable", mesh);
                    }
                }
            }
        }

        void OnDisable()
        {
            if (!outlineRenderers.Remove(this))
            {
                Debug.LogError("Failed to remove outline renderer, state has been corrupted");
            }

            if (outlineMesh != null)
            {
                outlineMesh.Dispose();
                outlineMesh = null;
            }

            if (accel != null)
            {
                accel.Dispose();
                accel = null;
            }
        }

        public void Reinitialize()
        {
            if (outlineMesh != null)
            {
                outlineMesh.Dispose();
                outlineMesh = null;
            }

            if (accel != null)
            {
                accel.Dispose();
                accel = null;
            }

            sourceMeshFilter = GetComponent<MeshFilter>();
            sourceMeshRenderer = GetComponent<MeshRenderer>();
            sourceSkinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();

            if (CanSetUp)
            {
                EnsureOutlineMesh();
            }
            else
            {
                if (sourceMeshFilter == null && sourceSkinnedMeshRenderer == null)
                    Debug.LogError("Missing mesh filter or skinned mesh renderer for outline renderer", this);

                Mesh mesh = sourceMeshFilter != null ? sourceMeshFilter.sharedMesh : sourceSkinnedMeshRenderer.sharedMesh;
                if (mesh != null)
                {
                    if (!mesh.isReadable)
                    {
                        Debug.LogError($"Cannot generate outlines for mesh {mesh.name}: it is not readable", mesh);
                    }
                }
            }
        }

        public void SetBakedLineData(BakedLineData bakedLineData)
        {
            if (outlineMesh != null)
            {
                outlineMesh.Dispose();
                outlineMesh = null;
            }

            this.bakedLineData = bakedLineData;
        }

        public void GenerateForCamera(Camera camera, in LineGenerationContext context)
        {
            if (!enabled)
                throw new Exception("Outline Renderer is disabled");

            if (camera == null)
                throw new ArgumentNullException(nameof(camera));

            EnsureOutlineMesh();

            if (outlineMesh != null)
            {
                outlineMesh.GenerateOutlines(
                    camera.transform.position,
                    camera.transform.forward,
                    transform.localToWorldMatrix,
                    context);
            }
        }

        public bool GetComputeBufferParams(out OutlineMeshComputeArgs args)
        {
            if (!enabled)
                throw new Exception("Outline Renderer is disabled");

            EnsureOutlineMesh();

            if (outlineMesh != null)
            {
                outlineMesh.GetComputeBuffersParams(
                    transform.localToWorldMatrix,
                    out args);

                return true;
            }

            args = default;
            return false;
        }

        public void BuildAccelerationStructure()
        {
            if (!enabled)
                throw new Exception("Outline Renderer is disabled");

            EnsureOutlineMesh();

            if (outlineMesh != null)
            {
                if (accel != null)
                {
                    accel.Dispose();
                    accel = null;
                }

                accel = outlineMesh.BuildAccelerationStructure();
            }
        }

        public void UpdateFromSkinningResult()
        {
            if (!enabled)
                throw new Exception("Outline Renderer is disabled");

            if (!IsSkinned)
                throw new Exception("Cannot update from skinning result on non skinned outline renderer");

            if (outlineMesh != null)
            {
                outlineMesh.UpdateFromSkinnedMesh(SkinnedMeshRenderer);
            }
        }

        public void UpdateDynamicVerticesFromSkinnedMesh(CommandBuffer cmd)
        {
            if (!enabled)
                throw new Exception("Outline Renderer is disabled");

            if (!IsSkinned)
                throw new Exception("Cannot update from skinning result on non skinned outline renderer");

            if (outlineMesh != null)
            {
                outlineMesh.UpdateDynamicVerticesFromSkinnedMesh(SkinnedMeshRenderer, cmd);
            }
        }

        void EnsureOutlineMesh()
        {
            if (outlineMesh == null && CanSetUp)
            {
                if (bakedLineData != null)
                {
                    outlineMesh = new OutlineMesh(bakedLineData);
                }
                else
                {
                    outlineMesh = new OutlineMesh(SourceMesh, IsSkinned);
                }
            }
        }
    }

    public struct OutlineMeshComputeArgs
    {
        public int numWeldedVertices;
        public int numEdges;
        public int numFaces;
        public Matrix4x4 localToWorldMatrix;
        public ComputeBuffer weldedVerticesBuffer;
        public ComputeBuffer facesBuffer;
        public ComputeBuffer halfEdgesBuffer;
        public ComputeBuffer edgesBuffer;
        public ComputeBuffer weldedVertexToSourceVertexBuffer;
        public ComputeBuffer faceNormalsBuffer;
        public ComputeBuffer dynamicSourceVerticesBuffer;
    }

    public class OutlineMesh : IDisposable
    {
        NativeArray<Vector3> weldedVertices;
        NativeArray<Face> faces;
        NativeArray<Vector3> faceNormals;
        NativeArray<HalfEdge> halfEdges;
        NativeArray<Edge> edges;
        NativeArray<int> weldedVertexToSourceVertex;

        ComputeBuffer weldedVerticesBuffer;
        ComputeBuffer facesBuffer;
        ComputeBuffer faceNormalsBuffer;
        ComputeBuffer halfEdgesBuffer;
        ComputeBuffer edgesBuffer;
        ComputeBuffer weldedVertexToSourceVertexBuffer;
        ComputeBuffer dynamicSourceVerticesBuffer;

        Mesh bakingMesh;

        BakedLineData bakedLineData;

        static readonly ProfilerMarker ConstructOutlineMeshPerfMarker = new ProfilerMarker("OutlineMesh.Construct");

        public OutlineMesh(BakedLineData bakedLineData)
        {
            if (bakedLineData == null)
                throw new ArgumentNullException(nameof(bakedLineData));

            this.bakedLineData = bakedLineData;
            bakedLineData.Load();
        }

        public OutlineMesh(Mesh sourceMesh, bool dynamic)
        {
            if (!sourceMesh.isReadable)
                throw new ArgumentException("Source mesh is not readable");

            ConstructOutlineMeshPerfMarker.Begin();

            var meshDataArray = Mesh.AcquireReadOnlyMeshData(sourceMesh);

            var meshData = meshDataArray[0];

            int numVertices = meshData.vertexCount;

            NativeArray<Vector3> vertices = new NativeArray<Vector3>(numVertices, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            NativeArray<Vector3> normals = new NativeArray<Vector3>(numVertices, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            meshData.GetVertices(vertices);
            meshData.GetNormals(normals);

            NativeList<int> indices = new NativeList<int>(1024, Allocator.TempJob);
            for (int i = 0; i < meshData.subMeshCount; i++)
            {
                var subMesh = meshData.GetSubMesh(i);
                if (subMesh.topology != MeshTopology.Triangles)
                    continue;

                NativeArray<int> temp = new NativeArray<int>(subMesh.indexCount, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
                meshData.GetIndices(temp, i);
                indices.AddRange(temp);
                temp.Dispose();
            }

            meshDataArray.Dispose();

            faces = new NativeArray<Face>(indices.Length / 3, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
            faceNormals = new NativeArray<Vector3>(indices.Length / 3, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
            halfEdges = new NativeArray<HalfEdge>(faces.Length * 3, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);

            NativeList<Vector3> weldedVerticesList = new NativeList<Vector3>(numVertices, Allocator.TempJob);
            NativeList<Edge> edgesList = new NativeList<Edge>(halfEdges.Length, Allocator.TempJob);
            NativeList<int> weldedVertexToSourceVertexList = new NativeList<int>(numVertices, Allocator.TempJob);

            new ConstructOutlineMeshJob
            {
                vertices = vertices,
                normals = normals,
                indices = indices,
                weldedVerticesList = weldedVerticesList,
                weldedVertexToSourceVertexList = weldedVertexToSourceVertexList,
                faces = faces,
                faceNormals = faceNormals,
                halfEdges = halfEdges,
                edgesList = edgesList,
            }.Schedule().Complete();

            vertices.Dispose();
            normals.Dispose();
            indices.Dispose();

            edges = new NativeArray<Edge>(edgesList.AsArray(), Allocator.Persistent);
            weldedVertices = new NativeArray<Vector3>(weldedVerticesList.AsArray(), Allocator.Persistent);
            weldedVertexToSourceVertex = new NativeArray<int>(weldedVertexToSourceVertexList.AsArray(), Allocator.Persistent);

            if (SystemInfo.supportsComputeShaders)
            {
                weldedVerticesBuffer = new ComputeBuffer(weldedVertices.Length, UnsafeUtility.SizeOf<Vector3>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                weldedVerticesBuffer.SetData(weldedVertices);

                facesBuffer = new ComputeBuffer(faces.Length, UnsafeUtility.SizeOf<Face>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                facesBuffer.SetData(faces);

                halfEdgesBuffer = new ComputeBuffer(halfEdges.Length, UnsafeUtility.SizeOf<HalfEdge>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                halfEdgesBuffer.SetData(halfEdges);

                edgesBuffer = new ComputeBuffer(edges.Length, UnsafeUtility.SizeOf<Edge>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                edgesBuffer.SetData(edges);

                faceNormalsBuffer = new ComputeBuffer(faceNormals.Length, UnsafeUtility.SizeOf<Vector3>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                faceNormalsBuffer.SetData(faceNormals);

                weldedVertexToSourceVertexBuffer = new ComputeBuffer(weldedVertexToSourceVertexList.Length, UnsafeUtility.SizeOf<int>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                weldedVertexToSourceVertexBuffer.SetData(weldedVertexToSourceVertex);

                if (dynamic)
                {
                    dynamicSourceVerticesBuffer = new ComputeBuffer(numVertices, UnsafeUtility.SizeOf<Vector3>(), ComputeBufferType.Structured, ComputeBufferMode.Dynamic);
                }
            }

            weldedVerticesList.Dispose();
            edgesList.Dispose();
            weldedVertexToSourceVertexList.Dispose();

            ConstructOutlineMeshPerfMarker.End();

            if (dynamic)
            {
                bakingMesh = new Mesh()
                {
                    name = "Outline Renderer Baked Mesh",
                    hideFlags = HideFlags.HideAndDontSave,
                };
            }
        }

#if UNITY_EDITOR
        public static BakedLineData GenerateBakedData(Mesh sourceMesh)
        {
            var meshDataArray = UnityEditor.MeshUtility.AcquireReadOnlyMeshData(sourceMesh);

            var meshData = meshDataArray[0];

            int numVertices = meshData.vertexCount;

            NativeArray<Vector3> vertices = new NativeArray<Vector3>(numVertices, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            NativeArray<Vector3> normals = new NativeArray<Vector3>(numVertices, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            meshData.GetVertices(vertices);
            meshData.GetNormals(normals);

            NativeList<int> indices = new NativeList<int>(1024, Allocator.TempJob);
            for (int i = 0; i < meshData.subMeshCount; i++)
            {
                var subMesh = meshData.GetSubMesh(i);
                if (subMesh.topology != MeshTopology.Triangles)
                    continue;

                NativeArray<int> temp = new NativeArray<int>(subMesh.indexCount, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
                meshData.GetIndices(temp, i);
                indices.AddRange(temp);
                temp.Dispose();
            }

            meshDataArray.Dispose();


            NativeArray<Face> faces = new NativeArray<Face>(indices.Length / 3, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            NativeArray<Vector3> faceNormals = new NativeArray<Vector3>(indices.Length / 3, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
            NativeArray<HalfEdge> halfEdges = new NativeArray<HalfEdge>(faces.Length * 3, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

            NativeList<Vector3> weldedVerticesList = new NativeList<Vector3>(numVertices, Allocator.TempJob);
            NativeList<Edge> edgesList = new NativeList<Edge>(halfEdges.Length, Allocator.TempJob);
            NativeList<int> weldedVertexToSourceVertexList = new NativeList<int>(numVertices, Allocator.TempJob);

            new ConstructOutlineMeshJob
            {
                vertices = vertices,
                normals = normals,
                indices = indices,
                weldedVerticesList = weldedVerticesList,
                weldedVertexToSourceVertexList = weldedVertexToSourceVertexList,
                faces = faces,
                faceNormals = faceNormals,
                halfEdges = halfEdges,
                edgesList = edgesList,
            }.Schedule().Complete();

            vertices.Dispose();
            normals.Dispose();
            indices.Dispose();

            BakedLineData result = ScriptableObject.CreateInstance<BakedLineData>();

            result._weldedVertices = weldedVerticesList.ToArray();
            result._faceNormals = faceNormals.ToArray();
            result._halfEdges = halfEdges.ToArray();
            result._edges = edgesList.ToArray();

            faces.Dispose();
            faceNormals.Dispose();
            halfEdges.Dispose();
            weldedVerticesList.Dispose();
            edgesList.Dispose();
            weldedVertexToSourceVertexList.Dispose();

            return result;
        }
#endif

        public void Dispose()
        {
            if (bakedLineData != null)
            {
                bakedLineData.Unload();
            }
            else
            {
                weldedVertices.Dispose();
                faces.Dispose();
                halfEdges.Dispose();
                edges.Dispose();
                weldedVertexToSourceVertex.Dispose();
                faceNormals.Dispose();

                if (weldedVerticesBuffer != null)
                    weldedVerticesBuffer.Dispose();

                if (facesBuffer != null)
                    facesBuffer.Dispose();

                if (halfEdgesBuffer != null)
                    halfEdgesBuffer.Dispose();

                if (edgesBuffer != null)
                    edgesBuffer.Dispose();

                if (weldedVertexToSourceVertexBuffer != null)
                    weldedVertexToSourceVertexBuffer.Dispose();

                if (faceNormalsBuffer != null)
                    faceNormalsBuffer.Dispose();

                if (dynamicSourceVerticesBuffer != null)
                    dynamicSourceVerticesBuffer.Dispose();

                if (bakingMesh != null)
                {
                    GameObject.DestroyImmediate(bakingMesh);
                }
            }
        }

        public AccelerationStructure BuildAccelerationStructure()
        {
            return new AccelerationStructure(this);
        }

        public void GetComputeBuffersParams(Matrix4x4 localToWorldMatrix, out OutlineMeshComputeArgs args)
        {
            if (bakedLineData != null)
            {
                args.numWeldedVertices = bakedLineData.weldedVertices.Length;
                args.numEdges = bakedLineData.edges.Length;
                args.numFaces = bakedLineData.faceNormals.Length;
                args.localToWorldMatrix = localToWorldMatrix;
                args.weldedVerticesBuffer = bakedLineData.weldedVerticesBuffer;
                args.facesBuffer = null;
                args.halfEdgesBuffer = bakedLineData.halfEdgesBuffer;
                args.edgesBuffer = bakedLineData.edgesBuffer;
                args.weldedVertexToSourceVertexBuffer = null;
                args.faceNormalsBuffer = bakedLineData.faceNormalsBuffer;
                args.dynamicSourceVerticesBuffer = null;
            }
            else
            {
                args.numWeldedVertices = weldedVertices.Length;
                args.numEdges = edges.Length;
                args.numFaces = faces.Length;
                args.localToWorldMatrix = localToWorldMatrix;
                args.weldedVerticesBuffer = weldedVerticesBuffer;
                args.facesBuffer = facesBuffer;
                args.halfEdgesBuffer = halfEdgesBuffer;
                args.edgesBuffer = edgesBuffer;
                args.weldedVertexToSourceVertexBuffer = weldedVertexToSourceVertexBuffer;
                args.faceNormalsBuffer = faceNormalsBuffer;
                args.dynamicSourceVerticesBuffer = dynamicSourceVerticesBuffer;
            }
        }

        public void UpdateDynamicVerticesFromSkinnedMesh(SkinnedMeshRenderer skinnedMeshRenderer, CommandBuffer cmd)
        {
            if (bakingMesh == null)
                throw new Exception("Cannot update non dynamic outline mesh");

            skinnedMeshRenderer.BakeMesh(bakingMesh);

            var meshDataArray = Mesh.AcquireReadOnlyMeshData(bakingMesh);

            var meshData = meshDataArray[0];
            NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.TempJob);
            meshData.GetVertices(vertices);

            cmd.SetComputeBufferData(dynamicSourceVerticesBuffer, vertices);

            vertices.Dispose();
            meshDataArray.Dispose();
        }

        public void UpdateFromSkinnedMesh(SkinnedMeshRenderer skinnedMeshRenderer)
        {
            if (bakingMesh == null)
                throw new Exception("Cannot update non dynamic outline mesh");

            skinnedMeshRenderer.BakeMesh(bakingMesh);

            var meshDataArray = Mesh.AcquireReadOnlyMeshData(bakingMesh);

            var meshData = meshDataArray[0];
            NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.TempJob);
            meshData.GetVertices(vertices);

            var jobHandle = new UpdateWeldedVerticesJob
            {
                vertices = vertices,
                weldedVertices = weldedVertices,
                weldedVertexToSourceVertex = weldedVertexToSourceVertex,
            }.Schedule(weldedVertices.Length, 256);

            jobHandle = new UpdateFaceNormalsJob
            {
                weldedVertices = weldedVertices,
                faces = faces,
                faceNormals = faceNormals,
            }.Schedule(faces.Length, 256, jobHandle);

            jobHandle.Complete();

            vertices.Dispose();
            meshDataArray.Dispose();
        }

        [BurstCompile]
        struct UpdateWeldedVerticesJob : IJobParallelFor
        {
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> vertices;

            [NativeMatchesParallelForLength]
            public NativeArray<Vector3> weldedVertices;

            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<int> weldedVertexToSourceVertex;

            public void Execute(int index)
            {
                weldedVertices[index] = vertices[weldedVertexToSourceVertex[index]];
            }
        }

        [BurstCompile]
        struct UpdateFaceNormalsJob : IJobParallelFor
        {
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> weldedVertices;

            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<Face> faces;

            [NativeMatchesParallelForLength]
            public NativeArray<Vector3> faceNormals;

            public void Execute(int index)
            {
                Face face = faces[index];
                Vector3 normal = math.normalize(math.cross(weldedVertices[face.v2] - weldedVertices[face.v1], weldedVertices[face.v3] - weldedVertices[face.v1]));
                faceNormals[index] = normal;
            }
        }

        [BurstCompile]
        struct ConstructOutlineMeshJob : IJob
        {
            [ReadOnly]
            public NativeArray<Vector3> vertices;
            [ReadOnly]
            public NativeArray<Vector3> normals;
            [ReadOnly]
            public NativeArray<int> indices;

            public NativeList<Vector3> weldedVerticesList;
            public NativeList<int> weldedVertexToSourceVertexList;
            public NativeArray<Face> faces;
            public NativeArray<Vector3> faceNormals;
            public NativeArray<HalfEdge> halfEdges;
            public NativeList<Edge> edgesList;

            public void Execute()
            {
                NativeArray<int> vertexToWeldedVertex = new NativeArray<int>(vertices.Length, Allocator.Temp);
                NativeHashMap<Vector3, int> weldingMap = new NativeHashMap<Vector3, int>(vertices.Length, Allocator.Temp);
                for (int i = 0; i < vertices.Length; i++)
                {
                    Vector3 vertex = vertices[i];
                    if (!weldingMap.TryGetValue(vertex, out int index))
                    {
                        index = weldedVerticesList.Length;
                        weldedVerticesList.Add(vertex);
                        weldedVertexToSourceVertexList.Add(i);
                        weldingMap.Add(vertex, index);
                    }

                    vertexToWeldedVertex[i] = index;
                }

                NativeArray<Face> unweldedFaces = new NativeArray<Face>(faces.Length, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
                for (int i = 0; i < faces.Length; i++)
                {
                    int i1 = indices[i * 3 + 0];
                    int i2 = indices[i * 3 + 1];
                    int i3 = indices[i * 3 + 2];

                    int v1 = vertexToWeldedVertex[i1];
                    int v2 = vertexToWeldedVertex[i2];
                    int v3 = vertexToWeldedVertex[i3];

                    faces[i] = new Face
                    {
                        v1 = v1,
                        v2 = v2,
                        v3 = v3,
                    };

                    unweldedFaces[i] = new Face
                    {
                        v1 = i1,
                        v2 = i2,
                        v3 = i3,
                    };

                    faceNormals[i] = math.normalize(math.cross(weldedVerticesList[v2] - weldedVerticesList[v1], weldedVerticesList[v3] - weldedVerticesList[v1]));
                }

                NativeHashMap<HalfEdge, int> halfEdgeMap = new NativeHashMap<HalfEdge, int>(halfEdges.Length, Allocator.Temp);
                NativeArray<Vector3> halfEdgeNormals = new NativeArray<Vector3>(halfEdges.Length, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
                for (int i = 0; i < faces.Length; i++)
                {
                    Face face = faces[i];
                    Face unweldedFace = unweldedFaces[i];

                    HalfEdge e1 = new HalfEdge
                    {
                        v1 = face.v1,
                        v2 = face.v2,
                        face = i
                    };
                    HalfEdge e2 = new HalfEdge
                    {
                        v1 = face.v2,
                        v2 = face.v3,
                        face = i
                    };
                    HalfEdge e3 = new HalfEdge
                    {
                        v1 = face.v3,
                        v2 = face.v1,
                        face = i
                    };

                    int idx1 = i * 3 + 0;
                    int idx2 = i * 3 + 1;
                    int idx3 = i * 3 + 2;

                    halfEdges[idx1] = e1;
                    halfEdges[idx2] = e2;
                    halfEdges[idx3] = e3;

                    halfEdgeNormals[idx1] = math.normalize((normals[unweldedFace.v1] + normals[unweldedFace.v2]) * 0.5f);
                    halfEdgeNormals[idx2] = math.normalize((normals[unweldedFace.v2] + normals[unweldedFace.v3]) * 0.5f);
                    halfEdgeNormals[idx3] = math.normalize((normals[unweldedFace.v3] + normals[unweldedFace.v1]) * 0.5f);

                    if (!halfEdgeMap.TryAdd(e1, idx1))
                    {
                        //Debug.LogError($"Degenerate mesh detected: more than one edge between vertices {e1.v1} and {e1.v2}");
                    }

                    if (!halfEdgeMap.TryAdd(e2, idx2))
                    {
                        //Debug.LogError($"Degenerate mesh detected: more than one edge between vertices {e2.v1} and {e2.v2}");
                    }

                    if (!halfEdgeMap.TryAdd(e3, idx3))
                    {
                        //Debug.LogError($"Degenerate mesh detected: more than one edge between vertices {e3.v1} and {e3.v2}");
                    }
                }

                foreach (HalfEdge hEdge in halfEdges)
                {
                    if (!halfEdgeMap.TryGetValue(hEdge, out int e1))
                    {
                        // already processed
                        continue;
                    }
                    halfEdgeMap.Remove(hEdge);
                    bool sharp = false;

                    int edgeIndex = edgesList.Length;
                    halfEdges.ElementAt(e1).edge = edgeIndex;

                    HalfEdge other = new HalfEdge { v1 = hEdge.v2, v2 = hEdge.v1 };
                    // Look up other half edge.
                    // If we find it remove it from the temp half edges
                    // array, so the same edge is not added twice.
                    if (halfEdgeMap.TryGetValue(other, out int e2))
                    {
                        halfEdgeMap.Remove(other);
                        float3 n1 = halfEdgeNormals[e1];
                        float3 n2 = halfEdgeNormals[e2];

                        if (math.dot(n1, n2) < 0.999)
                        {
                            sharp = true;
                        }

                        halfEdges.ElementAt(e2).edge = edgeIndex;
                    }
                    else
                    {
                        // An open edge has -1 as the second edge index
                        e2 = -1;
                    }

                    var edge = new Edge { e1 = e1, e2 = e2, sharp = sharp ? 1 : 0 };
                    edgesList.Add(edge);
                }

                halfEdgeMap.Dispose();
                unweldedFaces.Dispose();
                halfEdgeNormals.Dispose();
                vertexToWeldedVertex.Dispose();
                weldingMap.Dispose();
            }
        }



        [BurstCompile]
        struct GenerateLinesJob : IJobParallelFor
        {
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> weldedVertices;
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> faceNormals;
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<HalfEdge> halfEdges;
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<Edge> edges;

            [WriteOnly]
            public NativeList<LineGeneration.Line>.ParallelWriter lines;

            public Vector3 localCameraPosition;
            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                Edge edge = edges[index];
                HalfEdge e1 = halfEdges[edge.e1];

                bool generate = false;
                if (edge.e2 == -1)
                {
                    // An open edge will always generate an outline when it is visible
                    Vector3 faceNormal = faceNormals[e1.face];
                    Vector3 faceVertex = weldedVertices[e1.v1];

                    // The face defines a plane.
                    // The face is visible in a perspective camera if the camera
                    // position is on the positive side of the plane.
                    bool faceVisible = new Plane(faceNormal, faceVertex).GetSide(localCameraPosition);

                    generate = faceVisible;
                }
                else
                {
                    // A closed edge will only generate an outline when one face is visible and the other is not
                    HalfEdge e2 = halfEdges[edge.e2];

                    Vector3 face1Normal = faceNormals[e1.face];
                    Vector3 face1Vertex = weldedVertices[e1.v1];
                    bool face1Visible = new Plane(face1Normal, face1Vertex).GetSide(localCameraPosition);

                    Vector3 face2Normal = faceNormals[e2.face];
                    Vector3 face2Vertex = weldedVertices[e2.v1];
                    bool face2Visible = new Plane(face2Normal, face2Vertex).GetSide(localCameraPosition);

                    generate = face1Visible != face2Visible;
                }

                // always generate an edge when it is sharp
                if (generate || edge.sharp != 0)
                {
                    Vector3 v1 = math.transform(localToWorldMatrix, weldedVertices[e1.v1]);
                    Vector3 v2 = math.transform(localToWorldMatrix, weldedVertices[e1.v2]);

                    lines.AddNoResize(new LineGeneration.Line { p1 = v1, p2 = v2 });
                }
            }
        }

        static readonly ProfilerMarker GenerateOutlinesPerfMarker = new ProfilerMarker("OutlineMesh.GenerateOutlines");
        public void GenerateOutlines(Vector3 cameraPosition, Vector3 cameraDirection, Matrix4x4 localToWorldMatrix, LineGenerationContext context)
        {
            GenerateOutlinesPerfMarker.Begin();

            NativeArray<Vector3> weldedVertices;
            NativeArray<Vector3> faceNormals;
            NativeArray<HalfEdge> halfEdges;
            NativeArray<Edge> edges;
            if (bakedLineData != null)
            {
                weldedVertices = bakedLineData.weldedVertices;
                faceNormals = bakedLineData.faceNormals;
                halfEdges = bakedLineData.halfEdges;
                edges = bakedLineData.edges;
            }
            else
            {
                weldedVertices = this.weldedVertices;
                faceNormals = this.faceNormals;
                halfEdges = this.halfEdges;
                edges = this.edges;
            }

            NativeList<LineGeneration.Line> lines = new NativeList<LineGeneration.Line>(edges.Length, Allocator.TempJob);

            float4x4 worldToLocalMatrix = math.inverse(localToWorldMatrix);
            float3 localCameraPosition = math.transform(worldToLocalMatrix, cameraPosition);

            new GenerateLinesJob
            {
                weldedVertices = weldedVertices,
                faceNormals = faceNormals,
                halfEdges = halfEdges,
                edges = edges,
                lines = lines.AsParallelWriter(),
                localCameraPosition = localCameraPosition,
                localToWorldMatrix = localToWorldMatrix,
            }.Schedule(edges.Length, 256).Complete();

            int numVertices = lines.Length * 4;
            int numIndices = lines.Length * 6;

            if (context.vertexData.Length + numVertices > context.vertexData.Capacity)
            {
                context.vertexData.Capacity = Mathf.Max(2 * context.vertexData.Length, context.vertexData.Length + numVertices);
            }

            if (context.indices.Length + numIndices > context.indices.Capacity)
            {
                context.indices.Capacity = Mathf.Max(2 * context.indices.Length, context.indices.Length + numIndices);
            }

            new LineGeneration.GenerateLineDataJob
            {
                lines = lines,
                indices = context.indices,
                vertexData = context.vertexData,
                indexOffset = context.vertexData.Length,
            }.Schedule().Complete();

            lines.Dispose();

            GenerateOutlinesPerfMarker.End();
        }

        public class AccelerationStructure : IDisposable
        {
            NativeBVH<ConnectedFace> bvh;

            public AccelerationStructure(OutlineMesh outlineMesh)
            {
                NativeArray<Face> faces = outlineMesh.faces;
                NativeArray<HalfEdge> halfEdges = outlineMesh.halfEdges;
                NativeArray<Edge> edges = outlineMesh.edges;
                NativeArray<Vector3> weldedVertices = outlineMesh.weldedVertices;

                NativeArray<ConnectedFace> connectedFaces = new NativeArray<ConnectedFace>(faces.Length, Allocator.Temp);
                for (int i = 0; i < faces.Length; i++)
                {
                    Face f = faces[i];
                    var connectedFace = new ConnectedFace
                    {
                        v1 = weldedVertices[f.v1],
                        v2 = weldedVertices[f.v2],
                        v3 = weldedVertices[f.v3],
                        f1 = -1,
                        f2 = -1,
                        f3 = -1,
                    };

                    Edge edge1 = edges[halfEdges[i * 3 + 0].edge];
                    if (edge1.e2 != -1)
                    {
                        int otherHalfEdge = edge1.e1 == i * 3 + 0 ? edge1.e2 : edge1.e1;
                        connectedFace.f1 = halfEdges[otherHalfEdge].face;
                    }

                    Edge edge2 = edges[halfEdges[i * 3 + 1].edge];
                    if (edge2.e2 != -1)
                    {
                        int otherHalfEdge = edge2.e1 == i * 3 + 1 ? edge2.e2 : edge2.e1;
                        connectedFace.f2 = halfEdges[otherHalfEdge].face;
                    }

                    Edge edge3 = edges[halfEdges[i * 3 + 2].edge];
                    if (edge3.e2 != -1)
                    {
                        int otherHalfEdge = edge3.e1 == i * 3 + 2 ? edge3.e2 : edge3.e1;
                        connectedFace.f3 = halfEdges[otherHalfEdge].face;
                    }

                    connectedFaces[i] = connectedFace;
                }

                bvh = new NativeBVH<ConnectedFace>(connectedFaces, Allocator.Persistent);
                connectedFaces.Dispose();

                bvh.Build(4);
            }

            public void Dispose()
            {
                bvh.Dispose();
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        struct ConnectedFace : IPrimitive
        {
            /// <summary>
            /// The first vertex.
            /// </summary>
            public Vector3 v1;

            /// <summary>
            /// The second vertex.
            /// </summary>
            public Vector3 v2;

            /// <summary>
            /// The third vertex.
            /// </summary>
            public Vector3 v3;

            /// <summary>
            /// Index of the first connected face.
            /// </summary>
            public int f1;

            /// <summary>
            /// Index of the second connected face.
            /// </summary>
            public int f2;

            /// <summary>
            /// Index of the third connected face.
            /// </summary>
            public int f3;

            public Bounds GetBounds()
            {
                Vector3 min = Vector3.Min(Vector3.Min(v1, v2), v3);
                Vector3 max = Vector3.Max(Vector3.Max(v1, v2), v3);
                return new Bounds((min + max) * 0.5f, max - min);
            }

            public Vector3 GetCentroid()
            {
                return (v1 + v2 + v3) * (1 / 3.0f);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        [Serializable]
        public struct Face
        {
            /// <summary>
            /// Index of the first vertex.
            /// </summary>
            public int v1;

            /// <summary>
            /// Index of the second vertex.
            /// </summary>
            public int v2;

            /// <summary>
            /// Index of the third vertex.
            /// </summary>
            public int v3;
        }

        [StructLayout(LayoutKind.Sequential)]
        [Serializable]
        public struct HalfEdge : IEquatable<HalfEdge>
        {
            /// <summary>
            /// Index of the first vertex.
            /// </summary>
            public int v1;

            /// <summary>
            /// Index of the second vertex.
            /// </summary>
            public int v2;

            /// <summary>
            /// Index of the face this half edge belongs to.
            /// </summary>
            public int face;

            /// <summary>
            /// Index of the edge this half edge belongs to.
            /// </summary>
            public int edge;

            public bool Equals(HalfEdge other)
            {
                return v1 == other.v1 && v2 == other.v2;
            }

            public override int GetHashCode()
            {
                return v1.GetHashCode() ^ (v2.GetHashCode() << 2);
            }

            public override bool Equals(object obj)
            {
                return obj is HalfEdge other && this.Equals(other);
            }
        }

        [StructLayout(LayoutKind.Sequential)]
        [Serializable]
        public struct Edge
        {
            /// <summary>
            /// Index of the first half edge
            /// </summary>
            public int e1;

            /// <summary>
            /// Index of the second half edge.
            /// If this is -1 the edge is considered open (boundary).
            /// </summary>
            public int e2;

            public int sharp;
        }
    }
}