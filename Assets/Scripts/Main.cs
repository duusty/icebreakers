using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Icebreakers
{
    public class Main : MonoBehaviour
    {
        [SerializeField]
        Camera mainCamera;

        Panel currentlyInsidePanel;

        Vector3 lastCameraPosition;

        Storyboard storyboard;

        StoryboardPage currentStorybardPage;
        Scene pageScene;
        Page activePage;

        bool isLoading;

        List<GameObject> rootGameObjects = new List<GameObject>();

        float t;

        void Start()
        {
            lastCameraPosition = mainCamera.transform.position;

            storyboard = Resources.Load<Storyboard>("Storyboard");
            if (storyboard == null)
            {
                Debug.LogError("[Main] Failed to load storyboard: no resource with name \"Storyboard\"");
            }

#if UNITY_EDITOR
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                foreach (var page in storyboard.Pages)
                {
                    if (page.runtimeSceneIndex == scene.buildIndex)
                    {
                        pageScene = scene;
                        currentStorybardPage = page;
                        break;
                    }
                }
            }

            if (currentStorybardPage != null)
            {
                SceneManager.SetActiveScene(pageScene);

                pageScene.GetRootGameObjects(rootGameObjects);
                foreach (var gameObject in rootGameObjects)
                {
                    if (gameObject.TryGetComponent<Page>(out Page page))
                    {
                        activePage = page;
                        break;
                    }
                }
            }
#endif

            if (currentStorybardPage == null)
            {
                if (storyboard.Pages.Count > 0)
                {
                    StartCoroutine(LoadPage(storyboard.Pages[0]));
                }
            }
            else
            {
                t = 0;
                InitializePanelAndBubbleVisibility();
            }
        }

        IEnumerator LoadPage(StoryboardPage storyboardPage)
        {
            if (isLoading)
            {
                throw new Exception("Already loading");
            }

            isLoading = true;

            if (currentStorybardPage != null)
            {
                currentStorybardPage = null;
                activePage = null;
                Scene toUnload = pageScene;
                pageScene = default;
                yield return SceneManager.UnloadSceneAsync(toUnload);
            }

            yield return SceneManager.LoadSceneAsync(storyboardPage.runtimeSceneIndex, LoadSceneMode.Additive);
            pageScene = SceneManager.GetSceneByBuildIndex(storyboardPage.runtimeSceneIndex);
            currentStorybardPage = storyboardPage;
            activePage = null;
            pageScene.GetRootGameObjects(rootGameObjects);
            foreach (var gameObject in rootGameObjects)
            {
                if (gameObject.TryGetComponent<Page>(out Page page))
                {
                    activePage = page;
                    break;
                }
            }

            if (activePage == null)
            {
                Debug.LogError($"[Main] Could not find Page GameObject for scene {storyboardPage.title}\nNote that it must be at the root of the scene!");
            }

            t = 0;
            InitializePanelAndBubbleVisibility();

            isLoading = false;
        }

        public void NextPage()
        {
            if (isLoading)
            {
                Debug.LogError("[Main] Cannot go to next page, currently loading");
                return;
            }

            int index = 0;
            foreach (var page in storyboard.Pages)
            {
                if (page == currentStorybardPage)
                {
                    break;
                }
                index++;
            }

            if (index == storyboard.Pages.Count - 1)
            {
                Debug.LogError("[Main] Cannot go to next page, we have no active page");
                return;
            }

            if (index == storyboard.Pages.Count - 1)
            {
                Debug.LogError("[Main] Cannot go to next page, we are at the last page");
                return;
            }

            StartCoroutine(LoadPage(storyboard.Pages[index + 1]));
        }

        void Update()
        {
            t += Time.deltaTime;

            UpdatePanelAndBubbleVisibility();
        }

        void OnNextPage()
        {
            NextPage();
        }

        void InitializePanelAndBubbleVisibility()
        {
            if (activePage == null)
                return;

            foreach (var panel in activePage.Panels)
            {
                if (panel.showAfter != 0)
                {
                    panel.gameObject.SetActive(false);
                }
                else
                {
                    panel.gameObject.SetActive(true);
                }

                foreach (var speechBubble in panel.SpeechBubbles)
                {
                    if (speechBubble.showAfter != 0)
                    {
                        speechBubble.gameObject.SetActive(false);
                    }

                    foreach (var bubble in speechBubble.bubbles)
                    {
                        if (speechBubble.showAfter + bubble.delay != 0)
                        {
                            speechBubble.HideBubble(bubble);
                        }
                    }
                }
            }
        }

        void UpdatePanelAndBubbleVisibility()
        {
            if (activePage == null)
                return;

            foreach (var panel in activePage.Panels)
            {
                if (!panel.gameObject.activeSelf && t >= panel.showAfter)
                {
                    panel.gameObject.SetActive(true);
                }

                foreach (var speechBubble in panel.SpeechBubbles)
                {
                    if (!speechBubble.gameObject.activeSelf && t >= speechBubble.showAfter)
                    {
                        speechBubble.gameObject.SetActive(true);
                    }

                    foreach (var bubble in speechBubble.bubbles)
                    {
                        if (!bubble.isVisible && t >= speechBubble.showAfter + bubble.delay)
                        {
                            speechBubble.ShowBubble(bubble);
                        }
                    }
                }
            }
        }

        void LateUpdate()
        {
            // XXX: When moving closer to the panel than the near clipping plane
            // the panel will not be rendered correctly. We might want to switch
            // to the inside earlier so that the transition is not visible.

            Vector3 currentCameraPosition = mainCamera.transform.position;

            Ray ray = new Ray(lastCameraPosition, currentCameraPosition - lastCameraPosition);
            float maxDistance = Vector3.Distance(currentCameraPosition, lastCameraPosition);

            if (currentlyInsidePanel != null)
            {
                if (currentlyInsidePanel.IntersectWithPanel(ray, maxDistance, out PanelIntersection intersection))
                {
                    if (!intersection.front)
                    {
                        currentlyInsidePanel.IsInside = false;
                        currentlyInsidePanel = null;
                    }
                }
            }

            if (activePage != null && currentlyInsidePanel == null)
            {
                foreach (var panel in activePage.Panels)
                {
                    if (panel.IntersectWithPanel(ray, maxDistance, out PanelIntersection intersection))
                    {
                        if (intersection.front)
                        {
                            currentlyInsidePanel = panel;
                            panel.IsInside = true;
                            break;
                        }
                    }
                }
            }

            lastCameraPosition = currentCameraPosition;
        }
    }
}