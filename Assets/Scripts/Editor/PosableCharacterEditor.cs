using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Icebreakers.Editor
{
    [CustomEditor(typeof(PosableCharacter))]
    public class PosableCharacterEditor : UnityEditor.Editor
    {
        bool showAdvanced;
        PosingTool posingTool;

        void OnEnable()
        {
            posingTool = new PosingTool();
        }

        void OnDisable()
        {
            if (posingTool.IsInPoseMode)
            {
                posingTool.LeavePoseMode();
            }
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button(posingTool.IsInPoseMode ? "Leave Pose Mode" : "Enter Pose Mode"))
            {
                if (posingTool.IsInPoseMode)
                {
                    posingTool.LeavePoseMode();
                }
                else
                {
                    posingTool.EnterPoseMode((PosableCharacter)target);
                }
            }

            if (GUILayout.Button("Restore Rest Pose"))
            {
                PosingTool.RestoreRestPose((PosableCharacter)target);
            }

            showAdvanced = EditorGUILayout.Foldout(showAdvanced, "Advanced");
            if (showAdvanced)
            {
                if (GUILayout.Button("Initialize Pose Bones"))
                {
                    var posableCharacter = (PosableCharacter)target;
                    Undo.RecordObject(posableCharacter, "Initialize Pose Bones");
                    posableCharacter.CreatePoseBones();
                }


                if (GUILayout.Button("Auto Set Bone Lengths"))
                {
                    var posableCharacter = (PosableCharacter)target;
                    Undo.RecordObject(posableCharacter, "Auto Set Bone Lengths");
                    posableCharacter.AutoSetBoneLengths();
                }
            }
        }
    }
}