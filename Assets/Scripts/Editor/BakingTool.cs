using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;

using Object = UnityEngine.Object;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Icebreakers.Editor
{
    public class BakingTool : IDisposable
    {
        [MenuItem("Tools/Icebreakers/Bake Open Scene")]
        public static void BakeOpenScene()
        {
            SceneAsset targetScene = null;
            for (int i = 0; i < EditorSceneManager.sceneCount; i++)
            {
                Scene scene = EditorSceneManager.GetSceneAt(i);
                foreach (var root in scene.GetRootGameObjects())
                {
                    if (root.TryGetComponent<Page>(out _))
                    {
                        SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                        if (sceneAsset != null)
                        {
                            targetScene = sceneAsset;
                            break;
                        }
                    }
                }
            }

            if (targetScene == null)
            {
                EditorUtility.DisplayDialog("Error", "Cannot bake scene. No valid scene open\nThere needs to be a scene that contains a page", "Ok");
                return;
            }

            if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                return;
            }

            using (BakingTool bakingTool = new BakingTool())
            {
                bakingTool.BakeScene(targetScene);
            }
        }

        const int atlasSize = 4096;
        const GraphicsFormat atlasFormat = GraphicsFormat.R8G8B8A8_SRGB;

        List<(string, Object)> errors = new List<(string, Object)>();

        public BakingTool()
        {

        }

        public void Dispose()
        {

        }

        public void BakeScene(SceneAsset sceneAsset)
        {
            var oldSetup = EditorSceneManager.GetSceneManagerSetup();
            EditorUtility.DisplayProgressBar("Baking Scene", "Starting", 0);

            errors.Clear();

            try
            {
                string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
                string bakingFolder = Path.Combine(Path.GetDirectoryName(scenePath), Path.GetFileNameWithoutExtension(scenePath));
                string bakedScenePath = Path.Combine(bakingFolder, Path.GetFileNameWithoutExtension(scenePath) + "_baked.unity");
                try
                {
                    Directory.Delete(bakingFolder, true);
                }
                catch (DirectoryNotFoundException)
                {
                }

                Directory.CreateDirectory(bakingFolder);

                SceneSetup[] sceneSetup = new SceneSetup[1]
                {
                    new SceneSetup
                    {
                        path = AssetDatabase.GetAssetPath(sceneAsset),
                        isLoaded = true,
                        isActive = true,
                    }
                };
                EditorSceneManager.RestoreSceneManagerSetup(sceneSetup);

                Scene scene = EditorSceneManager.GetSceneAt(0);



                UnpackPrefabs(scene.GetRootGameObjects());

                Page page = null;
                foreach (GameObject root in scene.GetRootGameObjects())
                {
                    if (root.TryGetComponent<Page>(out Page p))
                    {
                        page = p;
                    }
                }

                if (page == null)
                {
                    ReportError("No page found");
                    return;
                }
                else
                {
                    CheckPageTransforms(page);
                }

                List<PosableCharacter> posableCharacters = new List<PosableCharacter>();
                posableCharacters.AddRange(Object.FindObjectsOfType<PosableCharacter>(includeInactive: true));

                List<Mesh> bakedMeshes = new List<Mesh>();

                BakePosableCharacters(posableCharacters, bakedMeshes, bakingFolder);

                List<OutlineRenderer> outlineRenderers = new List<OutlineRenderer>();
                outlineRenderers.AddRange(Object.FindObjectsOfType<OutlineRenderer>(includeInactive: true));

                BakeLineData(outlineRenderers, bakingFolder);

                List<MeshRenderer> renderers = new List<MeshRenderer>();
                renderers.AddRange(Object.FindObjectsOfType<MeshRenderer>(includeInactive: true));

                List<TextureAtlas> textureAtlases = CreateTextureAtlases(renderers);

                BakeTextures(textureAtlases, page);

                SaveTextureAtlases(textureAtlases, bakingFolder);
                ApplyTextureAtlases(textureAtlases, bakingFolder);

                RemoveMaterialOverrides(renderers);

                EditorSceneManager.SaveScene(scene, bakedScenePath);

                AssetDatabase.Refresh();
            }
            finally
            {
                EditorUtility.ClearProgressBar();
                EditorSceneManager.RestoreSceneManagerSetup(oldSetup);
            }
        }

        void UnpackPrefabs(GameObject[] roots)
        {
            void UnpackRecursively(GameObject gameObject)
            {
                if (PrefabUtility.IsAnyPrefabInstanceRoot(gameObject))
                {
                    PrefabUtility.UnpackPrefabInstance(gameObject, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                }

                foreach (Transform child in gameObject.transform)
                {
                    UnpackRecursively(child.gameObject);
                }
            }

            foreach (var gameObject in roots)
            {
                UnpackRecursively(gameObject);
            }
        }

        class TextureAtlas
        {
            public Texture2D texture;
            public List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
        }

        class TextureAtlasEntry
        {
            public TextureAtlas atlas;
            public MeshRenderer renderer;
            public Rect atlasRect;
        }

        void RemoveMaterialOverrides(List<MeshRenderer> renderers)
        {
            foreach (var renderer in renderers)
            {
                if (renderer.TryGetComponent<MaterialOverride>(out var materialOverride))
                {
                    Object.DestroyImmediate(materialOverride);
                }
            }
        }

        void SaveTextureAtlases(List<TextureAtlas> textureAtlases, string bakingFolder)
        {
            for (int i = 0; i < textureAtlases.Count; i++)
            {
                TextureAtlas entry = textureAtlases[i];
                string path = Path.Combine(bakingFolder, $"BakedTexture_{i}.png");
                byte[] encoded = entry.texture.EncodeToPNG();
                Object.DestroyImmediate(entry.texture);
                File.WriteAllBytes(path, encoded);
                AssetDatabase.ImportAsset(path);
                entry.texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
                TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
                textureImporter.textureType = TextureImporterType.Default;
                textureImporter.wrapMode = TextureWrapMode.Clamp;
                textureImporter.filterMode = FilterMode.Trilinear;
                textureImporter.SaveAndReimport();
            }
        }

        void ApplyTextureAtlases(List<TextureAtlas> textureAtlases, string bakingFolder)
        {
            List<Material> bakedMaterials = new List<Material>();

            Shader standardShader = Shader.Find("Icebreakers/Standard");
            if (standardShader == null)
                throw new Exception("Cannot find standard shader");

            Shader bakedShader = Shader.Find("Icebreakers/Baked");

            foreach (var textureAtlas in textureAtlases)
            {
                foreach (var entry in textureAtlas.entries)
                {
                    Material[] materials = entry.renderer.sharedMaterials;

                    for (int i = 0; i < materials.Length; i++)
                    {
                        Material material = materials[i];
                        if (material.shader == standardShader)
                        {
                            material = new Material(bakedShader) { name = $"BakedMaterial_{bakedMaterials.Count}" };
                            material.SetTexture("_BakedTexture", textureAtlas.texture);
                            material.SetVector("_BakedRect", new Vector4(entry.atlasRect.x, entry.atlasRect.y, entry.atlasRect.width, entry.atlasRect.height));

                            bakedMaterials.Add(material);
                            AssetDatabase.CreateAsset(material, Path.Combine(bakingFolder, material.name + ".mat"));
                        }

                        materials[i] = material;
                    }

                    entry.renderer.sharedMaterials = materials;
                }
            }
        }

        List<TextureAtlas> CreateTextureAtlases(List<MeshRenderer> renderers)
        {
            List<TextureAtlas> result = new List<TextureAtlas>();

            TextureAtlas currentAtlas = null;
            float x = 0;
            float y = 0;
            foreach (var renderer in renderers)
            {
                if (currentAtlas == null)
                {
                    currentAtlas = new TextureAtlas();
                    currentAtlas.texture = new Texture2D(atlasSize, atlasSize, atlasFormat, TextureCreationFlags.None);
                    x = 0;
                    y = 0;
                    result.Add(currentAtlas);
                }

                // TODO: good packing
                Rect atlasRect = new Rect(x, y, 0.25f, 0.25f);

                currentAtlas.entries.Add(new TextureAtlasEntry
                {
                    atlas = currentAtlas,
                    renderer = renderer,
                    atlasRect = atlasRect,
                });

                x += 0.25f;
                if (x > 0.99f)
                {
                    x = 0;
                    y += 0.25f;
                }

                if (y > 0.99f)
                {
                    currentAtlas = null;
                }
            }

            return result;
        }

        class BakeSet
        {
            public Panel panel;
            public List<TextureAtlasEntry> entries;
        }

        void BakeTextures(List<TextureAtlas> textureAtlases, Page page)
        {
            List<BakeSet> sets = new List<BakeSet>();
            if (page == null)
            {
                List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                foreach (TextureAtlas textureAtlas in textureAtlases)
                {
                    entries.AddRange(textureAtlas.entries);
                }
                sets.Add(new BakeSet { panel = null, entries = entries});
            }
            else
            {
                foreach (var panel in page.Panels)
                {
                    List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                    uint layer = 1u << panel.PanelNumber;
                    foreach (TextureAtlas textureAtlas in textureAtlases)
                    {
                        foreach (TextureAtlasEntry entry in textureAtlas.entries)
                        {
                            if (entry.renderer.renderingLayerMask == layer)
                            {
                                entries.Add(entry);
                            }
                        }
                    }

                    if (entries.Count > 0)
                    {
                        sets.Add(new BakeSet { panel = panel, entries = entries });
                    }
                }

                {
                    List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                    uint layer = 1;
                    foreach (TextureAtlas textureAtlas in textureAtlases)
                    {
                        foreach (TextureAtlasEntry entry in textureAtlas.entries)
                        {
                            if (entry.renderer.renderingLayerMask == layer)
                            {
                                entries.Add(entry);
                            }
                        }
                    }

                    if (entries.Count > 0)
                    {
                        sets.Add(new BakeSet { panel = null, entries = entries });
                    }
                }
            }

            Dictionary<TextureAtlas, RenderTexture> atlasRenderTextures = new Dictionary<TextureAtlas, RenderTexture>();
            foreach (var textureAtlas in textureAtlases)
            {
                var descriptor = new RenderTextureDescriptor(textureAtlas.texture.width, textureAtlas.texture.height, textureAtlas.texture.graphicsFormat, 0, 0);
                atlasRenderTextures.Add(textureAtlas, new RenderTexture(descriptor));
            }

            Shader standardShader = Shader.Find("Icebreakers/Standard");
            if (standardShader == null)
                throw new Exception("Cannot find standard shader");

            int passIndex = -1;
            var lightModeId = new ShaderTagId("LightMode");
            var bakePassId = new ShaderTagId("Bake");
            for (int i = 0; i < standardShader.passCount; i++)
            {
                if (standardShader.FindPassTagValue(i, new ShaderTagId("LightMode")) == bakePassId)
                {
                    passIndex = i;
                    break;
                }
            }

            if (passIndex == -1)
                throw new Exception("Cannot bake pass");

            CommandBuffer cmd = new CommandBuffer();
            foreach (var renderTexture in atlasRenderTextures.Values)
            {
                cmd.SetRenderTarget(renderTexture);
                cmd.SetViewport(new Rect(0, 0, renderTexture.width, renderTexture.height));
                cmd.ClearRenderTarget(true, true, new Color(1, 0, 1, 0));
            }

            RenderPipeline.SetGlobalVariables(cmd, page, true);

            foreach (var set in sets)
            {
                RenderPipeline.SetPerPanelVariables(set.panel, cmd);

                foreach (var entry in set.entries)
                {
                    MeshFilter filter = entry.renderer.GetComponent<MeshFilter>();
                    if (filter == null || filter.sharedMesh == null)
                        continue;

                    Mesh mesh = filter.sharedMesh;

                    RenderTexture renderTexture = atlasRenderTextures[entry.atlas];
                    cmd.SetRenderTarget(renderTexture);
                    cmd.SetViewport(new Rect(0, 0, renderTexture.width, renderTexture.height));
                    cmd.SetGlobalVector("_BakeRect", new Vector4(entry.atlasRect.x, entry.atlasRect.y, entry.atlasRect.width, entry.atlasRect.height));

                    Material[] materials = entry.renderer.sharedMaterials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        Material material = materials[i];
                        int subMeshIndex = entry.renderer.subMeshStartIndex + i;
                        if (subMeshIndex < mesh.subMeshCount)
                        {
                            cmd.DrawRenderer(entry.renderer, material, subMeshIndex, passIndex);
                        }
                    }
                }
            }

            Graphics.ExecuteCommandBuffer(cmd);
            cmd.Dispose();

            foreach (var textureAtlas in textureAtlases)
            {
                RenderTexture renderTexture = atlasRenderTextures[textureAtlas];
                Graphics.SetRenderTarget(renderTexture);
                textureAtlas.texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0, false);
                Graphics.SetRenderTarget(null);
                Object.DestroyImmediate(renderTexture);
            }

            for (int i = 0; i < textureAtlases.Count; i++)
            {
                TextureAtlas textureAtlas = textureAtlases[i];
                EditorUtility.DisplayProgressBar("Baking Scene", $"Dilating Textures", (float)i / textureAtlases.Count);

                NativeArray<Color32> pixels = textureAtlas.texture.GetPixelData<Color32>(0);
                int width = textureAtlas.texture.width;
                int height = textureAtlas.texture.height;

                NativeList<int> indices = new NativeList<int>(Allocator.TempJob);
                indices.Capacity = width * height;

                NativeList<int> outIndices = new NativeList<int>(Allocator.TempJob);

                new ClassifyPixelsJob
                {
                    pixels = pixels,
                    outIndices = indices.AsParallelWriter(),
                }.Schedule(pixels.Length, width).Complete();

                // Number of indices can only shrink from this point on,
                // so we need to set the capacity only once.
                outIndices.Capacity = indices.Length;

                const int maxIterations = 64;
                for (int j = 0; j < maxIterations; j++)
                {
                    if (indices.Length == 0)
                        break;

                    new DilateJob
                    {
                        pixels = pixels,
                        indices = indices,
                        outIndices = outIndices.AsParallelWriter(),
                        width = width,
                        height = height,
                    }.Schedule(indices.Length, 64).Complete();

                    CoreUtils.Swap(ref indices, ref outIndices);
                    outIndices.Length = 0;
                }

                indices.Dispose();
                outIndices.Dispose();

                textureAtlas.texture.SetPixelData(pixels, 0);
            }
        }

        [BurstCompile]
        struct ClassifyPixelsJob : IJobParallelFor
        {
            [ReadOnly]
            [NativeMatchesParallelForLength]
            public NativeArray<Color32> pixels;

            [WriteOnly]
            public NativeList<int>.ParallelWriter outIndices;

            public void Execute(int index)
            {
                Color32 pixel = pixels[index];
                if (pixel.a == 0)
                {
                    outIndices.AddNoResize(index);
                }
            }
        }

        [BurstCompile]
        struct DilateJob : IJobParallelFor
        {
            [NativeDisableParallelForRestriction]
            public NativeArray<Color32> pixels;

            [ReadOnly]
            public NativeList<int> indices;

            [WriteOnly]
            public NativeList<int>.ParallelWriter outIndices;

            public int width;
            public int height;

            public void Execute(int index)
            {
                int pixelIndex = indices[index];

                int x = pixelIndex % width;
                int y = pixelIndex / width;

                float weight = 0;
                Color color = new Color(0, 0, 0, 0);
                for (int dy = -1; dy <= 1; dy++)
                {
                    for (int dx = -1; dx <= 1; dx++)
                    {
                        int sx = x + dx;
                        int sy = y + dy;

                        if (sx < 0 || sx >= width || sy < 0 || sy >= height)
                            continue;

                        Color32 s = pixels[sx + sy * width];
                        if (s.a != 0)
                        {
                            Color sample = ((Color)s).linear;
                            color += sample;
                            weight += 1.0f;
                        }
                    }
                }

                if (weight > 0)
                {
                    color /= weight;
                    color.a = 1;
                    pixels[x + y * width] = color.gamma;
                }
                else
                {
                    outIndices.AddNoResize(pixelIndex);
                }
            }
        }

        void BakePosableCharacters(List<PosableCharacter> posableCharacters, List<Mesh> bakedMeshes, string bakingFolder)
        {
            for (int i = 0; i < posableCharacters.Count; i++)
            {
                PosableCharacter posableCharacter = posableCharacters[i];

                foreach (var skinnedMeshRenderer in posableCharacter.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    if (skinnedMeshRenderer.sharedMesh == null)
                    {
                        continue;
                    }

                    Mesh bakedMesh = new Mesh();
                    bakedMesh.name = $"BakedMesh_{bakedMeshes.Count + 1} ({skinnedMeshRenderer.sharedMesh.name})";

                    skinnedMeshRenderer.BakeMesh(bakedMesh);

                    AssetDatabase.CreateAsset(bakedMesh, Path.Combine(bakingFolder, bakedMesh.name + ".asset"));

                    GameObject gameObject = skinnedMeshRenderer.gameObject;
                    Material[] materials = skinnedMeshRenderer.sharedMaterials;
                    uint renderingLayerMask = skinnedMeshRenderer.renderingLayerMask;

                    Object.DestroyImmediate(skinnedMeshRenderer);

                    var meshFilter = gameObject.AddComponent<MeshFilter>();
                    meshFilter.sharedMesh = bakedMesh;

                    var meshRenderer = gameObject.AddComponent<MeshRenderer>();
                    meshRenderer.sharedMaterials = materials;
                    meshRenderer.renderingLayerMask = renderingLayerMask;

                    if (gameObject.TryGetComponent<OutlineRenderer>(out var outlineRenderer))
                    {
                        outlineRenderer.Reinitialize();
                    }

                    bakedMeshes.Add(bakedMesh);
                }

                if (posableCharacter.RigRoot != null)
                {
                    Object.DestroyImmediate(posableCharacter.RigRoot.gameObject);
                }

                Object.DestroyImmediate(posableCharacter);
            }
        }

        void BakeLineData(List<OutlineRenderer> outlineRenderers, string bakingFolder)
        {
            Dictionary<Mesh, BakedLineData> bakedData = new Dictionary<Mesh, BakedLineData>();

            foreach (var outlineRenderer in outlineRenderers)
            {
                if (outlineRenderer.IsSkinned)
                {
                    Debug.LogError("Cannot bake outline renderer that is attached to skinned mesh");
                    continue;
                }

                var meshFilter = outlineRenderer.SourceRenderer.GetComponent<MeshFilter>();
                if (meshFilter == null || meshFilter.sharedMesh == null)
                {
                    Debug.LogError("Cannot bake outline renderer, no mesh filter with a valid mesh attached");
                    continue;
                }

                Mesh mesh = meshFilter.sharedMesh;
                if (!bakedData.TryGetValue(mesh, out BakedLineData bakedLineData))
                {
                    bakedLineData = OutlineMesh.GenerateBakedData(mesh);
                    bakedLineData.name = $"BakedLineData_{bakedData.Count} ({mesh.name})";
                    AssetDatabase.CreateAsset(bakedLineData, Path.Combine(bakingFolder, bakedLineData.name + ".asset"));
                    bakedData.Add(mesh, bakedLineData);
                }

                outlineRenderer.SetBakedLineData(bakedLineData);
            }
        }

        void CheckPageTransforms(Page page)
        {
            if (page.transform.position != Vector3.zero
                || page.transform.rotation != Quaternion.identity
                || page.transform.lossyScale != Vector3.one)
            {
                ReportError("Page transform is not zeroed", page);
            }

            foreach (var panel in page.Panels)
            {
                if (panel.transform.position != Vector3.zero
                    || panel.transform.rotation != Quaternion.identity
                    || panel.transform.lossyScale != Vector3.one)
                {
                    ReportError($"{panel.name} transform is not zeroed", panel);
                }
            }
        }

        void ReportError(string message, Object obj = null)
        {
            errors.Add((message, obj));
            Debug.LogError(message, obj);
        }
    }
}