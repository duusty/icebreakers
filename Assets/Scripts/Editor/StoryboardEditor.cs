using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using System.Linq;
using System.Reflection;

namespace Icebreakers.Editor
{
    [InitializeOnLoad]
    public class StoryboardEditor : EditorWindow
    {
        [MenuItem("Window/Icebreakers/Storyboard")]
        static void CreateWindow()
        {
            var window = EditorWindow.GetWindow<StoryboardEditor>();
            window.titleContent = new GUIContent("Storyboard");
        }

        Storyboard storyboard;
        [NonSerialized]
        StoryboardPage selectedPage;
        [NonSerialized]
        Panel selectedPanel;
        [NonSerialized]
        SpeechBubble selectedSpeechBubble;

        ReorderableList sceneList;
        ReorderableList panelList;
        ReorderableList speechBubbleList;
        ReorderableList bubbleList;
        Vector2 sceneListScrollPosition;
        Vector2 bubbleListScrollPosition;

        GUIContent loadButtonIcon;
        GUIContent deleteButtonIcon;
        GUIContent editButtonIcon;
        GUIContent eyeButtonIcon;
        GUIContent panelIcon;
        GUIContent bubbleIcon;

        Material panelCutoutMaterial;

        float timelineRowHeight, timelineItemWidth, timelineLength, timelineHeight;

        Panel currentDragPanel = null;
        SpeechBubble.SubBubble currentDragBubble = null;

        static StoryboardEditor()
        {
            HandleUtility.pickGameObjectCustomPasses += PickGameObjectCallback;
        }

        static GameObject PickGameObjectCallback(Camera cam, int layers, Vector2 position, GameObject[] ignore, GameObject[] filter, out int materialIndex)
        {
            List<Panel> toTest = new List<Panel>();
            if (filter != null)
            {
                foreach (var gameObject in filter)
                {
                    if (gameObject.TryGetComponent<Panel>(out Panel panel))
                    {
                        toTest.Add(panel);
                    }
                }
            }
            else
            {
                toTest.AddRange(GameObject.FindObjectsOfType<Panel>());
            }

            GameObject picked = null;

            Ray ray = cam.ScreenPointToRay(position);
            float closest = float.PositiveInfinity;

            foreach (var panel in toTest)
            {
                if (((1 << panel.gameObject.layer) & layers) == 0)
                    continue;

                if (!panel.IntersectWithPanel(ray, float.PositiveInfinity, out PanelIntersection isect))
                    continue;

                if (isect.t < closest)
                {
                    picked = panel.PanelView.gameObject;
                    closest = isect.t;
                }
            }

            materialIndex = -1;
            return picked;
        }

        void OnEnable()
        {
            storyboard = AssetDatabase.LoadAssetAtPath<Storyboard>("Assets/Resources/Storyboard.asset");

            if (storyboard != null)
            {
                sceneList = new ReorderableList(storyboard.PagesList, typeof(StoryboardPage), true, false, false, false);
                sceneList.drawElementCallback = DrawPageListElement;
                sceneList.elementHeight = 40;
                sceneList.onSelectCallback = OnSelectPageListElement;
                sceneList.onReorderCallback = OnSelectPageListReorder;
            }

            panelList = new ReorderableList(null, typeof(Panel), true, false, false, false);
            panelList.drawElementCallback = DrawPanelListElement;
            panelList.onSelectCallback = OnSelectPanelListElement;
            panelList.elementHeight = 35;

            speechBubbleList = new ReorderableList(null, typeof(SpeechBubble), true, false, false, false);
            speechBubbleList.drawElementCallback = DrawSpeechBubbleListElement;
            speechBubbleList.onSelectCallback = OnSelectSpeechBubbleListElement;
            speechBubbleList.elementHeight = 40;

            bubbleList = new ReorderableList(null, typeof(SpeechBubble.SubBubble), false, false, false, false);
            bubbleList.drawElementCallback = DrawBubbleListElement;
            bubbleList.elementHeight = 250;

            loadButtonIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/LoadIcon.png"), "Load Page");
            deleteButtonIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/DeleteIcon.png"), "Delete Page");
            editButtonIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/EditIcon.png"), "Edit Panel");
            eyeButtonIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/EyeIcon.png"), "Show Panel");
            panelIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/PanelIcon.png"), "Panel");
            bubbleIcon = new GUIContent(AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/Editor/Icons/BubbleIcon.png"), "Bubble");
            
            Undo.undoRedoPerformed += OnUndoRedoPerformed;
        }

        void OnDisable()
        {
            Undo.undoRedoPerformed -= OnUndoRedoPerformed;
        }

        void OnUndoRedoPerformed()
        {
            Repaint();
        }

        void OnSelectPageListElement(ReorderableList list)
        {
            selectedPage = (StoryboardPage)list.list[list.index];
            selectedPanel = null;
            selectedSpeechBubble = null;
        }

        void OnSelectPageListReorder(ReorderableList list)
        {
            storyboard.UpdateBuildSettings();
        }

        void DrawPageListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            var page = (StoryboardPage)sceneList.list[index];

            // padding
            rect.FromRight(4);

            Vector2 deleteButtonSize = GUI.skin.button.CalcSize(deleteButtonIcon);
            Rect deleteButtonRect = new Rect(rect.FromRight(deleteButtonSize.x).Centered(deleteButtonSize));
            if (GUI.Button(deleteButtonRect, deleteButtonIcon))
            {
                if (EditorUtility.DisplayDialog("Delete Page", $"Are you sure you want to delete page {index + 1} \"{page.title}\"", "Delete", "Cancel"))
                {
                    storyboard.RemovePage(page);
                    if (page == selectedPage)
                        selectedPage = null;

                    if (page.pageScene != null)
                    {
                        string scenePath = AssetDatabase.GetAssetPath(page.pageScene);

                        var setup = new List<SceneSetup>(EditorSceneManager.GetSceneManagerSetup());
                        bool setActiveScene = false;
                        for (int i = 0; i < setup.Count; i++)
                        {
                            SceneSetup sceneSetup = setup[i];
                            if (sceneSetup.path == scenePath)
                            {
                                setActiveScene = sceneSetup.isActive;
                                setup.RemoveAt(i);
                                break;
                            }
                        }

                        if (setup.Count == 0)
                        {
                            EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
                        }
                        else
                        {
                            if (setActiveScene)
                            {
                                setup[0].isActive = true;
                            }

                            EditorSceneManager.RestoreSceneManagerSetup(setup.ToArray());
                        }

                        AssetDatabase.DeleteAsset(AssetDatabase.GetAssetPath(page.pageScene));
                    }

                    // exit gui because we invalidate the scene list during iteration
                    GUIUtility.ExitGUI();
                }
            }

            // padding
            rect.FromRight(10);

            Vector2 loadButtonSize = GUI.skin.button.CalcSize(loadButtonIcon);
            Rect loadButtonRect = new Rect(rect.FromRight(loadButtonSize.x).Centered(loadButtonSize));
            if (GUI.Button(loadButtonRect, loadButtonIcon))
            {
                LoadPage(page);
                selectedPage = (StoryboardPage)sceneList.list[index];
            }

            GUI.Label(rect, $"Page {index + 1}\n{page.title}");
        }

        void OnSelectPanelListElement(ReorderableList list)
        {
            selectedPanel = (Panel)list.list[list.index];
            selectedSpeechBubble = null;
            bool pageInEditMode = false;
            foreach(object p in list.list)
            {
                Panel panel = (Panel)p;
                if (panel.isInEditMode) pageInEditMode = true;
            }
            if(!pageInEditMode || selectedPanel.isInEditMode) Selection.activeObject = selectedPanel.PanelView;
        }

        void DrawPanelListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            var panel = (Panel)panelList.list[index];

            Rect topRect = rect.FromTop(30);

            // padding
            topRect.FromRight(4);

            Vector2 deleteButtonSize = GUI.skin.button.CalcSize(deleteButtonIcon);
            Rect deleteButtonRect = new Rect(topRect.FromRight(deleteButtonSize.x).Centered(deleteButtonSize));
            if (GUI.Button(deleteButtonRect, deleteButtonIcon))
            {
                if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                {
                    if (panel.isInEditMode)
                    {
                        EditorUtility.DisplayDialog("Error", "Cannot delete panel while in edit mode.", "Ok");
                    }
                    else
                    {
                        string undoName = "Delete Panel";
                        Undo.RecordObject(page, undoName);
                        page.RemovePanel(panel);

                        Undo.DestroyObjectImmediate(panel.gameObject);

                        // Exit gui here because we modify the panel list
                        GUIUtility.ExitGUI();
                    }
                }
            }

            // padding
            topRect.FromRight(10);

            Vector2 editButtonSize = GUI.skin.button.CalcSize(editButtonIcon);
            Rect editButtonRect = new Rect(topRect.FromRight(editButtonSize.x).Centered(editButtonSize));

            GUI.color = panel.isInEditMode ? Color.red : Color.white;
            bool isInEditMode = GUI.Toggle(editButtonRect, panel.isInEditMode, editButtonIcon, GUI.skin.button);
            GUI.color = Color.white;
            if (isInEditMode != panel.isInEditMode)
            {
                if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                {
                    if (panel.isInEditMode)
                    {
                        LeavePanelEditMode(scene, page, panel);
                    }
                    else
                    {
                        if (page.Panels.Any(x => x.isInEditMode))
                        {
                            EditorUtility.DisplayDialog("Error", "Cannot enter edit mode for panel.\nThere is already a panel in edit mode!", "Ok");
                        }
                        else
                        {
                            EnterPanelEditMode(scene, page, panel);
                        }
                    }
                }
            }

            // padding
            topRect.FromRight(10);

            Vector2 eyeButtonSize = GUI.skin.button.CalcSize(eyeButtonIcon);
            Rect eyeButtonRect = new Rect(topRect.FromRight(eyeButtonSize.x).Centered(eyeButtonSize));

            if (GUI.Button(eyeButtonRect, eyeButtonIcon))
            {
                if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                {
                    bool pageInEditMode = false; // only allow reframing when appropriate
                    foreach (object p in page.PanelList)
                    {
                        Panel testp = (Panel)p;
                        if (testp.isInEditMode) pageInEditMode = true;
                    }

                    if (!pageInEditMode || panel.isInEditMode)
                    {
                        framePanelView(isInEditMode ? panel.localViewOrigion.transform.position : page.ViewOrigin, panel);
                    }

                }
            }

            GUI.Label(topRect, $"Panel {index + 1}\n{panel.name}");
        }

        /// <summary>
        /// frames the camera to a given panel from a given origin point.
        /// </summary>
        /// <param name="originPos">new position of the camera after framing</param>
        /// <param name="panel">panel to frame</param>
        void framePanelView(Vector3 originPos, Panel panel)
        {
            foreach (SceneView sceneView in SceneView.sceneViews)
            {
                //Vector3 originPos = isInEditMode ? panel.localViewOrigion.transform.position : page.ViewOrigin; //choose correct origin based on mode
                Vector3 toPanel = panel.PanelView.transform.position - originPos; // view direction
                Quaternion rotation = Quaternion.LookRotation(toPanel, Vector3.up);
                sceneView.pivot = originPos; //setup correct pivot
                sceneView.rotation = rotation; //setup correct rotation, both probably unneccessary...
                Bounds panelBounds = GeometryUtility.CalculateBounds( //calc panel view bounds so we can correctly reset scene pivot
                    new Vector3[]
                    {
                                    new Vector3( - panel.PanelView.Size.x/2, - panel.PanelView.Size.y/2, 0),
                                    new Vector3(   panel.PanelView.Size.x/2, - panel.PanelView.Size.y/2, 0),
                                    new Vector3( - panel.PanelView.Size.x/2,   panel.PanelView.Size.y/2, 0),
                                    new Vector3(   panel.PanelView.Size.x/2,   panel.PanelView.Size.y/2, 0)
                    },
                    panel.PanelView.transform.localToWorldMatrix
                );
                sceneView.Frame(panelBounds); // frame panel view and reset scene pivot in the process
                Vector3 origin = originPos + rotation * Vector3.forward * sceneView.cameraDistance; //get new camera origin _after_ reseting pivot so it's correct.
                sceneView.LookAtDirect(origin, rotation); // move to correct position and orientation
            }
        }

        void OnSelectSpeechBubbleListElement(ReorderableList list)
        {
            selectedSpeechBubble = (SpeechBubble)list.list[list.index];

            Selection.activeGameObject = selectedSpeechBubble.gameObject;
        }

        void DrawSpeechBubbleListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            var speechBubble = (SpeechBubble)speechBubbleList.list[index];

            // padding
            rect.FromRight(4);

            Vector2 deleteButtonSize = GUI.skin.button.CalcSize(deleteButtonIcon);
            Rect deleteButtonRect = new Rect(rect.FromRight(deleteButtonSize.x).Centered(deleteButtonSize));
            if (GUI.Button(deleteButtonRect, deleteButtonIcon))
            {
                if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                {
                    string undoName = "Delete Speech Bubble";
                    Undo.RecordObject(selectedPanel, undoName);
                    selectedPanel.RemoveSpeechBubble(speechBubble);

                    Undo.DestroyObjectImmediate(speechBubble.gameObject);

                    // Exit gui here because we modify the panel list
                    GUIUtility.ExitGUI();
                }
            }

            // padding
            rect.FromRight(10);

            string speechBubbleHeading = "-";
            if (speechBubble.bubbles.Count > 0)
            {
                speechBubbleHeading = speechBubble.bubbles[0].text;
                if (speechBubbleHeading.IndexOf("\n") != -1)
                {
                    speechBubbleHeading = speechBubbleHeading.Substring(0, speechBubbleHeading.IndexOf("\n"));
                }
            }

            GUI.Label(rect, $"Speech Bubble {index + 1}\n{speechBubbleHeading}");
        }

        void DrawBubbleListElement(Rect rect, int index, bool isActive, bool isFocused)
        {
            var bubble = (SpeechBubble.SubBubble)bubbleList.list[index];

            Rect topRect = rect.FromTop(30);

            // padding
            topRect.FromRight(4);

            Vector2 deleteButtonSize = GUI.skin.button.CalcSize(deleteButtonIcon);
            Rect deleteButtonRect = new Rect(topRect.FromRight(deleteButtonSize.x).Centered(deleteButtonSize));
            if (GUI.Button(deleteButtonRect, deleteButtonIcon))
            {
                if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                {
                    string undoName = "Delete Speech Bubble";
                    Undo.RecordObject(selectedSpeechBubble, undoName);
                    selectedSpeechBubble.RemoveBubble(bubble);

                    // Exit gui here because we modify the panel list
                    GUIUtility.ExitGUI();
                }
            }

            // padding
            topRect.FromRight(10);

            GUI.Label(topRect, $"Bubble {index}");

            Rect textArea = rect.FromTop(110);
            EditorGUI.BeginChangeCheck();
            string text = GUI.TextArea(textArea, bubble.text);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectedSpeechBubble, "Change Bubble Text");
                bubble.text = text;
                selectedSpeechBubble.RebuildBubbles();
            }

            rect.FromTop(2);

            EditorGUI.BeginChangeCheck();
            int resolution = EditorGUI.IntField(rect.FromTop(18), "Resolution", bubble.resolution);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectedSpeechBubble, "Change Bubble Resolution");
                bubble.resolution = resolution;
                selectedSpeechBubble.RebuildBubbles();
            }

            rect.FromTop(2);

            EditorGUI.BeginChangeCheck();
            float padding = EditorGUI.FloatField(rect.FromTop(18), "Padding", bubble.padding);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectedSpeechBubble, "Change Bubble Padding");
                bubble.padding = padding;
                selectedSpeechBubble.RebuildBubbles();
            }

            rect.FromTop(2);

            EditorGUI.BeginChangeCheck();
            float square = EditorGUI.Slider(rect.FromTop(18), "Square", bubble.square, 0, 1);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectedSpeechBubble, "Change Bubble Square");
                bubble.square = square;
                selectedSpeechBubble.RebuildBubbles();
            }

            rect.FromTop(2);

            EditorGUI.BeginChangeCheck();
            float delay = EditorGUI.FloatField(rect.FromTop(18), "Delay", bubble.delay);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(selectedSpeechBubble, "Change Bubble Delay");
                bubble.delay = delay;
                selectedSpeechBubble.RebuildBubbles();
            }

            rect.FromTop(2);

            AudioClip newVoiceline = (AudioClip)EditorGUI.ObjectField(rect.FromTop(18), "voiceline", bubble.voiceline, typeof(AudioClip), false);
            Undo.RecordObject(selectedSpeechBubble, "Change Bubble voiceline");
            bubble.voiceline = newVoiceline;
            selectedSpeechBubble.RebuildBubbles();
        }

        void LeavePanelEditMode(Scene scene, Page page, Panel panel)
        {
            EditorSceneManager.SetActiveScene(scene);

            Selection.activeObject = null; //deselect

            List<Transform> children = new List<Transform>();
            foreach (GameObject root in scene.GetRootGameObjects())
            {
                if (root.TryGetComponent<Panel>(out _) || root.TryGetComponent<Page>(out _))
                {
                    continue;
                }

                children.Add(root.transform);
            }

            List<Renderer> renderers = new List<Renderer>();
            foreach (var child in children)
            {
                renderers.AddRange(child.GetComponentsInChildren<Renderer>());
            }

            Undo.SetCurrentGroupName("Leave Panel Edit Mode");
            uint layer = 1u << panel.PanelNumber;
            foreach (var renderer in renderers)
            {
                Undo.RecordObject(renderer, "Change Rendering Layer");
                renderer.renderingLayerMask = layer;
            }

            Undo.RecordObject(page.WorldRoot.gameObject, "Enable World");
            page.WorldRoot.gameObject.SetActive(true);

            Undo.RecordObject(panel, "Set Edit Mode");
            panel.isInEditMode = false;

            Undo.RecordObject(panel.SceneRoot.transform, "Reset scene root");
            panel.SceneRoot.transform.localPosition = Quaternion.Inverse(panel.PanelView.transform.rotation) * -panel.PanelView.transform.position;
            panel.SceneRoot.transform.localRotation = Quaternion.Inverse(panel.PanelView.transform.rotation);

            Undo.SetTransformParent(panel.PanelView.transform, panel.transform, false, "Move Panel View From Camera");
            Undo.RecordObject(panel.PanelView.transform, "Change Panel Position");
            panel.PanelView.transform.localPosition = panel.PanelView.originalPosition;
            panel.PanelView.transform.localRotation = panel.PanelView.originalRotation;

            Undo.DestroyObjectImmediate(panel.localViewOrigion);
            Undo.DestroyObjectImmediate(panel.panelPreviewCamera);
            Undo.DestroyObjectImmediate(panel.panelCutoutMesh);

            Undo.RecordObject(panel.PanelView, "Record Original");
            panel.PanelView.originalPosition = Vector3.one;
            panel.PanelView.originalRotation = Quaternion.identity;

            foreach (var child in children)
            {
                Undo.SetTransformParent(child, panel.SceneRoot, false, "Reparent Scene Objects");
                Undo.RecordObject(child, "Set Transform");
                child.transform.localPosition = child.transform.localPosition;
                child.transform.localRotation = child.transform.localRotation;
                child.transform.localScale = child.transform.localScale;

                SceneVisibilityManager.instance.DisablePicking(child.gameObject, true);
            }

            foreach (var otherPanel in page.Panels)
            {
                if (otherPanel != panel)
                {
                    Undo.RecordObject(otherPanel.gameObject, "Enable Other Panels");
                    otherPanel.gameObject.SetActive(true);
                }
            }

            foreach (var child in children)
            {
                if (PrefabUtility.IsPartOfAnyPrefab(child))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(child);
                }
            }

            foreach (var renderer in renderers)
            {
                if (PrefabUtility.IsPartOfAnyPrefab(renderer))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(renderer);
                }
            }

            panel.SceneRoot.hideFlags |= HideFlags.HideInHierarchy;

            MaterialOverride[] materialOverrides = FindObjectsOfType<MaterialOverride>();
            foreach (MaterialOverride mo in materialOverrides)
            {
                mo.ApplyOverride();
            }

            framePanelView(page.ViewOrigin, panel); // frame camera after exiting panel

        }
        
        void EnterPanelEditMode(Scene scene, Page page, Panel panel)
        {
            EditorSceneManager.SetActiveScene(scene);

            Selection.activeObject = null; //deselect

            Transform sceneRoot = panel.SceneRoot;
            List<Transform> children = new List<Transform>();
            foreach (Transform child in sceneRoot)
            {
                children.Add(child);
            }

            List<Renderer> renderers = new List<Renderer>();
            foreach (var child in children)
            {
                renderers.AddRange(child.GetComponentsInChildren<Renderer>());
            }

            Undo.SetCurrentGroupName("Enter Panel Edit Mode");
            foreach (var renderer in renderers)
            {
                Undo.RecordObject(renderer, "Change Rendering Layer");
                renderer.renderingLayerMask = 1;
            }

            foreach (var otherPanel in page.Panels)
            {
                if (otherPanel != panel)
                {
                    Undo.RecordObject(otherPanel.gameObject, "Disable Other Panels");
                    otherPanel.gameObject.SetActive(false);
                }
            }

            foreach (var child in children)
            {
                Undo.SetTransformParent(child, null, false, "Reparent Scene Objects");
                Undo.RecordObject(child, "Set Transform");
                child.transform.localPosition = child.transform.localPosition;
                child.transform.localRotation = child.transform.localRotation;
                child.transform.localScale = child.transform.localScale;
                child.SetAsLastSibling();

                SceneVisibilityManager.instance.EnablePicking(child.gameObject, true);
            }

            Undo.RecordObject(panel.PanelView, "Record Original");
            panel.PanelView.originalPosition = panel.PanelView.transform.localPosition;
            panel.PanelView.originalRotation = panel.PanelView.transform.localRotation;

            Vector3 localViewOriginPosition = panel.SceneRoot.InverseTransformPoint(page.ViewOrigin);
            Quaternion localViewOriginRotation = Quaternion.Inverse(panel.SceneRoot.rotation) * page.ViewOriginRotation;

            Vector3 localPanelPosition = panel.SceneRoot.InverseTransformPoint(panel.PanelView.transform.position);
            Quaternion localPanelRotation = Quaternion.Inverse(panel.SceneRoot.rotation) * panel.PanelView.transform.rotation;

            var camera = new GameObject("PanelPreviewCamera").AddComponent<Camera>();
            Undo.RegisterCreatedObjectUndo(camera.gameObject, "Create Panel Preview Camera");
            Undo.SetTransformParent(camera.transform, panel.transform, false, "Move Camera to Panel");
            camera.farClipPlane = 1500;
            camera.transform.position = localViewOriginPosition;

            var panelCutoutMesh = new GameObject("panelCutoutMesh"); //add a panel cutout mesh so the panel border is visible from the preview camera
            MeshRenderer panelCutoutMeshRenderer = panelCutoutMesh.AddComponent<MeshRenderer>();
            if(panelCutoutMaterial == null) panelCutoutMaterial = new Material(Shader.Find("Icebreakers/Standard"));
            panelCutoutMaterial.SetColor("_BaseColor", Color.black);
            panelCutoutMaterial.SetColor("_ShadowColor", Color.black);
            panelCutoutMeshRenderer.material = panelCutoutMaterial;
            MeshFilter panelCutoutMeshFilter = panelCutoutMesh.AddComponent<MeshFilter>();
            Mesh cutoutMesh = new Mesh();
            Vector3[] vertices = new Vector3[] // create mesh with hole in the middle that's the size of the panel view
            {
                new Vector3(panel.PanelView.Size.x*0.5f, panel.PanelView.Size.y*0.5f, 0),
                new Vector3(-panel.PanelView.Size.x*0.5f, panel.PanelView.Size.y*0.5f, 0),
                new Vector3(-panel.PanelView.Size.x*0.5f, -panel.PanelView.Size.y*0.5f, 0),
                new Vector3(panel.PanelView.Size.x*0.5f, -panel.PanelView.Size.y*0.5f, 0),
                new Vector3(100, 100, 0),
                new Vector3(-100, 100, 0),
                new Vector3(-100, -100, 0),
                new Vector3(100, -100, 0)
            };
            int[] triangles = new int[]
            {
                0,4,1,
                1,4,5,
                2,1,6,
                1,5,6,
                6,3,2,
                6,7,3,
                3,7,4,
                3,4,0
            };
            cutoutMesh.vertices = vertices;
            cutoutMesh.triangles = triangles;
            cutoutMesh.UploadMeshData(false);
            panelCutoutMeshFilter.mesh = cutoutMesh;
            panelCutoutMesh.layer = LayerMask.NameToLayer("PreviewPanelOverlay"); // set object to custom layer that can be hidden in editor
            Undo.RegisterCreatedObjectUndo(panelCutoutMesh.gameObject, "Create Panel Cutout Mesh");
            Undo.SetTransformParent(panelCutoutMesh.transform, panel.PanelView.transform, false, "Parent Panel Cutout Mesh to Panel View");

            Tools.visibleLayers &= ~(1 << LayerMask.NameToLayer("PreviewPanelOverlay")); //make sure the panel overlay layer is hidden in editor

            Vector3 toPanel = localPanelPosition - camera.transform.position;
            camera.transform.rotation = Quaternion.LookRotation(toPanel, Vector3.up);
            float fieldOfView = Mathf.Atan((panel.PanelView.Size.y * 0.5f + 0.1f) / toPanel.magnitude) * 2 * Mathf.Rad2Deg;
            camera.fieldOfView = fieldOfView;
            //SetIcon(camera.gameObject, LabelIcon.Red);

            var localViewOrigin = new GameObject("LocalViewOrigion");
            Undo.RegisterCreatedObjectUndo(localViewOrigin, "Create Local View Origin");
            Undo.SetTransformParent(localViewOrigin.transform, camera.transform, false, "Move View Origion to Camera");
            localViewOrigin.transform.position = localViewOriginPosition;
            localViewOrigin.transform.rotation = localViewOriginRotation;

            Undo.SetTransformParent(panel.PanelView.transform, camera.transform, false, "Move Panel View To Camera");
            Undo.RecordObject(panel.PanelView.transform, "Change Panel Position");
            panel.PanelView.transform.position = localPanelPosition;
            panel.PanelView.transform.rotation = localPanelRotation;
            
            Undo.RecordObject(panel.SceneRoot.transform, "Reset scene root");
            panel.SceneRoot.transform.localPosition = Vector3.zero;
            panel.SceneRoot.transform.localRotation = Quaternion.identity;

            Undo.RecordObject(panel, "Set Edit Mode");
            panel.isInEditMode = true;
            panel.panelPreviewCamera = camera.gameObject;
            panel.localViewOrigion = localViewOrigin;
            panel.panelCutoutMesh = panelCutoutMesh;

            Undo.RecordObject(page.WorldRoot.gameObject, "Disable World");
            page.WorldRoot.gameObject.SetActive(false);

            foreach (var child in children)
            {
                if (PrefabUtility.IsPartOfAnyPrefab(child))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(child);
                }
            }

            foreach (var renderer in renderers)
            {
                if (PrefabUtility.IsPartOfAnyPrefab(renderer))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(renderer);
                }
            }

            MaterialOverride[] materialOverrides = FindObjectsOfType<MaterialOverride>();
            foreach (MaterialOverride mo in materialOverrides)
            {
                mo.ApplyOverride();
            }

            framePanelView(panel.localViewOrigion.transform.position, panel); //frame camera after entering panel
        }

        void OnGUI()
        {
            timelineRowHeight = 26;
            timelineItemWidth = 160;
            timelineHeight = 300;

            if (storyboard == null)
            {
                GUILayout.Label("No storyboard found");
                return;
            }

            Rect windowRect = new Rect(0, 0, Screen.width, Screen.height);

            Rect toolbarRect = windowRect.FromTop(EditorStyles.toolbar.fixedHeight);
            using (new GUILayout.AreaScope(toolbarRect, GUIContent.none, EditorStyles.toolbar))
            {
                using (new GUILayout.HorizontalScope())
                {
                    if (GUILayout.Button("New Page", EditorStyles.toolbarButton))
                    {
                        var scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);

                        var page = new GameObject("Page").AddComponent<Page>();
                        page.PerformInitialSetup();

                        string name = CreateUniqueSceneName("NewPage");
                        string path = $"Assets/Scenes/{name}.unity";
                        EditorSceneManager.SaveScene(scene, path);

                        var storyboardPage = storyboard.CreatePage(AssetDatabase.LoadAssetAtPath<SceneAsset>(path), name);

                        LoadPage(storyboardPage);

                        // XXX: exit gui here because creating a new scene seems to invalidate GUILayout state
                        GUIUtility.ExitGUI();
                    }

                    GUILayout.FlexibleSpace();
                }
            }
            
            Rect contentRect = windowRect.FromTop(position.height - timelineHeight - toolbarRect.height);
            
            Rect scrollRect = contentRect.FromLeft(300);
            Rect sceneListRect = new Rect(0, 0, scrollRect.width, sceneList.GetHeight());
            sceneListScrollPosition = GUI.BeginScrollView(scrollRect, sceneListScrollPosition, sceneListRect);
            sceneList.DoList(sceneListRect);
            GUI.EndScrollView();

            Rect selectedPageRect = contentRect.FromLeft(400);
            DrawSelectedPage(selectedPageRect);

            if (selectedSpeechBubble == null)
            {
                Rect selectedPanelRect = contentRect.FromLeft(400);
                DrawSelectedPanel(selectedPanelRect);
            }
            else
            {
                Rect selectedSpeechBubbleRect = contentRect.FromLeft(400);
                DrawSelectedSpeechBubble(selectedSpeechBubbleRect);
            }

            Rect timelineRect = new Rect(0, position.height - timelineHeight, Screen.currentResolution.width, timelineHeight);
            DrawTimeline(timelineRect);
        }

        struct TimelineItem
        {
            public float pos;
            public SpeechBubble.SubBubble bubble;
            public Panel panel;
            public Color col;
        }

        bool rowPositionFull(TimelineItem item, List<TimelineItem> row)
        {
            bool positionFull = false;
            foreach(TimelineItem rowItem in row)
            {
                if (item.pos > rowItem.pos && item.pos < rowItem.pos + timelineItemWidth) positionFull = true;
                if (item.pos + timelineItemWidth > rowItem.pos && item.pos + timelineItemWidth < rowItem.pos + timelineItemWidth) positionFull = true;
            }
            return positionFull;
        }

        float addTimelineItem(TimelineItem item, List<List<TimelineItem>> rowList)
        {

            if (rowList.Count == 0) rowList.Add(new List<TimelineItem>());

            List<TimelineItem> row = null;
            float rowPos = 0;
            for (int i = 0; i < rowList.Count; i++)
            {
                if (!rowPositionFull(item, rowList[i]))
                {
                    row = rowList[i];
                    rowPos = i * timelineRowHeight;
                    break;
                }
            }

            if (row == null)
            {
                rowList.Add(new List<TimelineItem>());
                row = rowList[rowList.Count - 1];
                rowPos = (rowList.Count - 1) * timelineRowHeight;
            }

            row.Add(item);
            
            return rowPos;
        }

        void DrawTimeline(Rect rect)
        {
            GUIStyle background = new GUIStyle(EditorStyles.helpBox); 
            GUI.BeginClip(rect);
            {
                if (selectedPage != null)
                {
                    if (TryGetLoadedSceneAndPage(selectedPage, out Scene scene, out Page page))
                    {
                        Rect controlsRect = new Rect(0, 0, rect.width, EditorStyles.toolbar.fixedHeight);
                        using (new GUILayout.AreaScope(controlsRect, GUIContent.none, EditorStyles.toolbar))
                        {
                            using (new GUILayout.HorizontalScope())
                            {
                                EditorGUI.BeginChangeCheck();
                                float nTimelineLength = EditorGUILayout.FloatField("Timeline Length", timelineLength);
                                if (EditorGUI.EndChangeCheck())
                                {
                                    timelineLength = nTimelineLength;
                                    Repaint();
                                }
                                GUILayout.FlexibleSpace();
                            }
                        }

                        drawTimelineMarkers();

                        List<List<TimelineItem>> timelineRows = new List<List<TimelineItem>>();
                        for(int i = 0; i < page.Panels.Count; i++)
                        {
                            Panel panel = page.Panels[i];
                            Color panelColor = getPanelColor(i);
                            float panelOffset = (currentDragPanel == panel) ? dragOffset.x : 0;
                            TimelineItem newPanel = new TimelineItem
                            {
                                pos = position.width * (panel.showAfter / timelineLength) + panelOffset,
                                bubble = null,
                                panel = panel,
                                col = panelColor
                            };
                            float PanelRowPos = addTimelineItem(newPanel, timelineRows);
                            drawTimelineItem(rect, newPanel, PanelRowPos);

                            foreach (SpeechBubble speechBubble in panel.SpeechBubbles)
                            {
                                foreach (SpeechBubble.SubBubble bubble in speechBubble.bubbles)
                                {
                                    float bubbleOffset = (currentDragBubble == bubble) ? dragOffset.x : 0;
                                    TimelineItem newBubble = new TimelineItem
                                    {
                                        pos = position.width * (bubble.delay / timelineLength) + bubbleOffset,
                                        bubble = bubble,
                                        panel = null,
                                        col = panelColor
                                    };
                                    float bubbleRowPos = addTimelineItem(newBubble, timelineRows);
                                    drawTimelineItem(rect, newBubble, bubbleRowPos);
                                }
                            }
                        }
                    }
                    else
                    {
                        GUILayout.Label("Page Scene not loaded");
                    }
                }
                else
                {
                    GUILayout.Label("No Page Selected");
                }

                if (Event.current.type == EventType.Repaint)
                {
                    background.Draw(new Rect(0, 0, rect.width, rect.height), false, false, false, false);
                }
            }
            GUI.EndClip();
        }

        void drawTimelineMarkers()
        {
            float secondDist = position.width / timelineLength;
            for(int i = 0; i < timelineLength; i++)
            {
                EditorGUI.DrawRect(new Rect(i* secondDist, EditorStyles.toolbar.fixedHeight, 1, 300), Color.grey);
            }
            
        }

        Vector2 dragStartPos;
        Vector2 dragOffset;
        
        Color getPanelColor(int index)
        {
            int[] colors = new int[]{
                0xFFB300, // Vivid Yellow
                0x803E75, // Strong Purple
                0xFF6800, // Vivid Orange
                0xA6BDD7, // Very Light Blue
                0xC10020, // Vivid Red
                0xCEA262, // Grayish Yellow
                0x817066, // Medium Gray

                // The following don't work well for people with defective color vision
                0x007D34, // Vivid Green
                0xF6768E, // Strong Purplish Pink
                0x00538A, // Strong Blue
                0xFF7A5C, // Strong Yellowish Pink
                0x53377A, // Strong Violet
                0xFF8E00, // Vivid Orange Yellow
                0xB32851, // Strong Purplish Red
                0xF4C800, // Vivid Greenish Yellow
                0x7F180D, // Strong Reddish Brown
                0x93AA00, // Vivid Yellowish Green
                0x593315, // Deep Yellowish Brown
                0xF13A13, // Vivid Reddish Orange
                0x232C16 // Dark Olive Green
            };

            int hex = colors[index % colors.Length];
            
            float r = ((hex >> 16) & 255) / 255f;
            float g = ((hex >> 8) & 255) / 255f;
            float b = (hex & 255) / 255f;
            
            return new Color(r, g, b);
        }

        void drawTimelineItem(Rect timelineRect, TimelineItem item, float row)
        {
            int id = GUIUtility.GetControlID(FocusType.Passive);
            Event e = Event.current;

            Rect itemRect = new Rect(
                item.pos,
                row + EditorStyles.toolbar.fixedHeight,
                timelineItemWidth,
                timelineRowHeight
            );

            Rect typeRect = new Rect(itemRect.x + 3, itemRect.y + 3, 18, 18);

            switch (e.GetTypeForControl(id))
            {
                case EventType.Repaint:
                    if (GUIUtility.hotControl == id)
                    {
                        new GUIStyle(GUI.skin.button).Draw(itemRect, false, true, false, false);
                    }
                    else
                    {
                        new GUIStyle(GUI.skin.button).Draw(itemRect, itemRect.Contains(e.mousePosition), false, false, false);
                    }
                    EditorGUI.DrawRect(typeRect, item.col);
                    break;

                case EventType.MouseDown:
                    if (e.button == 0 && itemRect.Contains(e.mousePosition))
                    {
                        dragStartPos = e.mousePosition;
                        GUIUtility.hotControl = id;
                        currentDragPanel = item.panel;
                        currentDragBubble = item.bubble;
                        Repaint();
                    }
                    break;

                case EventType.MouseUp:
                    if (GUIUtility.hotControl == id)
                    {
                        if (e.button == 0)
                        {
                            if (item.bubble != null)
                            {
                                Undo.RecordObject(item.bubble.parentBubble, "Set Speech Bubble Delay Time");
                                item.bubble.delay = (item.pos / position.width) * timelineLength;
                            }

                            if(item.panel != null)
                            {
                                Undo.RecordObject(item.panel, "Set Panel Delay Time");
                                item.panel.showAfter = (item.pos / position.width) * timelineLength;
                            }
                            dragOffset = Vector2.zero;
                            GUI.changed = true;
                            currentDragPanel = null;
                            currentDragBubble = null;
                            GUIUtility.hotControl = 0;
                        }
                    }
                    break;

                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == id)
                    {
                      dragOffset = e.mousePosition - dragStartPos;
                      GUI.changed = true;
                    }
                    break;
            }

            Rect numberInputRect = new Rect(itemRect.x + itemRect.width - 30 - 3, itemRect.y + 3, 30, 18);
            Rect labelRect = new Rect(itemRect.x + 24, itemRect.y + 3, itemRect.width - 36, 18);
            GUIStyle typeStyle = new GUIStyle();
            typeStyle.normal.textColor = Color.white;
            if (item.panel != null)
            {
                GUI.Label(typeRect, panelIcon);
                EditorGUI.LabelField(labelRect, item.panel.name);
                EditorGUI.BeginChangeCheck();
                float newShowAfter = EditorGUI.FloatField(numberInputRect, item.panel.showAfter);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(item.panel, "Set Panel Delay Time");
                    item.panel.showAfter = newShowAfter;
                    Repaint();
                }
            }

            if (item.bubble != null)
            {
                GUI.Label(typeRect, bubbleIcon);
                string bubbleLabel = item.bubble.text;
                if (bubbleLabel.IndexOf("\n") != -1) bubbleLabel = bubbleLabel.Substring(0, bubbleLabel.IndexOf("\n"));
                EditorGUI.LabelField(labelRect, bubbleLabel);
                EditorGUI.BeginChangeCheck();
                float newDelay = EditorGUI.FloatField(numberInputRect, item.bubble.delay);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(item.bubble.parentBubble, "Set Speech Bubble Delay Time");
                    item.bubble.delay = newDelay;
                    Repaint();
                }
            }
        }

        void DrawSelectedPage(Rect rect)
        {
            using (new GUILayout.AreaScope(rect, GUIContent.none, GUI.skin.box))
            {
                if (selectedPage == null)
                {
                    GUILayout.Label("No Page Selected");
                    return;
                }

                string newTitle = EditorGUILayout.DelayedTextField("Title", selectedPage.title);
                if (newTitle != selectedPage.title)
                {
                    string trimmed = newTitle.Trim();
                    if (trimmed != selectedPage.title)
                    {
                        Undo.RecordObject(storyboard, "Change Page Title");
                        selectedPage.title = CreateUniqueSceneName(trimmed);
                        if (selectedPage.pageScene != null)
                        {
                            AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(selectedPage.pageScene), selectedPage.title);
                        }

                        EditorUtility.SetDirty(storyboard);
                    }
                }

                EditorGUI.BeginChangeCheck();
                Texture2D newPreviewIcon = EditorGUILayout.ObjectField("Preview Icon", selectedPage.previewIcon, typeof(Texture2D), false) as Texture2D;
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(storyboard, "Change Page Icon");
                    selectedPage.previewIcon = newPreviewIcon;
                    EditorUtility.SetDirty(storyboard);
                }

                GUI.enabled = false;
                EditorGUILayout.ObjectField("Scene", selectedPage.pageScene, typeof(SceneAsset), false);
                GUI.enabled = true;


                if (TryGetLoadedSceneAndPage(selectedPage, out Scene loadedScene, out Page page))
                {
                    AudioClip newMusic = (AudioClip)EditorGUILayout.ObjectField("Music", page.music, typeof(AudioClip), false);
                    Undo.RecordObject(page, "Change Page Music");
                    page.music = newMusic;

                    EditorGUILayout.Space();

                    if (GUILayout.Button("New Panel"))
                    {
                        EditorSceneManager.SetActiveScene(loadedScene);

                        var panel = new GameObject("Panel").AddComponent<Panel>();
                        var sceneRoot = new GameObject("SceneRoot").transform;
                        var panelView = new GameObject("PanelView").AddComponent<PanelView>();
                        sceneRoot.hideFlags = HideFlags.HideInHierarchy;

                        panel.transform.SetParent(page.transform, false);
                        panelView.transform.SetParent(panel.transform, false);
                        sceneRoot.transform.SetParent(panelView.transform, false);

                        panelView.transform.localPosition = new Vector3(0, 1.5f, 0);
                        sceneRoot.transform.localPosition = new Vector3(0, -1.5f, 0);

                        Undo.RecordObject(panel, "Set Panel View");
                        panel.Initialize(panelView, sceneRoot);

                        Undo.RegisterCreatedObjectUndo(panel.gameObject, "Add Panel");
                        Undo.RegisterCreatedObjectUndo(sceneRoot.gameObject, "Add Panel");
                        Undo.RegisterCreatedObjectUndo(panelView.gameObject, "Add Panel");
                        Undo.RecordObject(page, "Add Panel");

                        page.AddPanel(panel);
                    }

                    panelList.list = page.PanelList;
                    panelList.DoLayoutList();

                    if (selectedPanel != null)
                    {
                        GUILayout.Label("Speech Bubbles:");
                        if (GUILayout.Button("New Speech Bubble"))
                        {
                            EditorSceneManager.SetActiveScene(loadedScene);

                            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/SpeechBubble/SpeechBubble.prefab");
                            GameObject instance = (GameObject)PrefabUtility.InstantiatePrefab(prefab, loadedScene);
                            if (!selectedPanel.isInEditMode)
                            {
                                instance.transform.SetParent(selectedPanel.SceneRoot, false);
                            }
                            Undo.RegisterCreatedObjectUndo(instance, "Create Speech Bubble");

                            var speechBubble = instance.GetComponent<SpeechBubble>();
                            Undo.RecordObject(selectedPanel, "Create Speech Bubble");
                            selectedPanel.AddSpeechBubble(speechBubble);
                        }

                        speechBubbleList.list = selectedPanel.SpeechBubbleList;
                        speechBubbleList.DoLayoutList();
                    }
                }
            }
        }

        void DrawSelectedPanel(Rect rect)
        {
            using (new GUILayout.AreaScope(rect, GUIContent.none, GUI.skin.box))
            {
                if (selectedPanel == null)
                {
                    GUILayout.Label("No Panel Selected");
                    return;
                }

                if (!TryGetLoadedSceneAndPage(selectedPage, out Scene loadedScene, out Page page))
                {
                    return;
                }

                /*string newTitle = EditorGUILayout.DelayedTextField("Panel Title", selectedPanel.Title);
                if (newTitle != selectedPanel.Title)
                {
                    string trimmed = newTitle.Trim();
                    if (trimmed != selectedPanel.Title)
                    {
                        Undo.RecordObject(selectedPanel.gameObject, "Change Panel Title");
                        selectedPanel.Title = newTitle;
                    }
                }*/

                string newName = EditorGUILayout.DelayedTextField("Name", selectedPanel.name);
                if (newName != selectedPanel.name)
                {
                    string trimmed = newName.Trim();
                    if (trimmed != selectedPanel.name)
                    {
                        Undo.RecordObject(selectedPanel.gameObject, "Change Speech Bubble Name");
                        selectedPanel.name = newName;
                    }
                }

                EditorGUI.BeginChangeCheck();
                float showAfter = EditorGUILayout.FloatField("Show After", selectedPanel.showAfter);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedPanel, "Change Show After");
                    selectedPanel.showAfter = showAfter;
                }

                EditorGUILayout.Space(10);

                EditorGUI.BeginChangeCheck();
                Vector2 panelViewSize = EditorGUILayout.Vector2Field("Panel View Size", selectedPanel.PanelView.Size);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedPanel.PanelView, "Change Panel View Size");
                    selectedPanel.PanelView.Size = panelViewSize;
                }
            }
        }

        void DrawSelectedSpeechBubble(Rect rect)
        {
            using (new GUILayout.AreaScope(rect, GUIContent.none, GUI.skin.box))
            {
                if (selectedSpeechBubble == null)
                {
                    GUILayout.Label("No Speech Bubble Selected");
                    return;
                }

                if (!TryGetLoadedSceneAndPage(selectedPage, out Scene loadedScene, out Page page))
                {
                    return;
                }

                string newName = EditorGUILayout.DelayedTextField("Name", selectedSpeechBubble.name);
                if (newName != selectedSpeechBubble.name)
                {
                    string trimmed = newName.Trim();
                    if (trimmed != selectedSpeechBubble.name)
                    {
                        Undo.RecordObject(selectedSpeechBubble.gameObject, "Change Speech Bubble Name");
                        selectedSpeechBubble.name = newName;
                    }
                }

                EditorGUI.BeginChangeCheck();
                float showAfter = EditorGUILayout.FloatField("Show After", selectedSpeechBubble.showAfter);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Show After");
                    selectedSpeechBubble.showAfter = showAfter;
                }

                EditorGUILayout.Space(10);

                EditorGUI.BeginChangeCheck();
                float outlineMiterLimit = EditorGUILayout.Slider("Outline Miter Limit", selectedSpeechBubble.outlineMiterLimit, 0, 1);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Miter Limit");
                    selectedSpeechBubble.outlineMiterLimit = outlineMiterLimit;
                    selectedSpeechBubble.RebuildBubbles();
                }

                EditorGUI.BeginChangeCheck();
                float outlineThickness = EditorGUILayout.FloatField("Outline Thickness", selectedSpeechBubble.outlineThickness);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Outline Thickness");
                    selectedSpeechBubble.outlineThickness = outlineThickness;
                    selectedSpeechBubble.RebuildBubbles();
                }

                EditorGUI.BeginChangeCheck();
                float arrowWidth = EditorGUILayout.FloatField("Arrow Width", selectedSpeechBubble.arrowWidth);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Arrow Width");
                    selectedSpeechBubble.arrowWidth = arrowWidth;
                    selectedSpeechBubble.RebuildBubbles();
                }

                EditorGUI.BeginChangeCheck();
                float attachmentAngle = EditorGUILayout.Slider("Attachment Angle", selectedSpeechBubble.attachmentAngle, 0, 1);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Attachment Angle");
                    selectedSpeechBubble.attachmentAngle = attachmentAngle;
                    selectedSpeechBubble.RebuildBubbles();
                }

                EditorGUI.BeginChangeCheck();
                bool hasArrow = EditorGUILayout.Toggle("Show Arrow", selectedSpeechBubble.hasArrow);
                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(selectedSpeechBubble, "Change Bubble Arrow Visibility");
                    selectedSpeechBubble.hasArrow = hasArrow;
                    selectedSpeechBubble.RebuildBubbles();
                }

                EditorGUILayout.Space(10);

                if (GUILayout.Button("Add Bubble"))
                {
                    Undo.RecordObject(selectedSpeechBubble, "Add Bubble");
                    selectedSpeechBubble.AddBubble("New Bubble");
                }

                bubbleListScrollPosition = EditorGUILayout.BeginScrollView(bubbleListScrollPosition);

                bubbleList.list = selectedSpeechBubble.bubbles;
                bubbleList.DoLayoutList();

                EditorGUILayout.EndScrollView();

                if (PrefabUtility.IsPartOfAnyPrefab(selectedSpeechBubble))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(selectedSpeechBubble);
                }
            }
        }

        bool TryGetLoadedSceneAndPage(StoryboardPage storyboardPage, out Scene scene, out Page page)
        {
            scene = default;
            string pageScenePath = AssetDatabase.GetAssetPath(selectedPage.pageScene);
            for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
            {
                var s = EditorSceneManager.GetSceneAt(i);
                if (s.path == pageScenePath)
                {
                    scene = s;
                }
            }

            if (!(scene.IsValid() && scene.isLoaded))
            {
                scene = default;
                page = default;
                return false;
            }

            page = null;
            if (scene.IsValid() && scene.isLoaded)
            {
                foreach (var gameObject in scene.GetRootGameObjects())
                {
                    if (gameObject.TryGetComponent<Page>(out page))
                    {
                        break;
                    }
                }
            }

            if (page == null)
            {
                scene = default;
                page = default;
                return false;
            }

            return true;
        }

        string CreateUniqueSceneName(string name)
        {
            string folder = "Assets/Scenes";
            int index = 0;
            string path = $"{folder}/{name}.unity";
            if (!File.Exists(path))
                return name;

            do
            {
                index++;
                path = $"{folder}/{name}.{index:000}.unity";
            }
            while (File.Exists(path));

            return $"{name}.{index:000}";
        }

        void LoadPage(StoryboardPage page)
        {
            if (page == null)
                throw new ArgumentNullException(nameof(page));

            if (page.pageScene == null)
            {
                Debug.LogError("[Storyboard] Cannot load page with null scene");
                return;
            }

            SceneSetup[] sceneSetup = new SceneSetup[2]
            {
                new SceneSetup
                {
                    path = AssetDatabase.GetAssetPath(storyboard.mainScene),
                    isLoaded = true,
                    isActive = false,
                },

                new SceneSetup
                {
                    path = AssetDatabase.GetAssetPath(page.pageScene),
                    isLoaded = true,
                    isActive = true,
                },
            };

            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                EditorSceneManager.RestoreSceneManagerSetup(sceneSetup);
            }
        }

        public enum LabelIcon
        {
            Gray = 0,
            Blue,
            Teal,
            Green,
            Yellow,
            Orange,
            Red,
            Purple
        }

        public enum Icon
        {
            CircleGray = 0,
            CircleBlue,
            CircleTeal,
            CircleGreen,
            CircleYellow,
            CircleOrange,
            CircleRed,
            CirclePurple,
            DiamondGray,
            DiamondBlue,
            DiamondTeal,
            DiamondGreen,
            DiamondYellow,
            DiamondOrange,
            DiamondRed,
            DiamondPurple
        }

        private static GUIContent[] labelIcons;
        private static GUIContent[] largeIcons;

        public static void SetIcon(GameObject gObj, LabelIcon icon)
        {
            if (labelIcons == null)
            {
                labelIcons = GetTextures("sv_label_", string.Empty, 0, 8);
            }

            SetIcon(gObj, labelIcons[(int)icon].image as Texture2D);
        }

        public static void SetIcon(GameObject gObj, Icon icon)
        {
            if (largeIcons == null)
            {
                largeIcons = GetTextures("sv_icon_dot", "_pix16_gizmo", 0, 16);
            }

            SetIcon(gObj, largeIcons[(int)icon].image as Texture2D);
        }

        private static void SetIcon(GameObject gObj, Texture2D texture)
        {
            var ty = typeof(EditorGUIUtility);
            var mi = ty.GetMethod("SetIconForObject", BindingFlags.NonPublic | BindingFlags.Static);
            mi.Invoke(null, new object[] { gObj, texture });
        }

        private static GUIContent[] GetTextures(string baseName, string postFix, int startIndex, int count)
        {
            GUIContent[] guiContentArray = new GUIContent[count];

            for (int index = 0; index < count; ++index)
            {
                guiContentArray[index] = EditorGUIUtility.IconContent(baseName + (object)(startIndex + index) + postFix);
            }

            return guiContentArray;
        }
    }
}