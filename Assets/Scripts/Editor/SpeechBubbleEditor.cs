using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Icebreakers.Editor
{
    [CustomEditor(typeof(SpeechBubble))]
    public class SpeechBubbleEditor : UnityEditor.Editor
    {
        void OnSceneGUI()
        {
            SpeechBubble speechBubble = (SpeechBubble)target;

            // TODO: What to do when the the bubble overlaps with the speech bubble transform?
            // The gizmos will be at the exact same position.
            foreach (var bubble in speechBubble.bubbles)
            {
                EditorGUI.BeginChangeCheck();

                customHandles.DragHandleResult dhResult;
                Vector3 newPosition = customHandles.DragHandle(
                    speechBubble.transform.TransformPoint(bubble.position),
                    speechBubble.transform.rotation, bubble.bubbleMesh.Size * 2 * speechBubble.transform.localScale.x,
                    Color.blue, Color.yellow, new Color(0.2f, 0.7f, 1),
                    out dhResult
                    );

                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(speechBubble, "Change Bubble Position");
                    newPosition = speechBubble.transform.InverseTransformPoint(newPosition);
                    newPosition.z = 0;
                    bubble.position = newPosition;
                    speechBubble.RebuildBubbles();
                }
            }

            {
                EditorGUI.BeginChangeCheck();

                customHandles.DragHandleResult dhResult;
                Vector3 newPosition = customHandles.DragHandle(
                    speechBubble.transform.TransformPoint(speechBubble.arrowPosition), 
                    speechBubble.transform.rotation, new Vector2(0.6f, 0.6f) * speechBubble.transform.localScale.x,
                    Color.blue, Color.yellow, new Color(0.2f,0.7f,1), 
                    out dhResult
                    );

                /*switch (dhResult) // how to use events here
                {
                    case MyHandles.DragHandleResult.LMBDoubleClick:
                        // do something
                        break;
                }*/

                if (EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(speechBubble, "Change Arrow Position");
                    newPosition = speechBubble.transform.InverseTransformPoint(newPosition);
                    newPosition.z = 0;
                    speechBubble.arrowPosition = newPosition;
                    speechBubble.RebuildBubbles();
                }
            }
        }
    }

    /////////////////////////////////////////////


    public class customHandles
    {
        // internal state for DragHandle()
        static int s_DragHandleHash = "DragHandleHash".GetHashCode();
        static Vector2 s_DragHandleMouseStart;
        static Vector2 s_DragHandleMouseCurrent;
        static Vector3 s_DragHandleWorldStart;
        static Vector3 s_DragHandleMouseOffset;
        static float s_DragHandleClickTime = 0;
        static int s_DragHandleClickID;
        static float s_DragHandleDoubleClickInterval = 0.5f;
        static bool s_DragHandleHasMoved;
        static bool s_DragHandleIsHover;

        // externally accessible to get the ID of the most resently processed DragHandle
        public static int lastDragHandleID;

        public enum DragHandleResult
        {
            none = 0,

            LMBPress,
            LMBClick,
            LMBDoubleClick,
            LMBDrag,
            LMBRelease,

            RMBPress,
            RMBClick,
            RMBDoubleClick,
            RMBDrag,
            RMBRelease,
        };

        public static float distanceToRect(Vector3 position, Quaternion orientation, Vector2 size)
        {
            Vector3 up = orientation * Vector3.up, right = orientation * Vector3.right;
            Vector3[] pointcloud = new Vector3[]{
                        (position + up * size.y / 2 + right * size.x / 2),
                        (position - up * size.y / 2 + right * size.x / 2),
                        (position - up * size.y / 2 - right * size.x / 2),
                        (position + up * size.y / 2 - right * size.x / 2),
                        (position + up * size.y / 2 + right * size.x / 2)
                    };
            return PosingToolHelper.DistanceToPointCloudConvexHull(pointcloud);
        }

        public static Vector3 mouseToPointOnPlane(Vector3 planeOrigin, Quaternion orientation, Vector2 mousePosition)
        {
            Ray mousePointRay = HandleUtility.GUIPointToWorldRay(mousePosition);
            Vector3 rayOrigin = Quaternion.Inverse(orientation) * (mousePointRay.origin - planeOrigin);
            Vector3 rayDirection = Quaternion.Inverse(orientation) * mousePointRay.direction;
            Vector3 dirNorm = rayDirection / rayDirection.z;
            Vector3 intersectionPos = rayOrigin - dirNorm * rayOrigin.z;
            intersectionPos = (orientation * intersectionPos) + planeOrigin;
            return intersectionPos;
        }
        
        public static Vector3 DragHandle(
            Vector3 position, Quaternion orientation, Vector2 handleSize, 
            Color color, Color colorSelected, Color colorHover, 
            out DragHandleResult result)
        {
            int id = GUIUtility.GetControlID(s_DragHandleHash, FocusType.Passive);
            lastDragHandleID = id;

            Vector3 screenPosition = Handles.matrix.MultiplyPoint(position);
            Matrix4x4 cachedMatrix = Handles.matrix;

            result = DragHandleResult.none;

            switch (Event.current.GetTypeForControl(id))
            {
                case EventType.MouseDown:
                    if (HandleUtility.nearestControl == id && (Event.current.button == 0 || Event.current.button == 1))
                    {
                        GUIUtility.hotControl = id;
                        s_DragHandleMouseCurrent = s_DragHandleMouseStart = Event.current.mousePosition;
                        s_DragHandleWorldStart = position;
                        s_DragHandleHasMoved = false;
                        s_DragHandleMouseOffset = mouseToPointOnPlane(Handles.matrix.MultiplyPoint(position), orientation, s_DragHandleMouseStart) - Handles.matrix.MultiplyPoint(position);

                        Event.current.Use();
                        EditorGUIUtility.SetWantsMouseJumping(1);

                        if (Event.current.button == 0)
                            result = DragHandleResult.LMBPress;
                        else if (Event.current.button == 1)
                            result = DragHandleResult.RMBPress;
                    }
                    break;

                case EventType.MouseUp:
                    if (GUIUtility.hotControl == id && (Event.current.button == 0 || Event.current.button == 1))
                    {
                        GUIUtility.hotControl = 0;
                        Event.current.Use();
                        EditorGUIUtility.SetWantsMouseJumping(0);

                        if (Event.current.button == 0)
                            result = DragHandleResult.LMBRelease;
                        else if (Event.current.button == 1)
                            result = DragHandleResult.RMBRelease;

                        if (Event.current.mousePosition == s_DragHandleMouseStart)
                        {
                            bool doubleClick = (s_DragHandleClickID == id) &&
                                (Time.realtimeSinceStartup - s_DragHandleClickTime < s_DragHandleDoubleClickInterval);

                            s_DragHandleClickID = id;
                            s_DragHandleClickTime = Time.realtimeSinceStartup;

                            if (Event.current.button == 0)
                                result = doubleClick ? DragHandleResult.LMBDoubleClick : DragHandleResult.LMBClick;
                            else if (Event.current.button == 1)
                                result = doubleClick ? DragHandleResult.RMBDoubleClick : DragHandleResult.RMBClick;
                        }
                    }
                    break;

                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == id)
                    {
                        //constrained to the x/y plane given by orientation
                        Vector3 planeOrigin = Handles.matrix.MultiplyPoint(position);
                        position = Handles.matrix.inverse.MultiplyPoint(mouseToPointOnPlane(planeOrigin, orientation, Event.current.mousePosition)) - s_DragHandleMouseOffset;

                        if (Camera.current.transform.forward == Vector3.forward || Camera.current.transform.forward == -Vector3.forward)
                            position.z = s_DragHandleWorldStart.z;
                        if (Camera.current.transform.forward == Vector3.up || Camera.current.transform.forward == -Vector3.up)
                            position.y = s_DragHandleWorldStart.y;
                        if (Camera.current.transform.forward == Vector3.right || Camera.current.transform.forward == -Vector3.right)
                            position.x = s_DragHandleWorldStart.x;

                        if (Event.current.button == 0)
                            result = DragHandleResult.LMBDrag;
                        else if (Event.current.button == 1)
                            result = DragHandleResult.RMBDrag;

                        s_DragHandleHasMoved = true;
                        
                        GUI.changed = true;
                        Event.current.Use();
                    }
                    break;

                case EventType.Repaint:

                    Color currentColour = Handles.color;

                    Handles.color = color;
                    //if (distanceToRect(screenPosition, orientation, handleSize) == 0) Handles.color = colorHover;
                    if (id == HandleUtility.nearestControl) Handles.color = colorHover;
                    if (id == GUIUtility.hotControl)
                    {
                        if (s_DragHandleHasMoved) Handles.color = colorSelected;
                    } 

                    Handles.matrix = Matrix4x4.identity;
                    //capFunc(id, screenPosition, Quaternion.identity, handleSize);
                    //Handles.SphereHandleCap(id, screenPosition, Quaternion.identity, handleSize, EventType.Ignore);
                    //Handles.DrawSolidDisc(screenPosition, orientation * Vector3.forward, handleSize);
                    Vector3 up = orientation * Vector3.up, right = orientation * Vector3.right;
                    Handles.DrawPolyLine(new Vector3[]{
                        (screenPosition + up * handleSize.y / 2 + right * handleSize.x / 2),
                        (screenPosition - up * handleSize.y / 2 + right * handleSize.x / 2),
                        (screenPosition - up * handleSize.y / 2 - right * handleSize.x / 2),
                        (screenPosition + up * handleSize.y / 2 - right * handleSize.x / 2),
                        (screenPosition + up * handleSize.y / 2 + right * handleSize.x / 2)
                    });
                    Handles.matrix = cachedMatrix;

                    Handles.color = currentColour;
                    break;

                case EventType.Layout:
                    Handles.matrix = Matrix4x4.identity;
                    // set selection area
                    //HandleUtility.AddControl(id, HandleUtility.DistanceToLine(position, position + direction * handleSize) / 2f); // line selection are
                    //HandleUtility.AddControl(id, HandleUtility.DistanceToCircle(screenPosition, handleSize));
                    HandleUtility.AddControl(id, distanceToRect(screenPosition, orientation, handleSize));
                    Handles.matrix = cachedMatrix;
                    break;
            }

            return position;
        }
    }

}