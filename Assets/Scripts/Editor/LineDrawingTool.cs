using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using Unity.Collections;
using Unity.Profiling;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using System;

namespace Icebreakers.Editor
{
    public class LineDrawingTool : EditorWindow
    {
        UWintab_Tablet tablet;

        [MenuItem("Window/Icebreakers/Line Drawing")]
        static void Initialize()
        {
            var window = EditorWindow.GetWindow<LineDrawingTool>();
            window.titleContent = new GUIContent("Line Drawing");
        }

        bool isInDrawingMode = false;

        NativeBVH<BVHPrimitive> drawingScene;
        Dictionary<int, RendererEntry> renderers = new Dictionary<int, RendererEntry>();

        float brushSize = 4;

        Vector3? lastBrushPosition;
        int lastBrushPrimIndex;
        DrawnLineRenderer activeLineRenderer;

        void OnDisable()
        {
            if (isInDrawingMode)
            {
                LeaveDrawingMode();
            }
        }

        void OnGUI()
        {
            string text = isInDrawingMode ? "Leave Drawing Mode" : "Enter Drawing Mode";
            bool newIsInDrawingMode = GUILayout.Toggle(isInDrawingMode, text, GUI.skin.button);
            if (newIsInDrawingMode != isInDrawingMode)
            {
                if (newIsInDrawingMode)
                {
                    EnterDrawingMode();
                }
                else
                {
                    LeaveDrawingMode();
                }
            }

            brushSize = EditorGUILayout.Slider(brushSize, 1, 20);
        }

        void EnterDrawingMode()
        {
            isInDrawingMode = true;
            SceneView.duringSceneGui += OnSceneGUI;
            if(tablet == null) tablet = new UWintab_Tablet();
            tablet.Init();
            SceneView.RepaintAll();
        }

        void LeaveDrawingMode()
        {
            isInDrawingMode = false;
            SceneView.duringSceneGui -= OnSceneGUI;
            tablet.Deinit();
            activeLineRenderer = null;
            ReleaseDrawingStructure();
            SceneView.RepaintAll();
        }

        Texture2D tex;
        void OnSceneGUI(SceneView sceneView)
        {
            Event evt = Event.current;

            if (evt.type == EventType.MouseDown && evt.control && evt.button == 0)
            {
                GameObject picked = HandleUtility.PickGameObject(evt.mousePosition, false);
                if (picked.TryGetComponent<Renderer>(out Renderer renderer))
                {
                    if (!picked.TryGetComponent<DrawnLineRenderer>(out DrawnLineRenderer lineRenderer))
                    {
                        lineRenderer = (DrawnLineRenderer)Undo.AddComponent(picked, typeof(DrawnLineRenderer));
                    }

                    activeLineRenderer = lineRenderer;

                    BuildDrawingStructure(renderer);
                }
                evt.Use();
            }

            if (activeLineRenderer != null)
            {

                if ((evt.type == EventType.MouseDown || evt.type == EventType.MouseDrag) && evt.button == 0)
                {
                    Ray ray = HandleUtility.GUIPointToWorldRay(evt.mousePosition);

                    NativeRay nativeRay = NativeRay.FromRay(ray, 0, float.PositiveInfinity);

                    Vector3? newBrushPosition = null;
                    int newBrushPrimIndex = -1;

                    Intersection isect = drawingScene.Intersect(new Intersector(), nativeRay);
                    if (isect.t != float.PositiveInfinity)
                    {
                        newBrushPosition = nativeRay.o + nativeRay.d * isect.t;
                        newBrushPrimIndex = isect.primitiveIndex;
                    }

                    if (lastBrushPosition.HasValue && newBrushPosition.HasValue
                        && lastBrushPosition != newBrushPosition)
                    {
                        BVHPrimitive prim = drawingScene.Primitives[newBrushPrimIndex];
                        Vector3 normal = Vector3.Normalize(Vector3.Cross(prim.v2 - prim.v1, prim.v3 - prim.v1));

                        Camera camera = sceneView.camera;
                        float relativeSize = brushSize * 2 / camera.pixelHeight;
                        float worldSize = Mathf.Tan(Mathf.Deg2Rad * camera.fieldOfView * 0.5f);

                        Undo.RecordObject(activeLineRenderer, "Draw Line");
                        Matrix4x4 worldToLocalMatrix = activeLineRenderer.transform.worldToLocalMatrix;
                        Vector3 localNormal = worldToLocalMatrix.MultiplyVector(normal);
                        Vector3 localP1 = worldToLocalMatrix.MultiplyPoint(lastBrushPosition.Value);
                        Vector3 localP2 = worldToLocalMatrix.MultiplyPoint(newBrushPosition.Value);
                        Vector3 localTangent = math.normalize(math.cross(localNormal, localP1 - localP2));
                        DrawnLine current = new DrawnLine
                        {
                            p1 = localP1,
                            p2 = localP2,
                            width1 = Vector3.Dot(camera.transform.forward, lastBrushPosition.Value - camera.transform.position) * worldSize * relativeSize,
                            width2 = Vector3.Dot(camera.transform.forward, newBrushPosition.Value - camera.transform.position) * worldSize * relativeSize,
                            p1Normal = localNormal,
                            p2Normal = localNormal,
                            tangent = localTangent,
                            p1Tangent = localTangent,
                            p2Tangent = localTangent,
                            end = false
                        };

                        if (activeLineRenderer.lines.Count > 0)
                        {
                            DrawnLine prev = activeLineRenderer.lines[activeLineRenderer.lines.Count - 1];
                            if (prev.end == false)
                            {
                                float angle = math.acos(math.dot(current.tangent, prev.tangent));
                                Vector3 pointTan = math.normalize(math.lerp(current.tangent, prev.tangent, 0.5f));
                                Vector3 pointNorm = math.normalize(math.lerp(current.p1Normal, prev.p2Normal, 0.5f));
                                prev.p2Normal = pointNorm;
                                current.p1Normal = pointNorm;
                                prev.p2Tangent = pointTan;
                                current.p1Tangent = pointTan;
                            }
                            activeLineRenderer.lines[activeLineRenderer.lines.Count - 1] = prev;
                        }

                        activeLineRenderer.lines.Add(current);
                        activeLineRenderer.RefreshMesh();
                    }

                    lastBrushPosition = newBrushPosition;
                    lastBrushPrimIndex = newBrushPrimIndex;

                    sceneView.Repaint();
                    evt.Use();
                }

                if (evt.type == EventType.MouseUp && evt.button == 0)
                {
                    lastBrushPosition = null;
                    Undo.RecordObject(activeLineRenderer, "Draw Line");
                    DrawnLine dl = activeLineRenderer.lines[activeLineRenderer.lines.Count - 1];
                    dl.end = true;
                    activeLineRenderer.lines[activeLineRenderer.lines.Count - 1] = dl;
                    evt.Use();
                }

                if (evt.type == EventType.Repaint)
                {
                    if (lastBrushPosition != null)
                    {
                        //Handles.color = Color.red;
                        //Handles.DrawSolidDisc(lastBrushPosition.Value, sceneView.camera.transform.forward, 0.05f);
                    }
                }
            }

            Handles.BeginGUI();

            Handles.color = Color.white;
            Handles.DrawWireDisc(evt.mousePosition, new Vector3(0, 0, 1), brushSize - 0.5f);
            Handles.color = Color.black;
            Handles.DrawWireDisc(evt.mousePosition, new Vector3(0, 0, 1), brushSize + 0.5f);

            Handles.EndGUI();

#if false
            if (evt.type == EventType.Repaint)
            {
                int width = 256;
                int height = 256;
                if (tex == null)
                {
                    tex = new Texture2D(width, height, TextureFormat.RGBA32, false);
                }

                NativeArray<Color32> pixels = new NativeArray<Color32>(width * height, Allocator.TempJob);
                NativeArray<Ray> rays = new NativeArray<Ray>(width * height, Allocator.TempJob);
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Ray ray = HandleUtility.GUIPointToWorldRay(new Vector2(x, y));
                        rays[x + (height - y - 1) * width] = ray;
                    }
                }

                new RenderDrawingStructureJob
                {
                    rays = rays,
                    pixels = pixels,
                    drawingScene = drawingScene,
                }.Schedule(pixels.Length, 256).Complete();

                tex.SetPixelData(pixels, 0);
                tex.Apply();

                pixels.Dispose();
                rays.Dispose();

                Handles.BeginGUI();

                GUI.DrawTexture(new Rect(0, 0, tex.width, tex.height), tex);

                Handles.EndGUI();

                // NativeArray<BVHNode> nodes = drawingScene.Nodes;
                // NativeArray<BVHPrimitive> primitives = drawingScene.Primitives;
                // foreach (BVHNode node in nodes)
                // {
                //     if (node.IsLeaf)
                //     {
                //         Handles.color = Color.white;
                //         for (int i = 0; i < node.primitiveCount; i++)
                //         {
                //             BVHPrimitive primitive = primitives[node.start + i];
                //             Handles.DrawPolyLine(primitive.v1, primitive.v2, primitive.v3, primitive.v1);
                //         }
                //     }
                //     else
                //     {
                //         Handles.color = Color.red;
                //         Handles.DrawWireCube(node.bbox.center, node.bbox.extents * 2);
                //     }
                // }
            }
#endif

            if (evt.type == EventType.Layout)
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }


        [BurstCompile]
        struct RenderDrawingStructureJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<Ray> rays;
            [WriteOnly, NativeMatchesParallelForLength]
            public NativeArray<Color32> pixels;

            [ReadOnly]
            public NativeBVH<BVHPrimitive> drawingScene;

            public void Execute(int index)
            {
                Color color = new Color32(0, 0, 0, 255);
                Ray ray = rays[index];

                NativeRay nativeRay = NativeRay.FromRay(ray, 0, float.PositiveInfinity);

                Intersection isect = drawingScene.Intersect(new Intersector(), nativeRay);
                if (isect.t != float.PositiveInfinity)
                {
                    color = new Color32(255, 255, 255, 255);
                }

                pixels[index] = color;
            }
        }

        static readonly ProfilerMarker BuildDrawingStructurePerfMarker = new ProfilerMarker("LineDrawingTool.BuildDrawingStructure");
        void BuildDrawingStructure(params Renderer[] toBuild)
        {
            using var _ = BuildDrawingStructurePerfMarker.Auto();

            ReleaseDrawingStructure();

            foreach (var renderer in toBuild)
            {
                if (renderer is MeshRenderer meshRenderer)
                {
                    MeshFilter meshFilter = meshRenderer.GetComponent<MeshFilter>();
                    if (meshFilter != null && meshFilter.sharedMesh != null)
                    {
                        RendererEntry entry = new RendererEntry();
                        entry.renderer = renderer;
                        entry.mesh = meshFilter.sharedMesh;
                        entry.isAllocated = false;
                        entry.rendererIndex = renderers.Count;

                        renderers.Add(entry.rendererIndex, entry);
                    }
                }
                else if (renderer is SkinnedMeshRenderer skinnedMeshRenderer)
                {
                    if (skinnedMeshRenderer.sharedMesh != null)
                    {
                        Mesh bakedMesh = new Mesh() { hideFlags = HideFlags.HideAndDontSave };
                        skinnedMeshRenderer.BakeMesh(bakedMesh);

                        RendererEntry entry = new RendererEntry();
                        entry.renderer = renderer;
                        entry.mesh = bakedMesh;
                        entry.isAllocated = true;
                        entry.rendererIndex = renderers.Count;

                        renderers.Add(entry.rendererIndex, entry);
                    }
                }
            }

            NativeList<BVHPrimitive> primitives = new NativeList<BVHPrimitive>(200_000, Allocator.TempJob);
            foreach (RendererEntry entry in renderers.Values)
            {
                var meshDataArray = MeshUtility.AcquireReadOnlyMeshData(entry.mesh);
                var meshData = meshDataArray[0];

                NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.TempJob);
                meshData.GetVertices(vertices);

                int numUsedSubMeshes = Mathf.Min(meshData.subMeshCount, entry.renderer.sharedMaterials.Length);
                for (int i = 0; i < numUsedSubMeshes; i++)
                {
                    SubMeshDescriptor descriptor = meshData.GetSubMesh(i);
                    if (descriptor.topology != MeshTopology.Triangles)
                        continue;

                    NativeArray<int> indices = new NativeArray<int>(descriptor.indexCount, Allocator.TempJob);
                    meshData.GetIndices(indices, i);

                    int primitiveCount = descriptor.indexCount / 3;
                    int offset = primitives.Length;
                    primitives.ResizeUninitialized(primitives.Length + primitiveCount);

                    new AppendPrimitivesJob
                    {
                        primitives = primitives,
                        primOffset = offset,
                        vertices = vertices,
                        indices = indices,
                        rendererIndex = entry.rendererIndex,
                        localToWorldMatrix = entry.renderer.localToWorldMatrix,
                    }.Schedule(primitiveCount, 256).Complete();

                    indices.Dispose();
                }

                vertices.Dispose();

                meshDataArray.Dispose();
            }

            drawingScene = new NativeBVH<BVHPrimitive>(primitives.AsArray(), Allocator.Persistent);
            primitives.Dispose();

            drawingScene.BuildJob(4).Complete();
        }

        [BurstCompile(CompileSynchronously = true)]
        struct AppendPrimitivesJob : IJobParallelFor
        {
            [NativeDisableParallelForRestriction]
            public NativeList<BVHPrimitive> primitives;
            public int primOffset;

            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> vertices;
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<int> indices;

            public int rendererIndex;

            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                int index1 = indices[index * 3 + 0];
                int index2 = indices[index * 3 + 1];
                int index3 = indices[index * 3 + 2];

                float3 vertex1 = math.transform(localToWorldMatrix, vertices[index1]);
                float3 vertex2 = math.transform(localToWorldMatrix, vertices[index2]);
                float3 vertex3 = math.transform(localToWorldMatrix, vertices[index3]);

                primitives[primOffset + index] = new BVHPrimitive
                {
                    v1 = vertex1,
                    v2 = vertex2,
                    v3 = vertex3,
                    rendererIndex = rendererIndex,
                };
            }
        }

        void ReleaseDrawingStructure()
        {
            foreach (var entry in renderers.Values)
            {
                if (entry.isAllocated)
                {
                    GameObject.DestroyImmediate(entry.mesh);
                }
            }

            renderers.Clear();

            if (drawingScene.IsCreated)
            {
                drawingScene.Dispose();
                drawingScene = default;
            }
        }

        class RendererEntry
        {
            public Renderer renderer;
            public Mesh mesh;
            public bool isAllocated;
            public int rendererIndex;
        }

        struct BVHPrimitive : IPrimitive
        {
            public float3 v1;
            public float3 v2;
            public float3 v3;
            public int rendererIndex;

            public Bounds GetBounds()
            {
                float3 min = Vector3.Min(Vector3.Min(v1, v2), v3);
                float3 max = Vector3.Max(Vector3.Max(v1, v2), v3);
                return new Bounds((min + max) * 0.5f, max - min);
            }

            public Vector3 GetCentroid()
            {
                return (v1 + v2 + v3) * (1 / 3.0f);
            }
        }

        struct Intersector : IIntersectionTest<BVHPrimitive>
        {
            //IntersectionPrimitives.RayTrianglePrecomputed precomputed;

            public float IntersectWith(in BVHPrimitive primitive, in NativeRay ray)
            {
                bool intersect = IntersectionPrimitives.IntersectRayTriangle2(
                    primitive.v1, primitive.v2, primitive.v3, ray,
                    out float t, out float b0, out float b1, out float b2);

                if (!intersect)
                {
                    return float.PositiveInfinity;
                }

                return t;
            }

            public void Precompute(in NativeRay ray)
            {
                //precomputed = IntersectionPrimitives.PrecomputeRayTriangle(ray);
            }
        }
    }
}