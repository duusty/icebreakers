using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.AssetImporters;

public class AssetPostprocessor : UnityEditor.AssetPostprocessor
{
    public override int GetPostprocessOrder()
    {
        return 2;
    }

    public void OnPreprocessMaterialDescription(MaterialDescription description, Material material, AnimationClip[] animations)
    {
        Shader shader = Shader.Find("Icebreakers/Standard");
        if (shader == null)
            return;

        material.shader = shader;

        if (description.TryGetProperty("DiffuseColor", out Vector4 value))
        {
            Color color = value;
            material.SetColor("_BaseColor", color);

            color.r *= 0.5f;
            color.g *= 0.5f;
            color.b *= 0.5f;
            material.SetColor("_ShadowColor", color);
        }
    }
}
