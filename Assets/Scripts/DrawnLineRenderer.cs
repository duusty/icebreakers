using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Collections;
using UnityEngine;
using Unity.Profiling;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Rendering;

namespace Icebreakers
{
    [ExecuteAlways]
    public class DrawnLineRenderer : MonoBehaviour
    {
        [SerializeField]
        public List<DrawnLine> lines = new List<DrawnLine>();

        [NonSerialized]
        DrawnLineMesh drawnLineMesh;

        Renderer sourceRenderer;
        public Renderer SourceRenderer => sourceRenderer;

        public static readonly HashSet<DrawnLineRenderer> drawnLineRenderers = new HashSet<DrawnLineRenderer>();

        void OnEnable()
        {
            if (!drawnLineRenderers.Add(this))
            {
                Debug.LogError("Failed to add drawn line renderer, state has been corrupted");
            }

            sourceRenderer = GetComponent<Renderer>();
        }

        void OnDisable()
        {
            if (!drawnLineRenderers.Remove(this))
            {
                Debug.LogError("Failed to remove drawn line renderer, state has been corrupted");
            }

            if (drawnLineMesh != null)
            {
                drawnLineMesh.Dispose();
                drawnLineMesh = null;
            }
        }

        void OnValidate()
        {
            RefreshMesh();
        }

        public void RefreshMesh()
        {
            if (drawnLineMesh == null)
                return;

            drawnLineMesh.Refresh(lines);
        }

        public void GenerateForCamera(Camera camera, in LineGenerationContext context)
        {
            if (!enabled)
                throw new Exception("Drawn Lines Renderer is disabled");

            if (camera == null)
                throw new ArgumentNullException(nameof(camera));

            EnsureDrawnLineMesh();

            if (drawnLineMesh != null)
            {
                drawnLineMesh.GenerateLines(
                    transform.localToWorldMatrix,
                    context);
            }
        }

        public bool GetDrawingParams(out DrawnLineMeshDrawingArgs args)
        {
            if (!enabled)
                throw new Exception("Drawn Lines Renderer is disabled");

            EnsureDrawnLineMesh();

            if (drawnLineMesh != null)
            {
                drawnLineMesh.GetDrawingParams(
                    transform.localToWorldMatrix,
                    out args);

                return true;
            }

            args = default;
            return false;
        }

        void EnsureDrawnLineMesh()
        {
            if (drawnLineMesh == null)
            {
                drawnLineMesh = new DrawnLineMesh(lines);
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct DrawnLine
    {
        public Vector3 p1;
        public Vector3 p2;
        public float width1;
        public float width2;
        public Vector3 tangent;
        public Vector3 p1Normal;
        public Vector3 p2Normal;
        public Vector3 p1Tangent;
        public Vector3 p2Tangent;
        public bool end;
    }

    public struct DrawnLineMeshDrawingArgs
    {
        public Mesh lineMesh;
        public Matrix4x4 localToWorldMatrix;
    }

    public class DrawnLineMesh : IDisposable
    {
        NativeList<DrawnLine> drawnLines;

        Mesh lineMesh;

        static readonly ProfilerMarker ConstructDrawnLineMeshPerfMarker = new ProfilerMarker("DrawnLineMesh.Construct");

        public DrawnLineMesh(List<DrawnLine> lines)
        {
            if (lines == null)
                throw new ArgumentNullException();

            ConstructDrawnLineMeshPerfMarker.Begin();

            drawnLines = new NativeList<DrawnLine>(lines.Count, Allocator.Persistent);
            foreach (var line in lines)
            {
                drawnLines.AddNoResize(line);
            }

            lineMesh = new Mesh() { name = "Drawn Line Mesh" };
            lineMesh.hideFlags = HideFlags.HideAndDontSave;

            GenerateMesh();

            ConstructDrawnLineMeshPerfMarker.End();
        }

        public void Dispose()
        {
            drawnLines.Dispose();
            GameObject.DestroyImmediate(lineMesh);
        }

        public void Refresh(List<DrawnLine> lines)
        {
            drawnLines.ResizeUninitialized(lines.Count);
            for (int i = 0; i < lines.Count; i++)
            {
                drawnLines[i] = lines[i];
            }

            GenerateMesh();
        }

        public void GetDrawingParams(Matrix4x4 localToWorldMatrix, out DrawnLineMeshDrawingArgs args)
        {
            args.localToWorldMatrix = localToWorldMatrix;
            args.lineMesh = lineMesh;
        }

        static readonly ProfilerMarker GenerateMeshPerfMarker = new ProfilerMarker("DrawnLinesMesh.GenerateMesh");
        void GenerateMesh()
        {
            using var _ = GenerateMeshPerfMarker.Auto();

            NativeArray<float3> vertices = new NativeArray<float3>(drawnLines.Length * 4, Allocator.TempJob);
            NativeArray<float4> sides = new NativeArray<float4>(drawnLines.Length * 4, Allocator.TempJob);
            NativeArray<int> indices = new NativeArray<int>(drawnLines.Length * 6, Allocator.TempJob);

            new GenerateMeshJob
            {
                drawnLines = drawnLines,
                vertices = vertices,
                sides = sides,
                indices = indices,
            }.Schedule(drawnLines.Length, 64).Complete();

            lineMesh.Clear();
            lineMesh.subMeshCount = 1;
            lineMesh.indexFormat = vertices.Length < ushort.MaxValue ? IndexFormat.UInt16 : IndexFormat.UInt32;

            lineMesh.SetVertices(vertices);
            lineMesh.SetUVs(0, sides);
            lineMesh.SetIndices(indices, MeshTopology.Triangles, 0);

            lineMesh.UploadMeshData(false);

            vertices.Dispose();
            sides.Dispose();
            indices.Dispose();
        }

        [BurstCompile]
        public struct GenerateMeshJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<DrawnLine> drawnLines;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<float3> vertices;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<float4> sides;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<int> indices;

            public void Execute(int index)
            {
                DrawnLine line = drawnLines[index];
                int vertexOffset = index * 4;
                int indexOffset = index * 6;

                vertices[vertexOffset + 0] = line.p1 + line.p1Normal * 0.001f;
                vertices[vertexOffset + 1] = line.p1 + line.p1Normal * 0.001f;
                vertices[vertexOffset + 2] = line.p2 + line.p2Normal * 0.001f;
                vertices[vertexOffset + 3] = line.p2 + line.p2Normal * 0.001f;

                //float3 side = math.normalize(math.cross(line.p2 - line.p1, line.normal));
                sides[vertexOffset + 0] = new float4(- line.p1Tangent, line.width1);
                sides[vertexOffset + 1] = new float4(line.p1Tangent, line.width1);
                sides[vertexOffset + 2] = new float4(-line.p2Tangent, line.width2);
                sides[vertexOffset + 3] = new float4(line.p2Tangent, line.width2);

                indices[indexOffset + 0] = vertexOffset + 0;
                indices[indexOffset + 1] = vertexOffset + 1;
                indices[indexOffset + 2] = vertexOffset + 3;
                indices[indexOffset + 3] = vertexOffset + 0;
                indices[indexOffset + 4] = vertexOffset + 3;
                indices[indexOffset + 5] = vertexOffset + 2;
            }
        }

        public void GenerateLines(Matrix4x4 localToWorldMatrix, LineGenerationContext context)
        {
            GenerateMeshPerfMarker.Begin();

            NativeArray<LineGeneration.Line> lines = new NativeArray<LineGeneration.Line>(drawnLines.Length, Allocator.TempJob);

            int numVertices = lines.Length * 4;
            int numIndices = lines.Length * 6;

            if (context.vertexData.Length + numVertices > context.vertexData.Capacity)
            {
                context.vertexData.Capacity = Mathf.Max(2 * context.vertexData.Length, context.vertexData.Length + numVertices);
            }

            if (context.indices.Length + numIndices > context.indices.Capacity)
            {
                context.indices.Capacity = Mathf.Max(2 * context.indices.Length, context.indices.Length + numIndices);
            }

            JobHandle jobHandle = default;

            jobHandle = new TransformLinesJob
            {
                drawnLines = drawnLines,
                lines = lines,
                localToWorldMatrix = localToWorldMatrix,
            }.Schedule(drawnLines.Length, 256, jobHandle);

            jobHandle = new LineGeneration.GenerateLineDataJob
            {
                lines = lines,
                indices = context.indices,
                vertexData = context.vertexData,
                indexOffset = context.vertexData.Length,
            }.Schedule(jobHandle);

            jobHandle.Complete();

            lines.Dispose();

            GenerateMeshPerfMarker.End();
        }

        [BurstCompile]
        struct TransformLinesJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<DrawnLine> drawnLines;

            [WriteOnly, NativeMatchesParallelForLength]
            public NativeArray<LineGeneration.Line> lines;

            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                var line = drawnLines[index];
                line.p1 = math.transform(localToWorldMatrix, line.p1);
                line.p2 = math.transform(localToWorldMatrix, line.p2);
                lines[index] = new LineGeneration.Line { p1 = line.p1, p2 = line.p2 };
            }
        }
    }
}