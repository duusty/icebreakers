using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    public class MaterialOverride : MonoBehaviour
    {
        public bool overrideLightDirection = false;
        public Quaternion lightDirectionQuaternion = Quaternion.identity;
        public Vector3 lightDirection = Vector3.forward;

        public bool overrideAlbedo = false;
        public Texture2D albedo = null;

        public bool overrideColor = false;
        public Color color = Color.black;

        public bool overrideUseShadowmap = false;
        public bool useShadowmap = false;

        public bool overrideShadowAlbedo = false;
        public Texture2D shadowAlbedo = null;

        public bool overrideShadowColor = false;
        public Color shadowColor = Color.black;

        public bool overrideHatchingLineCount = false;
        public float hatchingLineCount = 200;

        public bool overrideHatchingRatio = false;
        public float hatchingRatio = 1;

        public bool overrideShadowOutlineWidth = false;
        public float shadowOutlineWidth = 1;

        void OnValidate()
        {
            ApplyOverride();
        }

        private void Awake()
        {
            ApplyOverride();
        }

        public void ApplyOverride()
        {
            var renderer = GetComponent<Renderer>();
            if (renderer == null)
            {
                return;
            }

            var properties = new MaterialPropertyBlock();

            if (overrideLightDirection)
            {
                Vector3 lightDir = transform.right * lightDirection.x + transform.up * lightDirection.y + transform.forward * lightDirection.z; //lightDirection * transform.forward;// Vector3.forward;
                properties.SetVector("_OverrideLightDirection", new Vector4(lightDir.x, lightDir.y, lightDir.z, overrideLightDirection ? 1 : 0));
            }

            if (overrideAlbedo)
            {
                properties.SetTexture("_BaseMap", albedo != null ? albedo : Texture2D.whiteTexture);
            }

            if (overrideColor)
            {
                properties.SetColor("_BaseColor", color);
            }

            if (overrideUseShadowmap)
            {
                properties.SetFloat("_UseShadowMap", useShadowmap ? 1 : 0);
            }

            if (overrideShadowAlbedo)
            {
                properties.SetTexture("_ShadowMap", shadowAlbedo != null ? shadowAlbedo : Texture2D.whiteTexture);
            }

            if (overrideShadowColor)
            {
                properties.SetColor("_ShadowColor", shadowColor);
            }

            if (overrideHatchingLineCount)
            {
                properties.SetFloat("_HatchingLineCount", hatchingLineCount);
            }

            if (overrideHatchingRatio)
            {
                properties.SetFloat("_HatchingRatio", hatchingRatio);
            }

            if (overrideShadowOutlineWidth)
            {
                properties.SetFloat("_ShadowOutlineWidth", shadowOutlineWidth);
            }

            renderer.SetPropertyBlock(properties);
        }
    }
}