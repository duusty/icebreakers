using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;
using Unity.Rendering;
using UnityEngine.Experimental.Rendering;
using System;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Profiling;

namespace Icebreakers
{
    public struct LineGenerationContext
    {
        public NativeList<int> indices;
        public NativeList<LineGeneration.VertexData> vertexData;
    }

    public class RenderPipeline : UnityEngine.Rendering.RenderPipeline
    {
        ShaderTagId basePassTag = new ShaderTagId("Base");

        int tempColorId = Shader.PropertyToID("_TempColorBuffer");
        int tempDepthId = Shader.PropertyToID("_TempDepthBuffer");

        const int STENCIL_PASS_FORWARD = 0;
        const int STENCIL_PASS_INVERTED = 1;
        const int STENCIL_PASS_CLEAR_DEPTH = 2;
        const int STENCIL_PASS_CLEAR_INITIAL = 3;
        const int STENCIL_PASS_DEBUG = 4;

        Material postProcessMaterial;
        Material[] outlineMaterials;
        Material[] drawnLineMaterials;
        Material[] panelStencilMaterials;
        Material invertedPanelStencilMaterial;
        Texture2D blankPanelMask;
        Mesh panelStencilMesh;

        RenderPipelineResources resources;
        RenderPipelineAsset settings;

        MaterialPropertyBlock outlineProperties = new MaterialPropertyBlock();
        MaterialPropertyBlock drawnLineProperties = new MaterialPropertyBlock();
        MaterialPropertyBlock panelStencilProperties = new MaterialPropertyBlock();

        class OutlineMeshState : IDisposable
        {
            public Mesh mesh;
            public ComputeBuffer lineBuffer;
            public ComputeBuffer drawArgsBuffer;

            public OutlineMeshState()
            {
                mesh = new Mesh();
                mesh.hideFlags = HideFlags.HideAndDontSave;
                mesh.MarkDynamic();

                if (SystemInfo.supportsComputeShaders)
                {
                    // TODO: can we make the maximum size of the line buffer dynamic?
                    drawArgsBuffer = new ComputeBuffer(1, 16, ComputeBufferType.IndirectArguments, ComputeBufferMode.Immutable);
                    lineBuffer = new ComputeBuffer(200_000, 24, ComputeBufferType.Append, ComputeBufferMode.Immutable);
                }
            }

            public void Dispose()
            {
                if (mesh != null)
                {
                    GameObject.DestroyImmediate(mesh);
                }

                if (drawArgsBuffer != null)
                {
                    drawArgsBuffer.Dispose();
                }

                if (lineBuffer != null)
                {
                    lineBuffer.Dispose();
                }
            }
        }

        readonly Stack<OutlineMeshState> cachedOutlineMeshes = new Stack<OutlineMeshState>();
        readonly List<OutlineMeshState> usedOutlineMeshes = new List<OutlineMeshState>();
        readonly Dictionary<Panel, OutlineMeshState> outlineMeshesForPanels = new Dictionary<Panel, OutlineMeshState>();

        struct DrawingState
        {
            public Camera camera;
            public CullingResults cullingResults;
            public bool stereoEnabled;
        }

        public RenderPipeline(RenderPipelineAsset settings, RenderPipelineResources resources)
        {
            this.resources = resources;
            this.settings = settings;
            postProcessMaterial = new Material(resources.postProcessShader) { hideFlags = HideFlags.HideAndDontSave };
            // XXX: Because we cannot change rendering state with MaterialPropertyBlock or SetGlobalProperty
            // we need to create all materials with the correct rendering state beforehand...
            outlineMaterials = new Material[32];
            for (int i = 0; i < 32; i++)
            {
                var material = new Material(resources.outlineShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                outlineMaterials[i] = material;
            }
            drawnLineMaterials = new Material[32];
            for (int i = 0; i < 32; i++)
            {
                var material = new Material(resources.drawnLineShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                drawnLineMaterials[i] = material;
            }
            panelStencilMaterials = new Material[32];
            for (int i = 0; i < panelStencilMaterials.Length; i++)
            {
                var material = new Material(resources.panelStencilShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                panelStencilMaterials[i] = material;
            }
            invertedPanelStencilMaterial = new Material(resources.panelStencilShader) { hideFlags = HideFlags.HideAndDontSave };
            invertedPanelStencilMaterial.SetFloat("_PanelNumber", 100);

            blankPanelMask = new Texture2D(1, 1, TextureFormat.Alpha8, false) { hideFlags = HideFlags.HideAndDontSave };
            blankPanelMask.SetPixels32(new Color32[] { new Color32(255, 255, 255, 255) });
            blankPanelMask.Apply(true, true);

            panelStencilMesh = new Mesh() { hideFlags = HideFlags.HideAndDontSave };
            Vector3[] vertices = new Vector3[]
            {
                new Vector3(-0.5f, -0.5f, 0),
                new Vector3(-0.5f,  0.5f, 0),
                new Vector3( 0.5f, -0.5f, 0),
                new Vector3( 0.5f,  0.5f, 0),

                new Vector3(-0.5f, -0.5f, -1f),
                new Vector3(-0.5f,  0.5f, -1f),
                new Vector3( 0.5f, -0.5f, -1f),
                new Vector3( 0.5f,  0.5f, -1f),
            };

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),
            };

            int[] indices = new int[6]
            {
                0, 3, 1,
                0, 2, 3,
            };

            int[] indicesCube = new int[]
            {
                4, 7, 5,
                4, 6, 7,
                4, 5, 1,
                4, 1, 0,
                6, 3, 7,
                6, 2, 3,
                5, 3, 1,
                5, 7, 3,
                4, 2, 6,
                4, 0, 2,
            };

            panelStencilMesh.subMeshCount = 2;
            panelStencilMesh.vertices = vertices;
            panelStencilMesh.uv = uvs;
            panelStencilMesh.SetIndices(indices, MeshTopology.Triangles, 0);
            panelStencilMesh.SetIndices(indicesCube, MeshTopology.Triangles, 1);
            panelStencilMesh.UploadMeshData(true);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            GameObject.DestroyImmediate(postProcessMaterial);
            foreach (var material in outlineMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            foreach (var material in drawnLineMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            foreach (var material in panelStencilMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            GameObject.DestroyImmediate(invertedPanelStencilMaterial);
            GameObject.DestroyImmediate(blankPanelMask);
            GameObject.DestroyImmediate(panelStencilMesh);

            foreach (var state in cachedOutlineMeshes)
            {
                state.Dispose();
            }

            foreach (var state in usedOutlineMeshes)
            {
                state.Dispose();
            }
        }

        protected override void Render(ScriptableRenderContext context, Camera[] cameras)
        {
            if (QualitySettings.antiAliasing != settings.MSAASamples)
            {
                QualitySettings.antiAliasing = settings.MSAASamples;
                UnityEngine.XR.XRDevice.UpdateEyeTextureMSAASetting();
            }

            Page page = GameObject.FindObjectOfType<Page>();

            UpdateSkinnedOutlines(context);

            ResetOutlineMeshes();

            foreach (var camera in cameras)
            {
                if (!camera.TryGetCullingParameters(out ScriptableCullingParameters cullingParameters))
                {
                    Debug.LogError("Failed to get culling parameters from camera", camera);
                    continue;
                }
                var cullingResults = context.Cull(ref cullingParameters);

                RenderTargetIdentifier target = camera.targetTexture != null ? new RenderTargetIdentifier(camera.targetTexture) : new RenderTargetIdentifier(BuiltinRenderTextureType.CameraTarget);

                int renderWidth = camera.pixelWidth;
                int renderHeight = camera.pixelHeight;
                bool stereoEnabled = IsStereoEnabled(camera);
                int eyeCount = stereoEnabled ? 2 : 1;

                DrawingState drawingState = new DrawingState
                {
                    camera = camera,
                    cullingResults = cullingResults,
                    stereoEnabled = stereoEnabled,
                };

                bool isValidPageScene = page != null && (camera.cameraType == CameraType.Game || camera.cameraType == CameraType.SceneView);

                for (int eyeIndex = 0; eyeIndex < eyeCount; eyeIndex++)
                {
                    context.SetupCameraProperties(camera, stereoEnabled, eyeIndex);

                    {
                        var cmd = CommandBufferPool.Get("Set globals");

                        SetGlobalVariables(cmd, page, isValidPageScene);

                        context.ExecuteCommandBuffer(cmd);
                        CommandBufferPool.Release(cmd);
                    }

                    if (stereoEnabled)
                    {
                        context.StartMultiEye(camera, eyeIndex);
                    }

                    {
                        var cmd = CommandBufferPool.Get("Bind Rendertargets");

                        bool clearColor = camera.clearFlags == CameraClearFlags.SolidColor;
                        bool clearDepth = camera.clearFlags != CameraClearFlags.Nothing;
                        cmd.SetRenderTarget(target);

                        context.ExecuteCommandBuffer(cmd);
                        CommandBufferPool.Release(cmd);
                    }

                    if (isValidPageScene)
                    {
                        Panel panelInEditMode = null;
                        foreach (var panel in page.Panels)
                        {
                            if (panel.isInEditMode)
                            {
                                panelInEditMode = panel;
                                break;
                            }
                        }

                        if (panelInEditMode != null)
                        {
                            DrawPanelInEditMode(context, drawingState, panelInEditMode);
                        }
                        else
                        {
                            DrawPanels(context, drawingState, page);
                        }
                    }
                    else
                    {
                        OutlineMeshState outlineMeshState = GetOutlineMesh();
                        if (camera.cameraType == CameraType.Game || camera.cameraType == CameraType.SceneView)
                        {
                            GenerateOutlines(camera, outlineMeshState, context, uint.MaxValue);
                        }

                        {
                            var cmd = CommandBufferPool.Get("Clear");

                            bool clearColor = camera.clearFlags == CameraClearFlags.SolidColor;
                            bool clearDepth = camera.clearFlags != CameraClearFlags.Nothing;
                            cmd.ClearRenderTarget(clearDepth, clearColor, camera.backgroundColor);

                            if (stereoEnabled)
                            {
                                XRUtils.DrawOcclusionMesh(cmd, camera);
                            }

                            context.ExecuteCommandBuffer(cmd);
                            CommandBufferPool.Release(cmd);
                        }

                        {
                            var cmd = CommandBufferPool.Get("Clear");

                            SetPerPanelVariables(null, cmd);

                            context.ExecuteCommandBuffer(cmd);
                            CommandBufferPool.Release(cmd);
                        }


                        SortingSettings sortingSettings = new SortingSettings(camera);
                        sortingSettings.criteria = SortingCriteria.CommonOpaque;
                        DrawingSettings drawingSettings = new DrawingSettings(basePassTag, sortingSettings);
                        FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque, camera.cullingMask);
                        RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
                        stateBlock.stencilState = new StencilState(enabled: false);
                        context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

                        if (camera.cameraType == CameraType.Game || camera.cameraType == CameraType.SceneView)
                        {
                            RenderDrawnLines(context, camera, uint.MaxValue, 0, false);
                            RenderOutlines(context, outlineMeshState, 0, false);
                        }

                        if (camera.clearFlags == CameraClearFlags.Skybox)
                        {
                            context.DrawSkybox(camera);
                        }

                        sortingSettings.criteria = SortingCriteria.CommonTransparent;
                        filteringSettings.renderQueueRange = RenderQueueRange.transparent;
                        context.DrawRenderers(cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);
                    }

#if UNITY_EDITOR
                    if (UnityEditor.Handles.ShouldRenderGizmos())
                    {
                        context.DrawGizmos(camera, GizmoSubset.PreImageEffects);
                        context.DrawGizmos(camera, GizmoSubset.PostImageEffects);
                    }
#endif

                    if (stereoEnabled)
                    {
                        context.StopMultiEye(camera);
                        bool isFinalPass = eyeIndex == eyeCount - 1;
                        context.StereoEndRender(camera, eyeIndex, isFinalPass);
                    }
                }

                context.Submit();
            }
        }

        OutlineMeshState GetOutlineMesh()
        {
            OutlineMeshState outlineMeshState;
            if (cachedOutlineMeshes.Count > 0)
            {
                outlineMeshState = cachedOutlineMeshes.Pop();
            }
            else
            {
                outlineMeshState = new OutlineMeshState();
            }
            usedOutlineMeshes.Add(outlineMeshState);
            return outlineMeshState;
        }

        void ResetOutlineMeshes()
        {
            foreach (var outlineMeshState in usedOutlineMeshes)
            {
                cachedOutlineMeshes.Push(outlineMeshState);
            }
            usedOutlineMeshes.Clear();
        }

        public static void SetGlobalVariables(CommandBuffer cmd, Page page, bool isValidPageScene)
        {
            Vector3 viewOrigin = Vector3.zero;
            Quaternion viewOriginRotation = Quaternion.identity;

            if (isValidPageScene)
            {
                viewOrigin = page.ViewOrigin;
                viewOriginRotation = page.ViewOriginRotation;

                foreach (var panel in page.Panels)
                {
                    if (panel.isInEditMode)
                    {
                        viewOrigin = panel.localViewOrigion.transform.position;
                        viewOriginRotation = panel.localViewOrigion.transform.rotation;
                        break;
                    }
                }
            }

            Shader.SetGlobalVector("_ViewOrigin", viewOrigin);
            Shader.SetGlobalVector("_ViewOriginForward", viewOriginRotation * new Vector3(0, 0, 1));
            Shader.SetGlobalVector("_ViewOriginUp", viewOriginRotation * new Vector3(0, 1, 0));
            Shader.SetGlobalVector("_ViewOriginRight", viewOriginRotation * new Vector3(1, 0, 0));
        }

        public static void SetPerPanelVariables(Panel panel, CommandBuffer cmd)
        {
            Vector3 lightDir = Vector3.Normalize(new Vector3(1, -1, -1));
            Color lightColor = Color.white;

            if (panel != null)
            {
                Light sceneLight = panel.SceneLight;
                if (sceneLight != null)
                {
                    lightDir = sceneLight.transform.forward;
                    lightColor = sceneLight.color;
                }
            }

            cmd.SetGlobalVector("_SceneLightDir", lightDir);
            cmd.SetGlobalVector("_SceneLightColor", lightColor);
        }

        void DrawPanelInEditMode(ScriptableRenderContext context, in DrawingState state, Panel panel)
        {
            uint renderingLayer = 1;

            OutlineMeshState outlineMeshState = GetOutlineMesh();

            GenerateOutlines(state.camera, outlineMeshState, context, renderingLayer);

            {
                var cmd = CommandBufferPool.Get("Clear");

                bool clearColor = state.camera.clearFlags == CameraClearFlags.SolidColor;
                bool clearDepth = state.camera.clearFlags != CameraClearFlags.Nothing;
                cmd.ClearRenderTarget(clearDepth, clearColor, state.camera.backgroundColor);

                if (state.stereoEnabled)
                {
                    XRUtils.DrawOcclusionMesh(cmd, state.camera);
                }

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            {
                var cmd = CommandBufferPool.Get("Set Global Variables");

                SetPerPanelVariables(panel, cmd);

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            SortingSettings sortingSettings = new SortingSettings(state.camera);
            sortingSettings.criteria = SortingCriteria.CommonOpaque;
            DrawingSettings drawingSettings = new DrawingSettings(basePassTag, sortingSettings);
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque, state.camera.cullingMask, renderingLayer);

            RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
            stateBlock.stencilState = new StencilState(enabled: false);
            context.DrawRenderers(state.cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

            RenderDrawnLines(context, state.camera, renderingLayer, 0, false);
            RenderOutlines(context, outlineMeshState, 0, false);

            {
                var cmd = CommandBufferPool.Get("Draw Panel");

                PanelView panelView = panel.PanelView;
                if (panelView != null)
                {
                    Matrix4x4 matrix = Matrix4x4.TRS(
                        panelView.transform.position,
                        panelView.transform.rotation,
                        new Vector3(panelView.Size.x, panelView.Size.y, 0.02f)
                    );

                    panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    cmd.DrawMesh(panelStencilMesh, matrix, panelStencilMaterials[0], 0, STENCIL_PASS_DEBUG, panelStencilProperties);
                }

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            if (state.camera.clearFlags == CameraClearFlags.Skybox)
            {
                context.DrawSkybox(state.camera);
            }

            sortingSettings.criteria = SortingCriteria.CommonTransparent;
            filteringSettings.renderQueueRange = RenderQueueRange.transparent;
            context.DrawRenderers(state.cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);
        }

        void DrawPanels(ScriptableRenderContext context, in DrawingState state, Page page)
        {
            Panel invertedPanel = null;
            foreach (var panel in page.Panels)
            {
                if (!panel.isActiveAndEnabled)
                    continue;

                if (panel.IsInside)
                {
                    invertedPanel = panel;
                    break;
                }
            }

            outlineMeshesForPanels.Clear();
            foreach (var panel in page.Panels)
            {
                if (!panel.isActiveAndEnabled)
                    continue;

                PanelView panelView = panel.PanelView;
                if (panelView == null)
                    continue;

                OutlineMeshState outlineMeshState = GetOutlineMesh();
                outlineMeshesForPanels.Add(panel, outlineMeshState);

                uint renderingLayerMask = 1u << panel.PanelNumber;
                GenerateOutlines(state.camera, outlineMeshState, context, renderingLayerMask);
            }

            SortingSettings sortingSettings = new SortingSettings(state.camera);
            sortingSettings.criteria = SortingCriteria.CommonOpaque;
            DrawingSettings drawingSettings = new DrawingSettings(basePassTag, sortingSettings);
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque, state.camera.cullingMask);


            // For clipping objects in panels to their panel rectangle we fill the stencil buffer
            // with an index for each panel and test for this index during drawing. The background
            // is always set to 0; panel indices start at 1.
            // Using the stencil buffer allows for sorting panels based on depth, so panels in front
            // of others do occlude what is behind them. Using the shader test for the panel would mix
            // the contents of the panels because the depth test does not include the panel geometry.
            // When we are inside of a panel the process becomes more complicated. The inverted panel
            // cannot use the stencil based test, because we need to draw geometry that occludes the
            // panel itself, so we fall back to the shader test for the inverted panel.
            // When looking to the outside from inside the inverted panel we need to draw the other panels
            // only in the places where we are looking outside. We do this by drawing the inverted panel
            // with a 0 stencil to a background cleared with a large stencil value. This way the greater stencil
            // test will only succeed on pixels where the inverted panel was drawn and fail on the background.
            // The panels will also still be sorted correctly based on depth. This method does not cover the case
            // where a panel is placed on the "inside" of the inverted panel and oriented towards the inside.

            {
                var cmd = CommandBufferPool.Get("Clear before Panel Stencils");

                // clear depth and write background stencil
                Material clearMaterial = invertedPanel != null ? invertedPanelStencilMaterial : panelStencilMaterials[0];
                cmd.DrawMesh(panelStencilMesh, Matrix4x4.identity, clearMaterial, 0, STENCIL_PASS_CLEAR_INITIAL);

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            {
                filteringSettings.renderingLayerMask = 1;
                RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
                stateBlock.stencilState = new StencilState(enabled: false);
                context.DrawRenderers(state.cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);
            }

            {
                var cmd = CommandBufferPool.Get("Draw Panel Stencils");

                if (invertedPanel != null)
                {
                    PanelView panelView = invertedPanel.PanelView;
                    if (panelView != null)
                    {
                        Matrix4x4 matrix = Matrix4x4.TRS(
                            panelView.transform.position,
                            panelView.transform.rotation,
                            panelView.Size
                        );

                        panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                        cmd.DrawMesh(panelStencilMesh, matrix, panelStencilMaterials[0], 0, STENCIL_PASS_INVERTED, panelStencilProperties);
                    }
                }

                // Draw panels from smaller to larger, so the greater check always succeeds where
                // a previous panel was already drawn, and only fails when
                // testing the outside of the inverted panel.
                foreach (Panel panel in page.Panels)
                {
                    if (!panel.isActiveAndEnabled)
                        continue;

                    // Inverted panel has already been drawn
                    if (panel == invertedPanel)
                        continue;

                    PanelView panelView = panel.PanelView;
                    if (panelView == null)
                        continue;

                    Matrix4x4 matrix = Matrix4x4.TRS(
                        panelView.transform.position,
                        panelView.transform.rotation,
                        new Vector3(panelView.Size.x, panelView.Size.y, 0.02f)
                    );

                    panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    cmd.DrawMesh(panelStencilMesh, matrix, panelStencilMaterials[panel.PanelNumber], 1, STENCIL_PASS_FORWARD, panelStencilProperties);
                }

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            if (state.camera.clearFlags == CameraClearFlags.Skybox)
            {
                context.DrawSkybox(state.camera);
            }

            {
                var cmd = CommandBufferPool.Get("Clear After Panel Stencils");

                // reset the depth buffer
                cmd.DrawMesh(panelStencilMesh, Matrix4x4.identity, panelStencilMaterials[0], 0, STENCIL_PASS_CLEAR_DEPTH);

                if (state.stereoEnabled)
                {
                    XRUtils.DrawOcclusionMesh(cmd, state.camera);
                }

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }

            foreach (var panel in page.Panels)
            {
                if (!panel.isActiveAndEnabled)
                    continue;

                PanelView panelView = panel.PanelView;
                if (panelView == null)
                    continue;

                string keyword = panel == invertedPanel ? "_PANEL_CUTOUT" : "_PANEL_CLIP";
                {
                    var cmd = CommandBufferPool.Get("Set Panel Variables");

                    Vector2 size = panelView.Size;

                    Quaternion rotation = panelView.transform.rotation;
                    Vector3 position = panelView.transform.position;

                    Vector3 normal = rotation * new Vector3(0, 0, 1);
                    if (panel == invertedPanel)
                        normal = -normal;

                    Vector3 up = rotation * new Vector3(0, 1, 0);
                    Vector3 right = rotation * new Vector3(1, 0, 0);

                    cmd.EnableShaderKeyword(keyword);
                    cmd.SetGlobalVector("_PanelForward", normal);
                    cmd.SetGlobalVector("_PanelUp", up / size.y);
                    cmd.SetGlobalVector("_PanelRight", right / size.x);
                    cmd.SetGlobalVector("_PanelPosition", position);
                    cmd.SetGlobalTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    cmd.SetGlobalInt("_PanelInvert", panel.IsInside ? 1 : 0);

                    SetPerPanelVariables(panel, cmd);

                    context.ExecuteCommandBuffer(cmd);
                    CommandBufferPool.Release(cmd);
                }

                filteringSettings.renderingLayerMask = 1u << panel.PanelNumber;
                filteringSettings.renderQueueRange = RenderQueueRange.opaque;
                sortingSettings.criteria = SortingCriteria.CommonOpaque;
                RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
                if (panel == invertedPanel)
                {
                    stateBlock.stencilState = new StencilState(enabled: false);
                }
                else
                {
                    stateBlock.stencilReference = panel.PanelNumber;
                    stateBlock.stencilState = new StencilState(compareFunction: CompareFunction.Equal);
                }
                context.DrawRenderers(state.cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

                RenderDrawnLines(context, state.camera, filteringSettings.renderingLayerMask, panel.PanelNumber, panel == invertedPanel);
                RenderOutlines(context, outlineMeshesForPanels[panel], panel.PanelNumber, panel == invertedPanel);

                sortingSettings.criteria = SortingCriteria.CommonTransparent;
                filteringSettings.renderQueueRange = RenderQueueRange.transparent;
                context.DrawRenderers(state.cullingResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

                {
                    var cmd = CommandBufferPool.Get("Set Panel Variables");

                    cmd.DisableShaderKeyword(keyword);

                    context.ExecuteCommandBuffer(cmd);
                    CommandBufferPool.Release(cmd);
                }
            }
        }

        void RenderOutlines(ScriptableRenderContext context, OutlineMeshState outlineMeshState, int panelNumber, bool inverted)
        {
            var cmd = CommandBufferPool.Get("Draw Outlines");

            outlineProperties.SetColor("_BaseColor", new Color(0, 0, 0, 1));
            outlineProperties.SetFloat("_Width", settings.OutlineThickness);

            if (settings.UseComputeLineGeneration)
            {
                cmd.SetGlobalBuffer("_LinesBuffer", outlineMeshState.lineBuffer);
                int passIndex = inverted ? 3 : 2;
                cmd.DrawProceduralIndirect(Matrix4x4.identity, outlineMaterials[panelNumber], passIndex, MeshTopology.Triangles, outlineMeshState.drawArgsBuffer, 0, outlineProperties);
            }
            else
            {
                int passIndex = inverted ? 1 : 0;
                cmd.DrawMesh(outlineMeshState.mesh, Matrix4x4.identity, outlineMaterials[panelNumber], 0, passIndex, outlineProperties);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        void RenderDrawnLines(ScriptableRenderContext context, Camera camera, uint renderingLayerMask, int panelNumber, bool inverted)
        {
            var cmd = CommandBufferPool.Get("Render Drawn Lines");

            drawnLineProperties.SetColor("_BaseColor", new Color(0, 0, 0, 1));

            int cameraMask = camera.cullingMask;
            foreach (var drawnLineRenderer in DrawnLineRenderer.drawnLineRenderers)
            {
                if (!drawnLineRenderer.enabled)
                    continue;

                if ((cameraMask & (1 << drawnLineRenderer.gameObject.layer)) == 0)
                    continue;

                if ((renderingLayerMask & drawnLineRenderer.SourceRenderer.renderingLayerMask) == 0)
                    continue;

                if (!drawnLineRenderer.GetDrawingParams(out DrawnLineMeshDrawingArgs args))
                    continue;

                int passIndex = inverted ? 1 : 0;
                cmd.DrawMesh(args.lineMesh, args.localToWorldMatrix, drawnLineMaterials[panelNumber], 0, passIndex, drawnLineProperties);
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        static readonly ProfilerMarker UpdateSkinnedOutlinesPerfMarker = new ProfilerMarker("RenderPipeline.UpdateSkinnedOutlines");
        void UpdateSkinnedOutlines(ScriptableRenderContext context)
        {
            UpdateSkinnedOutlinesPerfMarker.Begin();

            var cmd = CommandBufferPool.Get();

            foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
            {
                if (!outlineRenderer.IsValid)
                    continue;

                if (!outlineRenderer.enabled)
                    continue;

                if (outlineRenderer.IsSkinned)
                {
                    if (settings.UseComputeLineGeneration)
                    {
                        outlineRenderer.UpdateDynamicVerticesFromSkinnedMesh(cmd);

                        if (!outlineRenderer.GetComputeBufferParams(out OutlineMeshComputeArgs args))
                        {
                            continue;
                        }

                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "SourceVertices", args.dynamicSourceVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "WeldedVertexToSourceVertex", args.weldedVertexToSourceVertexBuffer);
                        cmd.DispatchCompute(resources.outlineUpdateVerticesComputeShader, 0, (args.numWeldedVertices + 31) / 32, 1, 1);

                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "Faces", args.facesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "FaceNormals", args.faceNormalsBuffer);
                        cmd.DispatchCompute(resources.outlineUpdateFaceNormalsComputeShader, 0, (args.numFaces + 31) / 32, 1, 1);
                    }
                    else
                    {
                        outlineRenderer.UpdateFromSkinningResult();
                    }
                }
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);

            UpdateSkinnedOutlinesPerfMarker.End();
        }

        static readonly ProfilerMarker GenerateOutlinesPerfMarker = new ProfilerMarker("RenderPipeline.GenerateOutlines");
        void GenerateOutlines(Camera camera, OutlineMeshState outlineMeshState, ScriptableRenderContext context, uint renderingLayerMask)
        {
            GenerateOutlinesPerfMarker.Begin();

            Vector3 cameraPosition = camera.transform.position;

            if (settings.UseComputeLineGeneration)
            {
                var cmd = CommandBufferPool.Get("Generate Outlines");

                cmd.SetComputeBufferCounterValue(outlineMeshState.lineBuffer, 0);

                int cameraMask = camera.cullingMask;
                foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
                {
                    if (!outlineRenderer.IsValid)
                        continue;

                    if (!outlineRenderer.enabled)
                        continue;

                    if ((cameraMask & (1 << outlineRenderer.gameObject.layer)) == 0)
                        continue;

                    if ((renderingLayerMask & outlineRenderer.SourceRenderer.renderingLayerMask) == 0)
                        continue;

                    if (!outlineRenderer.GetComputeBufferParams(out OutlineMeshComputeArgs args))
                    {
                        continue;
                    }

                    float4x4 worldToLocalMatrix = math.inverse(args.localToWorldMatrix);
                    float3 localCameraPosition = math.transform(worldToLocalMatrix, cameraPosition);

                    cmd.SetComputeMatrixParam(resources.outlineComputeShader, "LocalToWorldMatrix", args.localToWorldMatrix);
                    cmd.SetComputeVectorParam(resources.outlineComputeShader, "LocalCameraPosition", new float4(localCameraPosition, 0));
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "FaceNormals", args.faceNormalsBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "HalfEdges", args.halfEdgesBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "Edges", args.edgesBuffer);

                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "Lines", outlineMeshState.lineBuffer);
                    cmd.DispatchCompute(resources.outlineComputeShader, 0, (args.numEdges + 31) / 32, 1, 1);
                }

                cmd.CopyCounterValue(outlineMeshState.lineBuffer, outlineMeshState.drawArgsBuffer, 0);
                cmd.SetComputeBufferParam(resources.outlineArgsComputeShader, 0, "IndirectArgumentsBuffer", outlineMeshState.drawArgsBuffer);
                cmd.DispatchCompute(resources.outlineArgsComputeShader, 0, 1, 1, 1);

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }
            else
            {
                LineGenerationContext generationContext;
                generationContext.indices = new NativeList<int>(2024, Allocator.TempJob);
                generationContext.vertexData = new NativeList<LineGeneration.VertexData>(1024, Allocator.TempJob);

                int cameraMask = camera.cullingMask;
                foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
                {
                    if (!outlineRenderer.IsValid)
                        continue;

                    if (!outlineRenderer.enabled)
                        continue;

                    if ((cameraMask & (1 << outlineRenderer.gameObject.layer)) == 0)
                        continue;

                    if ((renderingLayerMask & outlineRenderer.SourceRenderer.renderingLayerMask) == 0)
                        continue;

                    outlineRenderer.GenerateForCamera(camera, generationContext);
                }

                outlineMeshState.mesh.Clear();
                outlineMeshState.mesh.indexFormat = IndexFormat.UInt32;
                outlineMeshState.mesh.SetVertexBufferParams(generationContext.vertexData.Length, new VertexAttributeDescriptor[]
                {
                    new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, dimension: 4),
                    new VertexAttributeDescriptor(VertexAttribute.TexCoord1, VertexAttributeFormat.Float32, dimension: 4),
                });

                outlineMeshState.mesh.SetVertexBufferData(generationContext.vertexData.AsArray(), 0, 0, generationContext.vertexData.Length);
                outlineMeshState.mesh.SetIndices(generationContext.indices.AsArray(), MeshTopology.Triangles, 0, false);
                outlineMeshState.mesh.UploadMeshData(false);

                generationContext.indices.Dispose();
                generationContext.vertexData.Dispose();
            }

            GenerateOutlinesPerfMarker.End();
        }

        /// <summary>
        /// Checks if a camera is a game camera.
        /// </summary>
        /// <param name="camera">Camera to check state from.</param>
        /// <returns>true if given camera is a game camera, false otherwise.</returns>
        public static bool IsGameCamera(Camera camera)
        {
            if (camera == null)
                throw new ArgumentNullException("camera");

            return camera.cameraType == CameraType.Game || camera.cameraType == CameraType.VR;
        }

        /// <summary>
        /// Checks if a camera is rendering in stereo mode.
        /// </summary>
        /// <param name="camera">Camera to check state from.</param>
        /// <returns>Returns true if the given camera is rendering in stereo mode, false otherwise.</returns>
        public static bool IsStereoEnabled(Camera camera)
        {
            if (camera == null)
                throw new ArgumentNullException("camera");

            bool isGameCamera = IsGameCamera(camera);
            bool isCompatWithXRDimension = true;
#if ENABLE_VR && ENABLE_VR_MODULE
            isCompatWithXRDimension &= (camera.targetTexture ? camera.targetTexture.dimension == UnityEngine.XR.XRSettings.deviceEyeTextureDimension : true);
#endif
            return XRGraphics.enabled && isGameCamera && (camera.stereoTargetEye == StereoTargetEyeMask.Both) && isCompatWithXRDimension;
        }
    }
}