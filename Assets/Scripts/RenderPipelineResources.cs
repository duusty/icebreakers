using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    [CreateAssetMenu(menuName = "Render Pipeline/Render Pipeline Resources", order = 2)]
    public class RenderPipelineResources : ScriptableObject
    {
        public Shader postProcessShader;
        public Shader outlineShader;
        public Shader drawnLineShader;
        public Shader panelStencilShader;
        public ComputeShader outlineComputeShader;
        public ComputeShader outlineArgsComputeShader;
        public ComputeShader outlineUpdateVerticesComputeShader;
        public ComputeShader outlineUpdateFaceNormalsComputeShader;
    }
}