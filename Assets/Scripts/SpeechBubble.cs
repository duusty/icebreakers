using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEditor;
using System.Linq;
using UnityEngine.Audio;

namespace Icebreakers
{
    [ExecuteInEditMode, SelectionBase]
    public class SpeechBubble : MonoBehaviour
    {
        [Serializable]
        public class SubBubble
        {
            [TextArea(10, 10)]
            public string text;
            [Space(10)]
            public int resolution;
            public float padding;
            [Range(0, 1)]
            public float square;

            public Vector2 position;
            public float delay = 0.0f;

            public AudioClip voiceline;

            [NonSerialized]
            public SpeechBubbleMesh bubbleMesh;
            [HideInInspector]
            public SpeechBubble parentBubble;
            [NonSerialized]
            public bool isVisible = true;
        }

        private AudioSource voiceSource;

        public GameObject prefabSpeechBubbleMesh;

        [NonReorderable]
        public List<SubBubble> bubbles = new List<SubBubble>();

        public float outlineMiterLimit;
        [Min(0)]
        public float outlineThickness;

        public Vector2 arrowPosition = new Vector2(-2, -1.5f);

        public float arrowWidth = 0.6f;

        [Range(0, 1)]
        public float attachmentAngle = 0.5f;

        public bool hasArrow = true;

        public float showAfter = 0.0f;

        public void AddBubble(string text)
        {
            var bubble = new SubBubble { text = text };
            if (bubbles.Count > 0)
            {
                SubBubble lastBubble = bubbles[bubbles.Count - 1];
                bubble.resolution = lastBubble.resolution;
                bubble.padding = lastBubble.padding;
                bubble.square = lastBubble.square;
                bubble.position = lastBubble.position + new Vector2(-0.5f, -0.5f);
                bubble.parentBubble = this;
            }
            else
            {
                bubble.resolution = 56;
                bubble.padding = 0.2f;
                bubble.square = 0.8f;
                bubble.position = new Vector2(0, 0);
            }

            bubbles.Add(bubble);

            InitBubble(bubble);

            RebuildBubbles();
        }

        public void RemoveBubble(SubBubble bubble)
        {
            if (!bubbles.Contains(bubble))
                throw new ArgumentException("Bubble is not part if this speech bubble");

            DeinitBubble(bubble);

            bubbles.Remove(bubble);

            RebuildBubbles();
        }

        void InitBubble(SubBubble bubble)
        {
            if (bubble.bubbleMesh != null)
                throw new Exception("Bubble already initialized");

            bubble.bubbleMesh = Instantiate(prefabSpeechBubbleMesh, this.transform, false).GetComponent<SpeechBubbleMesh>();
            bubble.bubbleMesh.transform.localPosition = bubble.position;
            bubble.bubbleMesh.transform.localRotation = Quaternion.identity;
            bubble.bubbleMesh.gameObject.hideFlags = HideFlags.HideAndDontSave;
            bubble.bubbleMesh.gameObject.SetActive(bubble.isVisible);
            bubble.bubbleMesh.Init();
        }

        void DeinitBubble(SubBubble bubble)
        {
            if (bubble.bubbleMesh != null)
            {
                bubble.bubbleMesh.Deinit();
                GameObject.DestroyImmediate(bubble.bubbleMesh.gameObject);
            }

            bubble.bubbleMesh = null;
        }

        public void RebuildBubbles()
        {
            for (int i = 0; i < bubbles.Count; i++)
            {
                SubBubble bubble = bubbles[i];
                bubble.bubbleMesh.UpdateSpeechBubbleMesh(this, bubble, i == 0 && hasArrow);
            }
        }

        public void ShowBubble(SubBubble bubble)
        {
            if (!bubbles.Contains(bubble))
                throw new ArgumentException("Bubble is not part if this speech bubble");

            if (bubble.bubbleMesh != null)
            {
                bubble.bubbleMesh.gameObject.SetActive(true);
            }

            if (voiceSource == null)
            {
                voiceSource = gameObject.AddComponent<AudioSource>();
                voiceSource.playOnAwake = false;
                AudioMixer mixer = Resources.Load("MainAudioMixer") as AudioMixer;
                voiceSource.outputAudioMixerGroup = mixer.FindMatchingGroups("Voiceover")[0];
            }
            if(bubble.voiceline != null) voiceSource.PlayOneShot(bubble.voiceline);

            bubble.isVisible = true;
        }

        public void HideBubble(SubBubble bubble)
        {
            if (!bubbles.Contains(bubble))
                throw new ArgumentException("Bubble is not part if this speech bubble");

            if (bubble.bubbleMesh != null)
            {
                bubble.bubbleMesh.gameObject.SetActive(false);
            }

            bubble.isVisible = false;
        }

        public void SetRenderingLayer(uint layer)
        {
            foreach (var bubble in bubbles)
            {
                if (bubble.bubbleMesh != null)
                {
                    bubble.bubbleMesh.meshRenderer.renderingLayerMask = layer;
                    bubble.bubbleMesh.textMesh.GetComponent<MeshRenderer>().renderingLayerMask = layer;
                }
            }
        }

        void OnEnable()
        {
            foreach (var bubble in bubbles)
            {
                InitBubble(bubble);
            }

            Panel panel = GetComponentInParent<Panel>();
            if (panel != null)
            {
                SetRenderingLayer(1u << panel.PanelNumber);
            }

            RebuildBubbles();
        }

        void OnDisable()
        {
            foreach (var bubble in bubbles)
            {
                DeinitBubble(bubble);
            }
        }

#if UNITY_EDITOR
        void Update()
        {
            if (!Application.isPlaying)
            {
                RebuildBubbles();
            }
        }
#endif

    }
}