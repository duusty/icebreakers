using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    [ExecuteInEditMode]
    public class TestIK : MonoBehaviour
    {
        IKSolver solver;

        PoseBone rootBone;
        PoseBone poseBone1;
        PoseBone poseBone2;
        PoseBone poseBone3;

        void OnEnable()
        {
            Transform root = this.transform;
            Transform bone1 = root.Find("Bone1");
            Transform bone2 = bone1.Find("Bone2");
            Transform bone3 = bone2.Find("Bone3");
            Transform target = root.Find("Target");
            Transform poleTarget = root.Find("PoleTarget");

            rootBone = new PoseBone
            {
                name = root.name,
                transform = root,
                restPosition = root.localPosition,
                restRotation = root.localRotation,
                restScale = root.localScale,
                parent = null,
            };

            poseBone1 = new PoseBone
            {
                name = bone1.name,
                transform = bone1,
                restPosition = bone1.localPosition,
                restRotation = Quaternion.AngleAxis(-30, Vector3.right),
                restScale = bone1.localScale,
                parent = rootBone,
            };

            poseBone2 = new PoseBone
            {
                name = bone2.name,
                transform = bone2,
                restPosition = bone2.localPosition,
                restRotation = Quaternion.AngleAxis(60, Vector3.right),
                restScale = bone2.localScale,
                parent = poseBone1,
            };

            poseBone3 = new PoseBone
            {
                name = bone3.name,
                transform = bone3,
                restPosition = bone3.localPosition,
                restRotation = bone3.localRotation,
                restScale = bone3.localScale,
                parent = poseBone2,
            };

            PoseBone targetBone = new PoseBone
            {
                name = target.name,
                transform = target,
                restPosition = target.localPosition,
                restRotation = target.localRotation,
                restScale = target.localScale,
                parent = rootBone,
            };

            PoseBone poleTargetBone = new PoseBone
            {
                name = poleTarget.name,
                transform = poleTarget,
                restPosition = poleTarget.localPosition,
                restRotation = poleTarget.localRotation,
                restScale = poleTarget.localScale,
                parent = rootBone,
            };

            solver = new IKSolver(new PoseBone[] { poseBone1, poseBone2, poseBone3 }, targetBone, poleTargetBone);
        }

        void Update()
        {
            solver.Solve(500, 0.01f);

            Quaternion poseBone3Rotation = poseBone3.transform.rotation;
            Quaternion poseBone3LocalRotation = poseBone3.transform.localRotation;
            Quaternion poseBone3MyRotation = rootBone.transform.localRotation * poseBone1.transform.localRotation * poseBone2.transform.localRotation * poseBone3.transform.localRotation;
            Quaternion poseBone3MyLocalRotation = Quaternion.Inverse(rootBone.transform.localRotation * poseBone1.transform.localRotation * poseBone2.transform.localRotation) * poseBone3.transform.rotation;

            Debug.Assert(poseBone3Rotation == poseBone3MyRotation);
            Debug.Assert(poseBone3LocalRotation == poseBone3MyLocalRotation);

            Debug.Assert(poseBone3Rotation == poseBone3.LocalSpaceToRigSpace(poseBone3.transform.localRotation));

            Debug.Assert(poseBone3.RigSpaceToLocalSpace(poseBone3Rotation) == poseBone3.transform.localRotation);

            Debug.Assert(poseBone2.RigSpaceToLocalSpace(poseBone2.transform.rotation) == poseBone2.transform.localRotation);
            Debug.Assert(poseBone2.LocalSpaceToRigSpace(poseBone2.transform.localRotation) == poseBone2.transform.rotation);


            Matrix4x4 rootBoneTransform = Matrix4x4.TRS(rootBone.transform.localPosition, rootBone.transform.localRotation, rootBone.transform.localScale);
            Matrix4x4 bone1Transform = Matrix4x4.TRS(poseBone1.transform.localPosition, poseBone1.transform.localRotation, poseBone1.transform.localScale);
            Matrix4x4 bone2Transform = Matrix4x4.TRS(poseBone2.transform.localPosition, poseBone2.transform.localRotation, poseBone2.transform.localScale);
            Matrix4x4 bone3Transform = Matrix4x4.TRS(poseBone3.transform.localPosition, poseBone3.transform.localRotation, poseBone3.transform.localScale);

            Vector3 poseBone3Position = poseBone3.transform.position;
            Vector3 poseBone3LocalPosition = poseBone3.transform.localPosition;
            Vector3 poseBone3MyPosition = (rootBoneTransform * bone1Transform * bone2Transform).MultiplyPoint(poseBone3.transform.localPosition);
            Vector3 poseBone3MyLocalPosition = (rootBoneTransform * bone1Transform * bone2Transform).inverse.MultiplyPoint(poseBone3.transform.position);

            Debug.Assert(poseBone3Position == poseBone3MyPosition);
            Debug.Assert(poseBone3LocalPosition == poseBone3MyLocalPosition);

            Debug.Assert(poseBone3Position == poseBone3.LocalSpaceToRigSpace(poseBone3.transform.localPosition));

            Debug.Assert(poseBone3.RigSpaceToLocalSpace(poseBone3Position) == poseBone3.transform.localPosition);

            Debug.Assert(poseBone2.RigSpaceToLocalSpace(poseBone2.transform.position) == poseBone2.transform.localPosition);
            Debug.Assert(poseBone2.LocalSpaceToRigSpace(poseBone2.transform.localPosition) == poseBone2.transform.position);
        }

        void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            for (int i = 0; i < solver.VertexPositions.Length - 1; i++)
            {
                Gizmos.DrawLine(solver.VertexPositions[i], solver.VertexPositions[i + 1]);
            }
        }
    }
}