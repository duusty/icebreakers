using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    public static class RectUtil
    {
        public static Rect FromLeft(this ref Rect rect, float amount)
        {
            Rect result = new Rect(rect.x, rect.y, amount, rect.height);
            rect.xMin += amount;
            return result;
        }

        public static Rect FromRight(this ref Rect rect, float amount)
        {
            Rect result = new Rect(rect.xMax - amount, rect.y, amount, rect.height);
            rect.width -= amount;
            return result;
        }

        public static Rect FromTop(this ref Rect rect, float amount)
        {
            Rect result = new Rect(rect.x, rect.y, rect.width, amount);
            rect.yMin += amount;
            return result;
        }

        public static Rect FromBottom(this ref Rect rect, float amount)
        {
            Rect result = new Rect(rect.x, rect.yMax - amount, rect.width, amount);
            rect.height -= amount;
            return result;
        }

        public static Rect Centered(this Rect rect, float size)
        {
            return Centered(rect, new Vector2(size, size));
        }

        public static Rect Centered(this Rect rect, Vector2 size)
        {
            float x = rect.x + Mathf.Round((rect.width - size.x) * 0.5f);
            float y = rect.y + Mathf.Round((rect.height - size.y) * 0.5f);
            Rect result = new Rect(x, y, size.x, size.y);
            return result;
        }
    }
}