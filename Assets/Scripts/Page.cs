using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Icebreakers
{
    [ExecuteInEditMode]
    public class Page : MonoBehaviour
    {
        [SerializeField]
        List<Panel> panels = new List<Panel>();

        public IReadOnlyList<Panel> Panels => panels;

        [SerializeField]
        Transform viewOriginTransform;

        public AudioClip music;
        private AudioSource musicSource;

        public Vector3 ViewOrigin => viewOriginTransform != null ? viewOriginTransform.position : Vector3.zero;
        public Quaternion ViewOriginRotation => viewOriginTransform != null ? viewOriginTransform.rotation : Quaternion.identity;

        [SerializeField]
        Transform worldRoot;
        public Transform WorldRoot => worldRoot;

        public void AddPanel(Panel panel)
        {
            panels.Add(panel);
        }

        public void RemovePanel(Panel panel)
        {
            panels.Remove(panel);
        }

#if UNITY_EDITOR
        public IList PanelList => panels;

        public void PerformInitialSetup()
        {
            viewOriginTransform = new GameObject("ViewOrigin").transform;
            worldRoot = new GameObject("World").transform;
            viewOriginTransform.SetParent(transform, false);
            worldRoot.SetParent(transform, false);
        }

        private void Awake()
        {
            if (!Application.isEditor || Application.isPlaying)
            {
                if (musicSource == null)
                {
                    musicSource = gameObject.AddComponent<AudioSource>();
                    musicSource.playOnAwake = false;
                    musicSource.loop = false;
                    AudioMixer mixer = Resources.Load("MainAudioMixer") as AudioMixer;
                    musicSource.outputAudioMixerGroup = mixer.FindMatchingGroups("Music")[0];
                }
                musicSource.clip = music;
                musicSource.Play();
            }
        }

        void Update()
        {
            if (Application.isPlaying)
                return;

            for (int i = 0; i < panels.Count; i++)
            {
                Panel panel = panels[i];
                panel.UpdateRenderingLayers(i + 1);

                panel.transform.localPosition = Vector3.zero;
                panel.transform.localRotation = Quaternion.identity;
                panel.transform.localScale = Vector3.one;
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }
#endif
    }
}