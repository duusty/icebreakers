using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputTestScript : MonoBehaviour
{
    // Start is called before the first frame update
    Material mat;
    bool isWhite;

    public Transform origin, target;


    // Update is called once per frame
    void OnEnable()
    {
            isWhite = !isWhite;
            mat = GetComponent<MeshRenderer>().sharedMaterial;
            mat.color = isWhite ? Color.white : Color.red;
            parentReparent();
    }

    void parentReparent()
    {
        target.SetParent(origin);
        origin.Rotate(new Vector3(90, 0, 0));
        target.SetParent(null);
    }
}
