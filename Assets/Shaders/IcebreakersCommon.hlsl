#ifndef ICEBREAKERS_COMMON_INCLUDED
#define ICEBREAKERS_COMMON_INCLUDED

// packs given two floats into one float
float pack_float(half x, half y) {
    int x1 = (int)(x * 255);
    int y1 = (int)(y * 255);
    float f = (y1 * 256) + x1;
    return f;
}

// unpacks given float to two floats
half2 unpack_float(float f) {
    float dy = floor(f / 256);
    float dx = f - (dy * 256);
    half y = (half)(dy / 255);
    half x = (half)(dx / 255);
    return half2(x, y);
}

#define UNITY_MATRIX_M     unity_ObjectToWorld
#define UNITY_MATRIX_I_M   unity_WorldToObject
#define UNITY_MATRIX_V     unity_MatrixV
#define UNITY_MATRIX_I_V   unity_MatrixInvV
#define UNITY_MATRIX_P     OptimizeProjectionMatrix(glstate_matrix_projection)
#define UNITY_MATRIX_I_P   ERROR_UNITY_MATRIX_I_P_IS_NOT_DEFINED
#define UNITY_MATRIX_VP    unity_MatrixVP
#define UNITY_MATRIX_I_VP  unity_MatrixInvVP
#define UNITY_MATRIX_MV    mul(UNITY_MATRIX_V, UNITY_MATRIX_M)
#define UNITY_MATRIX_T_MV  transpose(UNITY_MATRIX_MV)
#define UNITY_MATRIX_IT_MV transpose(mul(UNITY_MATRIX_I_M, UNITY_MATRIX_I_V))
#define UNITY_MATRIX_MVP   mul(UNITY_MATRIX_VP, UNITY_MATRIX_M)

#include "UnityInput.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/SpaceTransforms.hlsl"

#if _PANEL_CUTOUT || _PANEL_CLIP
Texture2D _PanelMask;
SamplerState sampler_PanelMask;
float3 _PanelForward;
float3 _PanelUp;
float3 _PanelRight;
float3 _PanelPosition;
int _PanelInvert;

bool IsInsidePanel(float3 posWS)
{
    float3 rayOrigin = _WorldSpaceCameraPos;
    float3 rayDir = posWS - rayOrigin;
    float dist = length(rayDir);
    rayDir /= dist;

    float3 planeNormal = _PanelForward;
    float3 planeOrigin = _PanelPosition;

    float distance = -dot(planeNormal, planeOrigin);

    float vdot = dot(rayDir, planeNormal);
    if (vdot > 0)
        return false;

    float ndot = -dot(rayOrigin, planeNormal) - distance;

    if (abs(vdot) < 0.0001f)
    {
        return false;
    }

    float t = ndot / vdot;

    if (t < 0 || t > dist)
    {
        return false;
    }

    float3 pointOnPlane = (rayOrigin + rayDir * t) - planeOrigin;
    float x = dot(pointOnPlane, _PanelRight);
    float y = dot(pointOnPlane, _PanelUp);

    float2 uv = float2(x, y) + 0.5;

    if (any(saturate(uv) != uv))
    {
        return false;
    }

    float mask = _PanelMask.Sample(sampler_PanelMask, uv).a;
    return mask > 0.5;
}

bool ClipToPanel(float3 positionWS)
{
    return _PanelInvert ^ IsInsidePanel(positionWS);
}

bool CullInFrontOfPanel(float3 positionWS)
{
	return false;// dot(positionWS - _PanelPosition, _PanelForward) > 0;
}
#endif

#endif