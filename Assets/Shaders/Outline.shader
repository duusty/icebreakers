Shader "Hidden/Icebreakers/Outline"
{
    Properties
    {
        _PanelNumber("Panel Number", float) = 0
    }

    SubShader
    {
        Tags{ "RenderType"="Opaque" "Queue"="Geometry+100" }
        LOD 300

        Cull Off
        Offset -3, -3

        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct Appdata
        {
            float4 p1 : TEXCOORD0;
            float4 p2 : TEXCOORD1;
        };

        struct v2f
        {
            float4 positionCS : SV_POSITION;
            noperspective float3 l : TEXCOORD0;
            float3 positionWS : TEXCOORD1;
        };

        half4 _BaseColor;
        float _Width;

        struct Line
        {
            float3 p1;
            float3 p2;
        };
        StructuredBuffer<Line> _LinesBuffer;

        static const float lineVertices[6] = { 0, 0, 1, 0, 1, 1};
        static const float lineSides[6] = { -1, 1, 1, -1, 1, -1 };

        v2f vert_compute(uint vertexID: SV_VertexID)
        {
            int lineIndex = vertexID / 6;
            Line l = _LinesBuffer[lineIndex];

            v2f output;

            float lineVertex = lineVertices[vertexID % 6];
            float lineSide = lineSides[vertexID % 6];
            float lineWidth = _Width;

            float3 p1 = l.p1;
            float3 p2 = l.p2;

            float aspect = _ScreenParams.x / _ScreenParams.y;

            float4 v1 = TransformWorldToHClip(p1);
            float4 v2 = TransformWorldToHClip(p2);

            float2 c1 = v1.xy / v1.w * aspect;
            float2 c2 = v2.xy / v2.w * aspect;

            float2 clipDir = normalize(c2 - c1);

            float2 normal = float2(-clipDir.y, clipDir.x);

            float pixelWidth = 1.0 / _ScreenParams.x;
			float width = 1.8 * lineWidth * pixelWidth;

            normal *= width * 0.5;
            normal.y *= aspect;

            float4 p = lineVertex ? v2 : v1;
            //p.xy += (lineVertex ? clipDir : -clipDir) * width * aspect * 0.5;
            p.xy += normal * lineSide * p.w;

            output.positionCS = p;
            output.l = float3(lineSide, lineVertex, lineWidth);
            output.positionWS = lineVertex ? p2 : p1;
            return output;
        }

        v2f vert(Appdata input)
        {
            v2f output;

            float lineVertex = input.p1.w;
            float lineSide = input.p2.w;
            float lineWidth = _Width;

            float3 p1 = TransformObjectToWorld(input.p1.xyz);
            float3 p2 = TransformObjectToWorld(input.p2.xyz);

            float aspect = _ScreenParams.x / _ScreenParams.y;

            float4 v1 = TransformWorldToHClip(p1);
            float4 v2 = TransformWorldToHClip(p2);

            float2 c1 = v1.xy / v1.w * aspect;
            float2 c2 = v2.xy / v2.w * aspect;

            float2 clipDir = normalize(c2 - c1);

            float2 normal = float2(-clipDir.y, clipDir.x);

            float pixelWidth = 1.0 / _ScreenParams.x;
            float width = 1.8 * lineWidth * pixelWidth;

            normal *= width * 0.5;
            normal.y *= aspect;

            float4 p = lineVertex ? v2 : v1;
            p.xy += (lineVertex ? clipDir : -clipDir) * width * aspect * 0.5;
            p.xy += normal * lineSide * p.w;

            output.positionCS = p;
            output.l = float3(lineSide, lineVertex, lineWidth);
            output.positionWS = lineVertex ? p2 : p1;
            return output;
        }

        half4 frag(v2f input) : SV_Target
        {
            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard;
            #endif

            #ifdef _PANEL_CUTOUT
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            half3 color = _BaseColor.rgb;
            return half4(color.rgb, 1);
        }

        ENDHLSL

        // 0 CPU
        Pass
        {
            Stencil {
                Ref [_PanelNumber]
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }

        // 1 CPU No Stencil
        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }

        // 2 Compute
        Pass
        {
            Stencil {
                Ref [_PanelNumber]
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert_compute
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }

        // 3 Compute No Stencil
        Pass
        {
            HLSLPROGRAM

            #pragma vertex vert_compute
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }
    }
}
