﻿Shader "Sony/OutlinePostEffect"
{
    Properties
    {
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100
        ZTest Always

        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "IcebreakersCommon.hlsl"

            #define OPTIMIZED 1

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            struct Appdata
            {
                float3 positionOS   : POSITION;
                float2 texcoord     : TEXCOORD0;
            };

            Texture2D _TempColorBuffer;
            SamplerState sampler_TempColorBuffer;
            float4 _TempColorBuffer_TexelSize;

            v2f vert (Appdata input)
            {
                v2f o;
                o.vertex = TransformWorldToHClip(input.positionOS);
                o.uv = input.texcoord;
                return o;
            }

            struct PackedData
            {
#if OPTIMIZED
                half4 combined;
#else
                half hatching;
                half shadow;
                half texLinesR;
                half texLinesG;
#endif
            };

            PackedData DecodeData(half packed)
            {
                int dataBits = int(packed * 256.0);

                PackedData data;

#if OPTIMIZED
                data.combined = half4((int4(dataBits, dataBits, dataBits, dataBits) >> int4(0, 1, 2, 3)) & int4(1, 1, 1, 1));
#else
                data.hatching = half(dataBits & 1);
                data.shadow = half((dataBits >> 1) & 1);
                data.texLinesR = half((dataBits >> 2) & 1);
                data.texLinesG = half((dataBits >> 3) & 1);
#endif

                return data;
            }

            half4 frag (v2f i) : SV_Target
            {
                int2 coord = int2(i.uv * _TempColorBuffer_TexelSize.zw);
                half4 sampleOrigin = _TempColorBuffer.Load(int3(coord, 0));
                PackedData dataOrigin = DecodeData(sampleOrigin.a);
                PackedData dataCorner = DecodeData(_TempColorBuffer.Load(int3(coord - int2(1, 1), 0)).a);
                PackedData dataLeft = DecodeData(_TempColorBuffer.Load(int3(coord - int2(1, 0), 0)).a);
                PackedData dataDown = DecodeData(_TempColorBuffer.Load(int3(coord - int2(0, 1), 0)).a);

#if OPTIMIZED
                half3 lines1 = abs(dataCorner.combined.xyz - dataOrigin.combined.xyz);
                half3 lines2 = abs(dataLeft.combined.xyz - dataOrigin.combined.xyz);
                half3 lines3 = abs(dataDown.combined.xyz - dataOrigin.combined.xyz);

                half3 linesSum = saturate(lines1 + lines2 + lines3);
                linesSum *= half3(1 - dataOrigin.combined.w, 1 - dataOrigin.combined.y, 1);

                half lines = 1 - saturate(dot(linesSum, half3(1.0, 1.0, 1.0)));
#else
                half texLine1 = abs(dataCorner.texLinesR - dataOrigin.texLinesR);
                half texLine2 = abs(dataLeft.texLinesR - dataOrigin.texLinesR);
                half texLine3 = abs(dataDown.texLinesR - dataOrigin.texLinesR);
                half texLine = saturate(texLine1 + texLine2 + texLine3) * (1 - dataOrigin.texLinesG);

                half hatching1 = abs(dataCorner.hatching - dataOrigin.hatching);
                half hatching2 = abs(dataLeft.hatching - dataOrigin.hatching);
                half hatching3 = abs(dataDown.hatching - dataOrigin.hatching);
                half hatching = saturate(hatching1 + hatching2 + hatching3) * (1 - dataOrigin.shadow);

                half shadow1 = abs(dataCorner.shadow - dataOrigin.shadow);
                half shadow2 = abs(dataLeft.shadow - dataOrigin.shadow);
                half shadow3 = abs(dataDown.shadow - dataOrigin.shadow);
                half shadowOutline = saturate(shadow1 + shadow2 + shadow3);

                half lines = 1 - saturate(texLine + hatching + shadowOutline);
#endif
                half4 col;
                col.rgb = sampleOrigin.rgb * lines;
                col.a = 1;

                return col;
            }
            ENDHLSL
        }
    }
}
