#ifndef ICEBREAKERS_PANEL_STENCIL_INCLUDED
#define ICEBREAKERS_PANEL_STENCIL_INCLUDED

#include "IcebreakersCommon.hlsl"

struct Appdata
{
    float3 position : POSITION;
    float2 texcoord : TEXCOORD0;
};

struct v2f
{
    float4 positionCS : SV_POSITION;
    float2 uv : TEXCOORD0;
};

Texture2D _PanelMask;
SamplerState sampler_PanelMask;

v2f vert(Appdata input)
{
    v2f output;

    output.positionCS = TransformWorldToHClip(TransformObjectToWorld(input.position));
    output.uv = input.texcoord;
    return output;
}

half4 frag(v2f input) : SV_Target
{
    if (_PanelMask.Sample(sampler_PanelMask, input.uv).a < 0.5)
        discard;

    return half4(0, 0, 0, 0);
}

float4 vert_clear(float3 position : POSITION) : SV_POSITION
{
    float depth = 1;
    #if UNITY_REVERSED_Z
    depth = 0;
    #endif
    return float4(position.xy * 2, depth, 1);
}

half4 frag_clear() : SV_Target
{
    return half4(0, 0, 0, 0);
}

half4 frag_debug(v2f input) : SV_Target
{
    if (!(_PanelMask.Sample(sampler_PanelMask, input.uv).a < 0.5))
        discard;

    return half4(0, 0, 0, 0);
}

#endif