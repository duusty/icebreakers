Shader "Icebreakers/Baked"
{
    Properties
    {
        [NoScaleOffset] _BakedTexture("Baked Texture", 2D) = "white" {}
        _BakedRect("Baked Rect", Vector) = (0,0,1,1)
    }

    SubShader
    {
        Tags{ "RenderType"="Opaque"}
        LOD 300

        Stencil {
            Ref 0
            Comp Equal
        }

        Pass
        {
            Name "OutlineData"
            Tags{ "LightMode"="Base" }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            #include "IcebreakersCommon.hlsl"

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            struct v2f
            {
                float2 uv         : TEXCOORD0;
                float3 positionWS : TEXCOORD1;
                float3 normalWS   : TEXCOORD2;
                float4 positionCS : SV_POSITION;
            };

            Texture2D _BakedTexture;
            SamplerState sampler_BakedTexture;

            CBUFFER_START(UnityPerMaterial)
                float4 _BakedRect;
            CBUFFER_END

            v2f vert(Appdata input)
            {
                v2f output;

                output.positionWS = TransformObjectToWorld(input.position);
                output.positionCS = TransformWorldToHClip(output.positionWS);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord * _BakedRect.zw + _BakedRect.xy;
                return output;
            }

            half4 frag(v2f input) : SV_Target
            {
                #ifdef _PANEL_CLIP
                    if (CullInFrontOfPanel(input.positionWS)) discard;
                #endif

                #ifdef _PANEL_CUTOUT
                    if (IsInsidePanel(input.positionWS)) discard;
                #endif

                half4 color = _BakedTexture.Sample(sampler_BakedTexture, input.uv);
                return color;
            }

            ENDHLSL
        }
    }
}
