Shader "Hidden/Icebreakers/DrawnLine"
{
	Properties
	{
		_PanelNumber("Panel Number", float) = 0
	}

		SubShader
	{
		Tags{ "RenderType" = "Opaque" "Queue" = "Geometry+100" }
		LOD 300

        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct Appdata
        {
            float4 vertex : POSITION;
            float4 side : TEXCOORD0;
        };

        struct v2f
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS : TEXCOORD0;
        };

        half4 _BaseColor;

        v2f vert(Appdata input)
        {
            v2f output;

            float3 vertex = TransformObjectToWorld(input.vertex.xyz);
            float3 side = TransformObjectToWorldNormal(input.side.xyz);
            float width = input.side.w;

            vertex += side * width;

            output.positionCS = TransformWorldToHClip(vertex);
            output.positionWS = vertex;
            return output;
        }

        half4 frag(v2f input) : SV_Target
        {
            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard;
            #endif

            #ifdef _PANEL_CUTOUT
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            half3 color = _BaseColor.rgb;
            return half4(color.rgb, 1);
        }

        ENDHLSL

        // 0 CPU
        Pass
        {
            Cull Off
            Offset -5, -5

            Stencil {
                Ref [_PanelNumber]
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }

        // 1 CPU No Stencil
        Pass
        {
            Cull Off
            Offset -5, -5

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }
    }
}
