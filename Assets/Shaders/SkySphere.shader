Shader "Icebreakers/SkySphere"
{
    Properties
    {
        [MainTexture] _Skybox("Skybox", 2D) = "white" {}
    }

    SubShader
    {
        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct v2f
        {
            float2 uv         : TEXCOORD0;
            float3 positionWS : TEXCOORD1;
            float3 normalWS   : TEXCOORD2;
            float4 positionCS : SV_POSITION;
        };

        Texture2D _Skybox;
        SamplerState sampler_Skybox;

        CBUFFER_START(UnityPerMaterial)
        CBUFFER_END

        half4 frag(v2f input) : SV_Target
        {
            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard; //currently disabled
            #endif

            #ifdef _PANEL_CUTOUT 
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            half4 color = _Skybox.Sample(sampler_Skybox, input.uv);
            return color;
        }

        ENDHLSL

        Tags{ "RenderType"="Opaque"}
        LOD 300

        Pass
        {
            Name "OutlineData"
            Tags{ "LightMode"="Base" }

            Stencil {
                Ref 0
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

            v2f vert(Appdata input)
            {
                v2f output;

                output.positionWS = TransformObjectToWorld(input.position);
                output.positionCS = TransformWorldToHClip(output.positionWS);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord;
                return output;
            }

            ENDHLSL
        }

        Pass // 1 Bake
        {
            Name "Bake"
            Tags { "LightMode"="Bake" }

            Cull Off
            ZTest Always

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            float4 _BakeRect;

            v2f vert(Appdata input)
            {
                v2f output;

                float2 bakeUV = input.texcoord * _BakeRect.zw + _BakeRect.xy;
                output.positionCS = float4(bakeUV * 2 - 1, 0, 1);
                #if UNITY_UV_STARTS_AT_TOP
                output.positionCS.y *= -1;
                #endif

                output.positionWS = TransformObjectToWorld(input.position);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord;
                return output;
            }

            ENDHLSL
        }

        // Pass //shadowCasterPass
        // {
        // 	Name "ShadowCaster"
        // 	Tags{"LightMode" = "ShadowCaster"}

        // 	ZWrite On
        // 	ZTest LEqual

        // 	HLSLPROGRAM
        // 		// Required to compile gles 2.0 with standard srp library
        // 		#pragma prefer_hlslcc gles
        // 		#pragma exclude_renderers d3d11_9x
        // 		#pragma target 2.0

        // 		// -------------------------------------
        // 		// Material Keywords
        // 		#pragma shader_feature _ALPHATEST_ON

        // 		//--------------------------------------
        // 		// GPU Instancing
        // 		#pragma multi_compile_instancing
        // 		#pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

        // 		#pragma vertex ShadowPassVertex
        // 		#pragma fragment ShadowPassFragment

        // 		#include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"
        // 		#include "Packages/com.unity.render-pipelines.universal/Shaders/ShadowCasterPass.hlsl"
        // 	ENDHLSL
        // }

    }
}
