Shader "Icebreakers/SpeechBubble"
{
    Properties
    {
        [MainTexture] _BaseMap("Text Texture", 2D) = "white" {}
        [MainColor] _BaseColor("Color", Color) = (1,1,1,1)
    }

    SubShader
    {
        HLSLINCLUDE

			#include "IcebreakersCommon.hlsl"

			struct v2f
			{
				float2 uv         : TEXCOORD0;
				float3 positionWS : TEXCOORD1;
				float4 positionCS : SV_POSITION;
			};

			Texture2D _BaseMap;
			SamplerState sampler_BaseMap;

			CBUFFER_START(UnityPerMaterial)
				float4 _BaseMap_ST;
				float4 _BaseColor;
			CBUFFER_END

			half4 frag(v2f input) : SV_Target
			{
				#ifdef _PANEL_CLIP
					if (CullInFrontOfPanel(input.positionWS)) discard;
				#endif

				#ifdef _PANEL_CUTOUT
					if (IsInsidePanel(input.positionWS)) discard;
				#endif

				half4 color = _BaseMap.Sample(sampler_BaseMap, input.uv) * _BaseColor;
				return half4(color.rgb, 1);
			}

        ENDHLSL

        Tags{ "RenderType"="Opaque" "Queue"="Transparent+99"}
        LOD 300
		
        Pass
        {
            Name "MainPass"
            Tags{ "LightMode"="Base" }

			//ZTest Always
			ZWrite Off

			Cull Off

            Stencil {
                Ref 0
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float2 texcoord : TEXCOORD0;
            };

            v2f vert(Appdata input)
            {
                v2f output;
                output.positionWS = TransformObjectToWorld(input.position);
                output.positionCS = TransformWorldToHClip(output.positionWS);
                output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
                return output;
            }

            ENDHLSL
        }

        /*Pass // 1 Bake
        {
            Name "Bake"
            Tags { "LightMode"="Bake" }

            Cull Off
            ZTest Always

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            float4 _BakeRect;

            v2f vert(Appdata input)
            {
                v2f output;

                float2 bakeUV = input.texcoord * _BakeRect.zw + _BakeRect.xy;
                output.positionCS = float4(bakeUV * 2 - 1, 0, 1);
                #if UNITY_UV_STARTS_AT_TOP
                output.positionCS.y *= -1;
                #endif

                output.positionWS = TransformObjectToWorld(input.position);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
                return output;
            }

            ENDHLSL
        }*/
        
    }
}
