Shader "Icebreakers/Standard"
{
    Properties
    {
        [MainTexture] _BaseMap("Albedo", 2D) = "white" {}
        [MainColor] _BaseColor("Color", Color) = (1,1,1,1)

        [MaterialToggle] _UseShadowMap("Use Shadow Map", float) = 0
        [NoScaleOffset] _ShadowMap("Shadow Albedo", 2D) = "white" {}
        _ShadowColor("Shadow Color", Color) = (1,1,1,1)

        [NoScaleOffset] _HatchingMap("Hatching Map", 2D) = "white" {}
        _HatchingLineCount("Hatching Line Count", float) = 200
        _HatchingRatio("Hatching Ratio", float) = 1

        _ShadowOutlineWidth("Shadow Outline Width", float) = 1
    }

    SubShader
    {
        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct v2f
        {
            float2 uv         : TEXCOORD0;
            float3 positionWS : TEXCOORD1;
            float3 normalWS   : TEXCOORD2;
            float4 positionCS : SV_POSITION;
        };

        Texture2D _HatchingMap;
        SamplerState sampler_HatchingMap;
        Texture2D _ShadowMap;
        SamplerState sampler_ShadowMap;
        Texture2D _BaseMap;
        SamplerState sampler_BaseMap;

        CBUFFER_START(UnityPerMaterial)
            float4 _BaseMap_ST;
            float4 _BaseColor;
            float4 _ShadowColor;
            float _UseShadowMap;
            float _HatchingLineCount;
            float _HatchingRatio;
            float4 _OverrideLightDirection;
            float _ShadowOutlineWidth;
        CBUFFER_END

        float3 _ViewOrigin;
        float3 _ViewOriginForward;
        float3 _ViewOriginUp;
        float3 _ViewOriginRight;

        float4 _SceneLightDir;
        float4 _SceneLightColor;

        float ProceduralHatching(float x, float spacing)
        {
            // make x periodic
            x = (frac(x) * 2 - 1);

            // then use a light falloff function so that it reaches 0
            // at spacing and scale it back to [-1, 1]
            float x2 = x*x;
            float x4 = x*x*x*x;
            float xr = x*spacing*2;
            float f = ((1 - x4) * (1 - x4)) / ((xr*xr) + 1);

            // threshold at 0.5 to get a repeating pattern
            // the dark lines will smaller the steeper the falloff function
            return f < 0.5;
        }

        half4 frag(v2f input) : SV_Target
        {
			#ifdef _PANEL_CLIP
				if (CullInFrontOfPanel(input.positionWS)) discard; //currently disabled
			#endif

			#ifdef _PANEL_CUTOUT 
				if (IsInsidePanel(input.positionWS)) discard;
			#endif

            //Hatching Data///////////////////////////
            float3 staticLinesDirection = input.positionWS - _ViewOrigin;
            float distanceToViewOrigin = length(staticLinesDirection);

            float upComponent = dot(staticLinesDirection, _ViewOriginUp);
            float fwdComponent = dot(staticLinesDirection, _ViewOriginForward);

            half angle = atan2(upComponent, -fwdComponent);

            angle *= round(_HatchingLineCount);

            //float3 hatching = _HatchingMap.SampleLevel(sampler_HatchingMap, float2(0, angle), 0).rgb;
            float hatching = ProceduralHatching(angle, _HatchingRatio);
            ////////////////////////////////////////// lighting and combination

            half3 lightDir = -lerp(_SceneLightDir.xyz, _OverrideLightDirection.xyz, _OverrideLightDirection.w);
            half3 lightColor = _SceneLightColor.rgb;


            // TODO: shadows
            half shadow = 1;

            half lambert = dot(lightDir, input.normalWS);

            float shadingOutlineWidth = distanceToViewOrigin / 400 * _ShadowOutlineWidth;
            half shadingOutline = lambert > -shadingOutlineWidth && lambert < shadingOutlineWidth ? 0 : 1;

            int rshadow = saturate(lambert) * shadow > 0.001 ? 1 : 0;

            half4 color = _BaseMap.Sample(sampler_BaseMap, input.uv);
            half4 shadowmap = _ShadowMap.Sample(sampler_ShadowMap, input.uv);

            if (_UseShadowMap)
            {
                color = lerp(color * _BaseColor, shadowmap, rshadow);
            }
            else
            {
                color *= lerp(_ShadowColor, _BaseColor, rshadow);
            }

            color.rgb *= lerp(hatching, float3(1, 1, 1), rshadow);
            color *= shadingOutline;

            return half4(color.rgb, 1);
        }

        ENDHLSL

        Tags{ "RenderType"="Opaque"}
        LOD 300

        Pass
        {
            Name "OutlineData"
            Tags{ "LightMode"="Base" }

            Stencil {
                Ref 0
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
            };

            v2f vert(Appdata input)
            {
                v2f output;

                output.positionWS = TransformObjectToWorld(input.position);
                output.positionCS = TransformWorldToHClip(output.positionWS);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
                return output;
            }

            ENDHLSL
        }

        Pass // 1 Bake
        {
            Name "Bake"
            Tags { "LightMode"="Bake" }

            Cull Off
            ZTest Always

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Appdata
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            float4 _BakeRect;

            v2f vert(Appdata input)
            {
                v2f output;

                float2 bakeUV = input.texcoord * _BakeRect.zw + _BakeRect.xy;
                output.positionCS = float4(bakeUV * 2 - 1, 0, 1);
                #if UNITY_UV_STARTS_AT_TOP
                output.positionCS.y *= -1;
                #endif

                output.positionWS = TransformObjectToWorld(input.position);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
                return output;
            }

            ENDHLSL
        }

        // Pass //shadowCasterPass
        // {
        // 	Name "ShadowCaster"
        // 	Tags{"LightMode" = "ShadowCaster"}

        // 	ZWrite On
        // 	ZTest LEqual

        // 	HLSLPROGRAM
        // 		// Required to compile gles 2.0 with standard srp library
        // 		#pragma prefer_hlslcc gles
        // 		#pragma exclude_renderers d3d11_9x
        // 		#pragma target 2.0

        // 		// -------------------------------------
        // 		// Material Keywords
        // 		#pragma shader_feature _ALPHATEST_ON

        // 		//--------------------------------------
        // 		// GPU Instancing
        // 		#pragma multi_compile_instancing
        // 		#pragma shader_feature _SMOOTHNESS_TEXTURE_ALBEDO_CHANNEL_A

        // 		#pragma vertex ShadowPassVertex
        // 		#pragma fragment ShadowPassFragment

        // 		#include "Packages/com.unity.render-pipelines.universal/Shaders/LitInput.hlsl"
        // 		#include "Packages/com.unity.render-pipelines.universal/Shaders/ShadowCasterPass.hlsl"
        // 	ENDHLSL
        // }

    }
}
