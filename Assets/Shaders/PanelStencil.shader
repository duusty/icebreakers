Shader "Hidden/Icebreakers/PanelStencil"
{
    Properties
    {
        _PanelNumber("Panel Number", float) = 0
    }

    SubShader
    {
        // 0
        // draw stencils from smaller to larger to ensure that the
        // stencil test always succeeds where a panel pixel was written
        // pixels with larger stencil values will not be written (inside of a panel)
        Pass
        {
            Cull Back
            ZTest LEqual
            ZWrite On
            Stencil {
                Ref [_PanelNumber]
                Comp Greater
                Pass Replace
                Fail Keep
                ZFail Keep
            }

            ColorMask 0

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "IcebreakersPanelStencil.hlsl"

            ENDHLSL
        }

        // 1
        // draw inverted stencil looking from the inside
        // depth is not written
        Pass
        {
            Cull Front
            ZWrite Off
            ZTest Always
            Stencil {
                Ref [_PanelNumber]
                Comp Always
                Pass Replace
            }

            ColorMask 0

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            #include "IcebreakersPanelStencil.hlsl"

            ENDHLSL
        }

        // 2
        // clear depth
        Pass
        {
            ZTest Always
            Cull Off
            ColorMask 0

            HLSLPROGRAM

            #pragma vertex vert_clear
            #pragma fragment frag_clear

            #include "IcebreakersPanelStencil.hlsl"

            ENDHLSL
        }

        // 3
        // initial clear
        Pass
        {
            ZTest Always
            Cull Off
            ColorMask 0

            Stencil {
                Ref [_PanelNumber]
                Comp Always
                Pass Replace
            }

            HLSLPROGRAM

            #pragma vertex vert_clear
            #pragma fragment frag_clear

            #include "IcebreakersPanelStencil.hlsl"

            ENDHLSL
        }

        // 4
        // debug
        Pass
        {
            ZTest LEqual
            Cull Off

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag_debug

            #include "IcebreakersPanelStencil.hlsl"

            ENDHLSL
        }
    }
}
